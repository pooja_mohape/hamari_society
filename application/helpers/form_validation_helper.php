<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('form_validate_rules'))
{
    function form_validate_rules($config , $data =array())
    {
        $CI = & get_instance();
        

        if(count($data) >0)
        {
            $CI->form_validation->set_data($data);
        }
        
        $CI->form_validation->set_rules($config);

        $flag = TRUE;
                
        return $CI->form_validation->run() ;       
    }
}
