<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bill_payment extends CI_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Headers: Content-Type');
		header('Content-Type: text/html; charset=utf-8');
		header('Content-Type: application/json; charset=utf-8');
		$this->load->model(array('users_model','bill_payment_model','wallet_transaction_model','wallet_model','society_house_charges_model','parking_model','parkmaster_model','parking_charges_model','facility_model','my_bill_model','house_master_model'));
        
        $data = array();
        $data['url'] = current_url();
        $data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $data['post'] = json_decode(file_get_contents('php://input'));
        $data['ip'] = $this->input->ip_address();
        //log_data('Api/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $data, 'api');
	}
  public function bill_request()
  {
    $response = array();
    $msg = "";$status="";
    
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
       $input = json_decode(file_get_contents('php://input'));
       $user_id  = isset($input->user_id) ? $input->user_id : '';
       $transaction_type = isset($input->transaction_type) ? $input->transaction_type : '';
       $amount = isset($input->amount) ? $input->amount : '';
       $bank_name = isset($input->bank_name) ? $input->bank_name : '';
       $bank_accno = isset($input->bank_accno) ? $input->bank_accno : '';
       $branch_name = isset($input->branch_name) ? $input->branch_name : '';
       $ifsc_code = isset($input->ifsc_code) ? $input->ifsc_code : '';
       $transaction_refno = isset($input->transaction_refno) ? $input->transaction_refno : '';
       $payment_date = isset($input->payment_date) ? $input->payment_date : '';
       $payment_remark = isset($input->payment_remark) ? $input->payment_remark : '';

       if(empty($user_id))
       {
       	  $msg = "please provide user_id";
       	  $status = false;
       }
       else if(empty($transaction_type))
       {
          $msg = "transaction_type required";
          $status = false;
       }
       else if(empty($payment_date))
       {
       	  $msg = "payment date required";
          $status = false;
       }
       else if($transaction_type == "cheque")
       {
	          if(empty($bank_name)){
	            $msg = "bank name required";
	            $status = false;
	          } 
	          else if(empty($bank_accno)) {
	            $msg = "bank_accno required";
	            $status = false;
	          }
	          else if(empty($branch_name)){
	            $msg = "branch_name required";
	            $status = false;
	          }  
       }
       else if($transaction_type == "netbanking") 
       {
       	   if(empty($ifsc_code))
       	   {
              $msg = "ifsc_code required";
	          $status = false;
       	   }
       	   else if(empty($transaction_refno))
       	   {
               $msg = "transaction_refno required";
	           $status = false;
       	   }
       	   else{}
       }
      else
      {
      	$userdata = $this->users_model->where('id',$user_id)->find_all();
      	if($userdata)
      	{
            $society_id = $userdata[0]->society_id;
            
            $this->bill_payment_model->user_id = $user_id;
            $this->bill_payment_model->society_id = $society_id;

            $comment = '';
			if($payment_remark)
			{
			   $comment = $payment_remark;	
			}
    	    if ($transaction_type = 'cheque' || $transaction_type ='netbanking') 
			{
				 
		         $this->bill_payment_model->bank_name = $bank_name;
		         $this->bill_payment_model->bank_acc_no = $bank_accno;
		         $this->bill_payment_model->branch_name = $branch_name;
		         $this->bill_payment_model->ifsc_code = $ifsc_code;
		         $this->bill_payment_model->transaction_ref_no = $transaction_refno;
			}
			$this->bill_payment_model->amount = $amount;
            $this->bill_payment_model->transaction_date = $payment_date;
            $this->bill_payment_model->transaction_type = $transaction_type;
            $this->bill_payment_model->payment_remark = $comment;
            $this->bill_payment_model->created_by = $user_id; 

            $bill_request = $this->bill_payment_model->save();
            if($bill_request)
            {
            	$msg = "bill request send successfully";
            	$status = true;
            }
            else
            {
            	$msg = "Unable to send request pls try later";
            	$status = false;
            }
      	}
      	else
      	{
      		$msg = "invalid user_id";
      		$status = false;
      	}
      }
    }
    else
    {
    	$msg = "no direct script access allowed";
    	$status = false;
    }
    $response['msg'] = $msg;
    $response['status'] = $status;
    data2json($response);
  }
  public function bill_request_list()
  {
  	$response = array();
    $msg = "";$status="";
    
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
       $input = json_decode(file_get_contents('php://input'));
       $society_id  = isset($input->society_id) ? $input->society_id : '';
       if($society_id)
       {
          $bill_list = $this->bill_payment_model->where('society_id',$society_id)->where('is_approved','Pending')->find_all();
          if($bill_list)
          {
            $response['data'] = $bill_list;
            $msg = "bill list retrive successfully";
            $status = true;
          }
          else
          {
            $msg = "Unable to fetch bill details";
            $status = false;
          }
       }
       else
       {
          $msg = "society_id required";
          $status = false;
       }
    }
    else
    {
    	$msg = "no direct script access allowed";
    	$status = false;
    }
    $response['msg'] = $msg;
    $response['status'] = $status;
    data2json($response);
  }
  public function bill_remark()
  {
  	$response = array();
    $msg = "";$status="";
    
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
       $input = json_decode(file_get_contents('php://input'));
       $bill_id  = isset($input->bill_id) ? $input->bill_id : '';
       $status = isset($input->status) ? $input->status : '';
       $remark = isset($input->remark) ? $input->remark : '';

       if(empty($bill_id)){
         $msg = "bill id required";
         $status = false;
       }
       elseif(empty($status)) {
       	 $msg = "status required";
       	 $status = false;
       }
       else
       {
           if($status == "approved"){
              $transaction_status = "success";
              $is_approved = "Approved";
           }else{
              $transaction_status = "rejected";
              $is_approved = "Rejected";
           }
           
           $data = array('transaction_status' => $transaction_status,
           	             'is_approved' =>$is_approved,
           	             'admin_remark'=> $remark);

		   $this->db->where('id', $bill_id);
		   
		   $update_bill_payment = $this->db->update('bill_payment', $data);  

		   if($update_bill_payment)
		   {
  		   	    $bill_details = $this->bill_payment_model->where('id',$bill_id)->find_all();
  		   	    $bill_details = $bill_details[0];
  		   	    $wallet_details = $this->wallet_model->where('user_id',$bill_details->user_id)->find_all();
            
            if($wallet_details)
            {
               $wallet_details = $wallet_details[0];
     	         $current_outstanding = $wallet_details->current_outstanding;
     	         $wallet_id = $wallet_details->id;

            }else
            {
                $current_outstanding = '0.00';
                $this->wallet_model->user_id = $bill_details->user_id;
                $this->wallet_model->current_outstanding = '0.00';

                $wallet_id  = $this->wallet_model->save();                  
            }
            $amount = $bill_details->amount;
		   	   
		   	    if($status == 'approved')
		   	    {
                    $wallet_transaction = array('user_id'=> $bill_details->user_id,
                    	                        'w_id'=> $wallet_id,
                    	                        'outstanding_amount_before'=>$current_outstanding,
                    	                        'amount' => $amount,
                    	                        'outstanding_amount_after' => $current_outstanding - $amount,
                    	                        'bill_payment_id'=> $bill_details->id
                    	                    );
                    $transaction_insertion_id = $this->wallet_transaction_model->insert($wallet_transaction);
                    
                    if($wallet_transaction)
                    {

                         $update_data = array('current_outstanding' => $current_outstanding - $amount);
          						   $this->db->where('user_id', $bill_details->user_id);
          						   $update_wallet = $this->db->update('wallet', $update_data);

          						   if($update_wallet)
          						   {
          						   	  $msg = "bill payment update successfully done";
          						   }
          						   else
          						   {
          						       $msg = "unable to proceed your request try again later";
          						   }
                  }
          }else
          {
            $msg = "successfully updated";
            $status = true;
          }
		    } 
      }  
    }
    else
    {
    	$msg = "no direct script access allowed";
    	$status = false;
    }
    $response['msg'] = $msg;
    $response['status'] = $status;
    data2json($response);
  }
  public function bill_generate(){
    $response=array();
    $msg = '';
    $status = '';
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $input = json_decode(file_get_contents('php://input'));
      $society_id = isset($input->society_id) ? $input->society_id : '';
      if($society_id)
      {
        $date = date('Y-m');
        $chk_monthly_bill_generation = $this->my_bill_model->where("DATE_FORMAT(created_at, '%Y-%m')=", date('Y-m', strtotime($date)))->where('society_id',$society_id)->find_all();

        if(empty($chk_monthly_bill_generation))
        {
           $user_data = $this->users_model->where('society_id',$society_id)->where('status','Y')->where('is_deleted','N')->where('role_id !=','2')->find_all();
           if($user_data)
           {
            foreach ($user_data as $key => $value) {

                
                      log_data('api/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $value, 'userdata');

                      $user_id = $value->id;

                      $house_id = $value->house_id;
                      $current_outstanding = '0.00';

                      $wallet_details = $this->wallet_model->where('user_id',$user_id)->find_all();
                    
                      if($wallet_details)
                      {
                          $current_outstanding = $wallet_details[0]->current_outstanding;

                      }
                      else
                      {
                        $this->wallet_model->user_id = $user_id;
                        $this->wallet_model->current_outstanding = '0.00';

                        $wallet_id  = $this->wallet_model->save();
                      }
                      log_data('api/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $wallet_details, 'wallet_details');

                      $fetch_house_Details = $this->house_master_model->where('id',$house_id)->find_all();


                      log_data('api/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $fetch_house_Details, 'house details');
                      if($fetch_house_Details)
                      {
                        $house_type=$fetch_house_Details[0]->house_type;

                         $main_amt = "0.00"; 

                         $house_type = $this->society_house_charges_model->where('id',$house_type)->find_all();

                         log_data('api/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $house_type, 'house type');

                         if($house_type)
                         {
                            $main_amt = $house_type[0]->maintenance_charges;

                            $parking_amt = '0.00';
                            $parking_details = $this->parking_model->where('user_id',$user_id)->find_all();
                           
                            if($parking_details)
                            {

                               $parking_amt = '';
                               if(sizeof($parking_details) > '1')
                               {
                                

                                  foreach ($parking_details as $key => $value) {
                                  
                                      $parking_master_details =$this->parkmaster_model->where('id',$value->pm_id)->find_all();
                                    
                                      $parking_charges_details = $this->parking_charges_model->where('id',$parking_master_details[0]->parking_charge_id)->find_all();
                                      $parking_amt+=$parking_charges_details[0]->parking_amt; 
                                  }
                               }
                               else
                               {
                                     $parking_master_details =$this->parkmaster_model->where('id',$parking_details[0]->pm_id)->find_all();
                                     
                                      $parking_charges_details = $this->parking_charges_model->where('id',$parking_master_details[0]->parking_charge_id)->find_all();

                                      $parking_amt = $parking_charges_details[0]->parking_amt;

                               }
                            }
                            log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $parking_details, 'parking_details');

                            log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $parking_amt, 'parking amount');

                            $facility_charges = '0.00';

                            $facility_details = $this->facility_model->where('user_id',$user_id)->find_all();
                           
                            if($facility_details)
                            {
                               $facility_charges = 0;
                               
                               if(sizeof($facility_details) > '1')
                               {
                                  foreach ($facility_details as $key => $value) {
                                      $facility_charges += $value->total_fees;

                                  }
                               }
                               else
                               {
                                  $facility_charges = $facility_details[0]->total_fees;

                               }
                            }
                            $other_charges = '0.00';
                            $total_amt = $main_amt + $parking_amt + $facility_charges;

                            log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $facility_details, 'facility_details');

                            log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $facility_charges, 'facility charges');
                            
                            $this->db->trans_begin();      
                            
                            $res=$this->my_bill_model->society_id = $society_id;
                            $this->my_bill_model->user_id = $user_id;
                            $this->my_bill_model->maintenance_amt = $main_amt;
                            $this->my_bill_model->parking_amt = $parking_amt;
                            $this->my_bill_model->other_charges = $other_charges;
                            $this->my_bill_model->facility_charges = $facility_charges;
                            $this->my_bill_model->total_amt = $total_amt;
                            $this->my_bill_model->outstanding_amt_before = $current_outstanding;
                            $res=$this->my_bill_model->outstanding_amt_after = $current_outstanding + $total_amt;
                            
                            $insert_my_bill = $this->my_bill_model->save();
                            $update_array = array();

                            if($insert_my_bill)
                            {
                                $update_array = array('current_outstanding'=> $current_outstanding + $total_amt);
                                $this->db->where('user_id', $user_id);
                                $update_wallet = $this->db->update('wallet', $update_array);

                                if($update_wallet)
                                {
                                    $this->db->trans_commit();
                                    $Bill_array[] =$user_id;
                                    $msg="Bill generated successfully";
                                    $status=true;
                                    $response['bill_generate_list']=$Bill_array;
                                }else{
                                    $this->db->trans_rollback();
                                    $msg="Error Occured";
                                }
                            }

                          }

                         // else
                         // {
                         //  $msg = "unable to fetch house type details"; 
                         // }
                      }
                      else
                      {
                        $msg = "unble to fetch house details";
                      }
                  }

           }
           else
           {
                      $msg ="error_log(message)";
           }
        }
        else{

          $msg="Bill Alredy generated";
          $status=false;

        }

       } else
       {

       }

    }else
    {
    $msg="No direct script allow";
    }
    $response['message'] = $msg;
    $response['status'] = $status;
    data2json($response);
  }
  public function my_bill(){
    $response=array();
    $msg = '';
    $status = '';
    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
      $input = json_decode(file_get_contents('php://input'));
      $user_id = isset($input->user_id) ? $input->user_id : '';

      if($user_id)
      {
        $date=date('Y-m');
        $bill_details= $this->my_bill_model->where("DATE_FORMAT(created_at, '%Y-%m')=", date('Y-m', strtotime($date)))->where('user_id',$user_id)->find_all();
        if($bill_details)
        {
          $response['bill_details']=$bill_details;
          $status=true;
        }
        else{
          $msg="Bill not generated Yet";
          $status=false;
        }
      }else
      {
        $msg = "please provide valid user id";
        $status = false;
      }
         }
         else
     {
        $msg = "no direct script access allowed";
        $status = false;
     }
    $response['message'] = $msg;
    $response['status'] = $status;

    data2json($response);
}
}

?>
