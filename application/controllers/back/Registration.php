<?php

class Registration extends MY_Controller {

    public function __construct() {
        parent::__construct();

        is_logged_in();

        $this->load->model(array('users_model','society_master_model','house_master_model','society_house_charges_model','facility_master_model','member_master_model','society_master_model','parking_model'));
    }
    public function userView() {
      $facility_name=$this->facility_master_model->where('society_id',$this->session->userdata('society_id'))->find_all();
      $data['facility_name']=$facility_name;

      $member_auth=$this->member_master_model->find_all();
      $data['authority']=$member_auth;

      $parking_slot = $this->db->query('SELECT id,slot_no FROM parking_master WHERE id NOT IN (SELECT pm_id FROM parking)')->result();
     $data['park_slot'] = $parking_slot;

        $data['society'] = $this->society_master_model->allSociety();
        $society_id = $this->session->userdata('society_id');
        $data['h_option'] =  $this->users_model->houseSociety($society_id);
        load_back_view('admin/user/userView',$data);
    }
    public function houseSociety() {
            $society_id = $this->input->post('society_id');
            $option =  $this->users_model->houseSociety($society_id);
            $house_option="";
            foreach($option as $opt) {
                $house_option .= "<option value=$opt->id>$opt->wing $opt->block</option>";
            }
            echo ($house_option);
        }
   
    public function addUser() {
       
      $role = $this->session->userdata('role_id');
      $ses_scid = $this->session->userdata('society_id');
         if($role!= SUPERADMIN){
              $scid =  $ses_scid;
            } else {
              $scid =  $input['society_name'];
            }
      $input = $this->input->post();

      if($input && !empty($input)) {
      $pic = $_FILES['profile_pic']['name'];
      $size = $_FILES['profile_pic']['size'];
      $ext = pathinfo($pic, PATHINFO_EXTENSION);
      $allowed =  array('gif','png' ,'jpg','jpeg','');
        if(!in_array($ext,$allowed)) {
        show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
        }
        else {
        if($size>'2000000') {
          show('Max Allowed Image Size is 2MB',1);
        }
        else {
          $first_name   = $input['first_name'];
          $tpname=explode('.', $pic);
            $pic=$tpname[0].'_'.$first_name.'.'.$tpname[1];
            $tmp_name = $_FILES['profile_pic']['tmp_name'];
            $path = $_SERVER['DOCUMENT_ROOT'].'/upload/profileImages';
            move_uploaded_file($tmp_name, $path.'/'.$pic);
        
        $salt       = getRandomId(8);
        $pass       =  $input['password'];
        $password   =  md5($salt.$pass);

       $first_name  = $input['first_name'];
       $last_name   = $input['last_name'];
       $role_id     = $input['user_role'];
       $username    = $input['username'];
       $email       = $input['email'];
       $mobile      = $input['mobile'];
       if($role==SUPERADMIN){
       $society_id  = $input['society_name'];
        } else {
          $society_id = $ses_scid;
        }
       $house_id    = $input['house_id'];
       $birth_date  = $input['birth_date'];
       $no_of_member= $input['total_family_member'];
      $authority_id= $input['authority'];

       $data  = array(
      'role_id'             => $role_id,
      'first_name'          => $first_name,
      'last_name'           => $last_name,
      'username'            => $username,
      'password'            => $password,
      'email'               => $email,
      'phone'               => $mobile,
      'birth_date'          => $birth_date,
      'profile_pic'         => $pic,
      'total_family_member' => $no_of_member,
      'status'              => 'Y',
      'is_deleted'          =>'N',
      'salt'                => $salt,
      'society_id'          => $scid,
      'house_id'            => $house_id,
      'authority_id'       => $authority_id,
      'updated_by'          => $this->session->userdata('id'),
      'updated_date'        => date ('Y-m-d H:i:s'),
      'created_by'          => $this->session->userdata('id'),
      'created_date'        => date ('Y-m-d H:i:s')
    );
       
    $house_id   = $input['house_id'];
    
      if(!empty($first_name) && !empty($last_name) && !empty($username) && !empty($password) && !empty($email) && !empty($mobile) && !empty($society_id) && !empty($house_id)){
        
        $added = $this->users_model->addUser($data,$house_id);

          if($added)
            {              
              $society_details = $this->society_master_model->where('id',$society_id)->find_all();
               $society_name = $society_details[0]->name;
               $data=$this->users_model->where('id',$added)->find_all();
               $id=$data[0]->id;

              //  $park_slot=isset($input['park_slot']) ? $input['park_slot'] : '';

              // $society_id=$this->session->userdata('society_id');
              // $user_id=$this->session->userdata('user_id');

              // if($park_slot)
              // {
              //    $this->parking_model->pm_id = $park_slot;
              //    $this->parking_model->user_id = $added;
              //    $this->parking_model->society_id = $society_id;
              //    $this->parking_model->created_by = $user_id;
              //    $insert_parking = $this->parking_model->save(); 
              // }
              // $facility_id = isset($input['facility']) ? $input['facility'] : '';

              //   if($facility_id)
              // {
              //      $master=$this->db->get_where('facility_master',array('id'=>$facility_id,'society_id'=>$this->session->userdata('society_id')))->result();
              //    $adult_charges=$master[0]->adult_charges;
              //    $child_charges=$master[0]->child_charges;
              //    $total_adult_charges=($this->input->post('adult_count')* $adult_charges);
              //    $total_child_charges=($this->input->post('child_count')* $child_charges);

              //     $f_data = array(
              //     'society_id' => $this->session->userdata('society_id'),
              //     'facilty_id'=> $facility_id,
              //     'user_id'=>  $added,
              //     'adult_count'=> $this->input->post('adult_count'),
              //     'child_count'=> $this->input->post('child_count'),
              //     'total_member'=> $this->input->post('adult_count') + $this->input->post('child_count'),
              //     'created_by'=> $this->session->userdata('user_id'),
              //     'total_fees'=>  ($total_adult_charges+ $total_child_charges)
              //   );
              //   $insert_facility = $this->db->insert('facilty',$f_data);
              // }

              
              $first_name=$data[0]->first_name;
               $last_name=$data[0]->last_name;
               $full_name=$first_name." ".$last_name;

              $mail_replacement_array = array("{{fullname}}" => $first_name." ".$last_name,
                                                        "{{society_name}}" => $society_name,
                                                        "{{username}}" => $username,
                                                        "{{password}}" => $pass,
                                                       );
                        
              $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),SOCIETY_MEMBER_REGISTRATION);
              
              $params = array(
                    'to'        =>  $email,
                    'subject'   => 'Login Credentials',
                    'html'      =>  $data['mail_content'],
                    'from'      =>  ADMIN_EMAIL,
                );
              $response = sendMail($params);
              
              $data = array(
                    'user_id' => $added,
                    'current_outstanding' => '0.00',
                    'wallet_balance' => '0.00'
             );
              //sms after registration
              if($mobile=='') {
                log_data('sms/' .  date('d-m-Y'). '/username' . '.log', $username, 'username');
              } 
              else {
              $snm = strtoupper($society_data[0]->name);
               $name  = ucfirst($first_name);
               $msg ="Congratulations ".$name.", your login credentials is username: ".$username." password: ".$pass." login link https://hamarisociety.co.in/dashboard thank you"; 
             
                // $messages = array(
                // 'sender' => "TXTLCL",
                // 'messages' => array(
                //     array(
                //         'number' => $mobile,
                //         'text' => rawurlencode($msg)
                //      )
                //     )
                // );
                // $data = array (
                // 'apikey' => SMSAPI,
                // 'data' => json_encode($messages)
                // );
                // $response = SendDataOverPost(SMSURL,$data);
                // $smsres = json_decode($response);
                   $smsres = sendSms($mobile,$msg);
              }
             
             $this->db->insert('wallet', $data);
             $flash_msg='';
             if(!isset($smsres->messages_not_sent)) {
             $flash_msg=" and SMS send";
             }
             $this->session->set_flashdata('msg', "New User Created As Well As House Allocated $flash_msg successfully");
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            } 
        } else {
              
              $this->session->set_flashdata('msg', 'Fields Mark with * Can not be empty');
              $this->session->set_flashdata('msg_type', 'danger'); 
              redirect(base_url().'back/registration/userView');  
            }
         }      
      }
    }

        redirect(base_url().'back/registration/alluser');   
  }

    public function houseExist() {
      $input = $this->input->post();
      $res='';
      if($input) {
        $building  = $input['building']; 
        $wing      = $input['wing']; 
        $block     = $input['block'];
      $res = $this->house_master_model->houseExist($block,$wing,$building);
      }
      return $res;
    }
    public function userExist() {
        $username = $this->input->post('user_name');
        $res    = $this->users_model->userExist($username);
        return $res;
    }

    public function editUser($id) {

        $ses_id =$this->session->userdata('id');
        $ses_roleid =$this->session->userdata('role_id');
        if($ses_id==$id || $ses_roleid==SUPERADMIN || $ses_roleid==SOCIETY_SUPERUSER || $ses_roleid==SOCIETY_ADMIN) {
        $society_id = $this->session->userdata('society_id');
        $data['authority']=$this->member_master_model->find_all();
        $data['society'] = $this->society_master_model->allSociety();
        $data['h_option'] =  $this->users_model->houseSociety($society_id);
        $data['user'] = $this->users_model->editUser($id);
        load_back_view('admin/user/userEdit',$data);
      }
      else {
        redirect(base_url().'admin/dashboard');
      }
    }
    public function updateUser() {


      $role = $this->session->userdata('role_id');
      $ses_scid = $this->session->userdata('society_id');
       if($role!= SUPERADMIN){
            $scid =  $ses_scid;
          } else {
            $scid =  $input['society_name'];
          }
       $input = $this->input->post();
       
        if($input && !empty($input)) {
        $pic = $_FILES['profile_pic']['name'];
        if($pic==""){
          $pic = $input['user_img'];
        }
        $size = $_FILES['profile_pic']['size'];
        $ext = pathinfo($pic, PATHINFO_EXTENSION);
        $allowed =  array('gif','png' ,'jpg','jpeg','');
          if(!in_array($ext,$allowed)) {
           show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
          }
          else {
          if($size>'2000000') {
            show('Max Allowed Image Size is 2MB',1);
          }
          else {
            $username   = $input['username'];
            if($_FILES['profile_pic']['name']!=""){
            $tpname=explode('.', $pic);
              $pic=$tpname[0].'_'.$username.'.'.$tpname[1];
              $tmp_name = $_FILES['profile_pic']['tmp_name'];
            }
              $path = $_SERVER['DOCUMENT_ROOT'].'/dashboard/upload/profileImages';
              move_uploaded_file($tmp_name, $path.'/'.$pic);

        $authority_id = $input['authority'];
         $first_name  = $input['first_name'];
         $last_name   = $input['last_name'];
         $role_id     = $input['user_role'];
         $email       = $input['email'];
         $mobile      = $input['mobile'];
         $house_id    = $input['house_id'];
         $birth_date  = $input['birth_date'];
         $no_of_member= $input['total_family_member'];
         
         if($input['status'] == 1){
               $status = 'Y';
         }else{
               $status = 'N';
         }

         $user_data  = array(
        'role_id'             => $role_id,
        'first_name'          => $first_name,
        'last_name'           => $last_name,
        'username'            => $username,
        'email'               => $email,
        'phone'               => $mobile,
        'birth_date'          => $birth_date,
        'profile_pic'         => $pic,
        'total_family_member' => $no_of_member,
        'status'              => $status,
        'is_deleted'          =>'N',
        'society_id'          => $scid,
        'house_id'            => $house_id,
        'authority_id '       => $authority_id,
        'updated_by'          => $this->session->userdata('id'),
        'updated_date'        => date ('Y-m-d H:i:s'),
        'created_by'          => $this->session->userdata('id'),
        'created_date'        => date ('Y-m-d H:i:s')
      );
         
          $uid = $this->input->post('uid');
          /*Unlink Image*/
          $unlink_img = $this->db->where('id',$uid)->get('users')->result();
          $unlink_img = $unlink_img[0]->profile_pic;
          if($_FILES['profile_pic']['name']!=""){
             unlink("upload/profileImages/".$unlink_img);
          }
           if(!empty($first_name) && !empty($last_name) && !empty($username) &&  !empty($email) && !empty($mobile) && !empty($scid) && !empty($house_id)){
            
          $updated = $this->users_model->updateUser($uid,$user_data);
             if($updated)
            {
              $this->db->where('id',$house_id);
              $this->db->update('house_master',array('allocated' => 'Y'));

              $old_house = $input['old_house'];
              $this->db->where('id',$old_house);
              $this->db->update('house_master',array('allocated' => 'N'));

             $this->session->set_flashdata('msg', 'User Details Updated Successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            } 
          } else {
              $this->session->set_flashdata('msg', 'Fields Mark With * Can not be Empty');
              $this->session->set_flashdata('msg_type', 'danger');   
           }      
          }
      }
    }
      redirect(base_url().'back/registration/alluser');  
    }
    public function deleteUser($id) {
        $ses_role = $this->session->userdata('role_id');
        $deleted = $this->users_model->deleteUser($id);
         if($deleted)
            {
              $this->house_master_model->deAllocat_house($id);

             $this->session->set_flashdata('msg', 'User Deleted Successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            } 
        redirect(base_url().'back/registration/alluser');

    }
    public function allUser() {

        $result= $this->users_model->allUser();
        $data['user']=$result;
       
        load_back_view('admin/user/allUser',$data);
    }
    public function showUser($id) {

        $data['user'] = $this->users_model->showUser($id);
        load_back_view('admin/user/showUser',$data);
    }
    public function societyView() {

        $data['state'] = $this->society_master_model->selectState();
        load_back_view('admin/society/societyView',$data);
    }
    public function selectCity() {
        $state_id = $this->input->post('val');
        $response = $this->society_master_model->selectCity($state_id);
        $op = "";
        foreach($response as $row){
        $op .= "<option value=".$row->intCityId.">".$row->stCity."</option>";
        }
        echo ($op);
    }
    public function allSociety() {

        $data['society'] = $this->society_master_model->allSociety();
        load_back_view('admin/society/allSociety',$data);
    }
    public function addSociety() {

      $input = $this->input->post();
      
      if($input && !empty($input)) {
      $pic = $_FILES['societypic']['name'];
      $size = $_FILES['societypic']['size'];
      $ext = pathinfo($pic, PATHINFO_EXTENSION);
      $allowed =  array('gif','png' ,'jpg','jpeg','');
        if(!in_array($ext,$allowed) ) {
         show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
        }
        else {
        if($size>'2000000') {
          show('Max Allowed Image Size is 2MB',1);
        }
        else {
          $society_name = $input['society_name'];
          $code         = 'SC'.getRandomId(5);
          $tpname       =  explode('.', $pic);
          $pic          =  $tpname[0].'_'.$code.'.'.$tpname[1];
          
          $tmp_name     = $_FILES['societypic']['tmp_name'];
          $path         = $_SERVER['DOCUMENT_ROOT'].'/dashboard/upload/societyImages';
          
          move_uploaded_file($tmp_name, $path.'/'.$pic);
            
          $path1 = $_SERVER['DOCUMENT_ROOT'].'/dashboard/upload/profileImages';
          copy($path.'/'.$pic, $path1.'/'.$pic);
        
          $society_data  = array(
          'name'      => $society_name,
          'code'      => 'SC'.getRandomId(5),
          'no_of_house' =>  $input['no_of_house'],
          'address'   =>  $input['address'],
          'state'     =>  $input['state'],
          'city'      =>  $input['city'],
          'pincode'   =>  $input['pincode'],
          'image'     =>  $pic,
          'created_by'  =>  $this->session->userdata('id'),
          'created_date'  =>  date('Y-m-d H:m:i'),
          'updated_by'  =>  $this->session->userdata('id'),
          'updated_date'  =>  date('Y-m-d H:m:i')
        );   
          
         $this->society_master_model->addSociety($society_data);
          /*Society Created*/

         $salt  = getRandomId(8);
          $s_uname = $input['username'];
          $s_upass = $input['password'];
           $pass  =  $s_upass;
           $pass  =  md5($salt.$pass);

           $mobile        = $input['mobile'];

           $this->db->select('id as society_id');
           $this->db->from('society_master');
           $this->db->order_by('id','DESC');
           $this->db->limit(1);
           $res=$this->db->get()->result();
           $us_id = $res[0]->society_id;
          
        $society_user  = array(
                'role_id'       => SOCIETY_SUPERUSER,
                'first_name'    => $society_name,
                'last_name'     => 'Admin',
                'email'         =>  $input['email'],
                'phone'         =>  $input['mobile'],
                'username'      => $s_uname,
                'salt'          => $salt,
                'password'      => $pass,
                'profile_pic'   => $pic,
                'status'        => 'Y',
                'is_deleted'    => 'N',
                'society_id'    => $us_id,
                'house_id'      => '0',
                'updated_by'    => $this->session->userdata('id'),
                'updated_date'  =>  date('Y-m-d H:m:i'),
                'created_by'    =>  $this->session->userdata('id'),
                'created_date'  =>  date('Y-m-d H:m:i'),
                 );

        $inserted= $this->society_master_model->addSocietyUser($society_user);
        if($inserted)
            {
     //start send Sms         
               if($mobile=='') 

               {

                  log_data('sms/' .  date('d-m-Y'). '/username' . '.log', $s_uname, 'username');
               } 
              else {
                        $snm = strtoupper($society_name);
                        $fullname = $society_name;
                        $fullname  = ucfirst($fullname);
                        $msg ="Congratulations ".$fullname.", your login credentials is username: ".$s_uname." password: ".$s_upass." login link https://hamarisociety.co.in/dashboard thank you"; 
                        
                        $smsres = sendSms($mobile,$msg);
                        
                        $flash_msg='';
                         if(!isset($smsres->messages_not_sent)) {
                        
                          $flash_msg=" and SMS send";
                          
                         }
                  }
    //end Send Sms

                  //  start send mail
                  $mail_replacement_array = array("{{society_name}}" => $society_name,
                                                        "{{username}}" => $s_uname,
                                                        "{{password}}" => $s_upass,
                                                       );
                        
                  $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),SOCIETY_REGISTRATION);
                  $params = array(
                        'to'        =>  $input['email'],
                        'subject'   => 'Society Login Credentials',
                        'html'      =>  $data['mail_content'],
                        'from'      =>  ADMIN_EMAIL,
                    );
                  $response = sendMail($params);
                  //  end send mail
             $this->session->set_flashdata('msg', "Society created ".$flash_msg." successfully");
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            } 
         }      
    }
  }

      redirect(base_url().'back/registration/allSociety');    
    }
    public function showSociety($id) {

        $data['society'] = $this->society_master_model->showsociety($id);
        load_back_view('admin/society/showSociety',$data);
    }
    public function editSociety($id) {
        $data['state'] = $this->society_master_model->selectState();
        $res = $this->society_master_model->getData($id);
        $data['society'] = $res;
        $state_id    = $res[0]->intStateId;
        $data['city'] = $this->society_master_model->getCity($state_id);
        load_back_view('admin/society/societyEdit',$data);
    }
    public function updateSociety() {

      $input = $this->input->post();
      if($input && !empty($input)) {
      $pic = $_FILES['societypic']['name'];
      if($pic==""){
        $pic = $input['sc_img'];
      }
      $size = $_FILES['societypic']['size'];
      $ext = pathinfo($pic, PATHINFO_EXTENSION);
      $allowed =  array('gif','png' ,'jpg','jpeg','');
        if(!in_array($ext,$allowed)) {
         show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
        }
        else {
        if($size>'2000000') {
          show('Max Allowed Image Size is 2MB',1);
        }
        else {
          $society_name   = $input['society_name'];
          if($_FILES['societypic']['name']!=""){
          $tpname=explode('.', $pic);
            $pic=$tpname[0].'_'.getRandomId(5).'.'.$tpname[1];
            
            $tmp_name = $_FILES['societypic']['tmp_name'];
          }
            $path = $_SERVER['DOCUMENT_ROOT'].'/dashboard/upload/societyImages';
            move_uploaded_file($tmp_name, $path.'/'.$pic);
        
          $society_data  = array(
          'name'      => $society_name,
          'code'      => 'SC'.getRandomId(5),
          'no_of_house' =>  $input['no_of_house'],
          'address'   =>  $input['address'],
          'state'     =>  $input['state'],
          'city'      =>  $input['city'],
          'pincode'   =>  $input['pincode'],
          'image'     =>  $pic,
          'created_by'  =>  $this->session->userdata('id'),
          'created_date'  =>  date('Y-m-d H:m:i'),
          'updated_by'  =>  $this->session->userdata('id'),
          'updated_date'  =>  date('Y-m-d H:m:i')
        );   
        $society_id = $input['society_id'];
         /*unlink image*/
         $unlink_img = $this->db->where('id',$society_id)->get('society_master')->result();
          $unlink_img = $unlink_img[0]->image;
          if($_FILES['societypic']['name']!=""){
             unlink("upload/societyImages/".$unlink_img);
          }
        $updated = $this->society_master_model->updateSociety($society_data,$society_id);
         if($updated)
            {
             $this->session->set_flashdata('msg', 'Society updated successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            } 
         }      
       }
      }
        redirect(base_url().'back/registration/allSociety');   
    }
    public function deleteSociety($id) {
        $deleted = $this->society_master_model->deleteSociety($id);
        if($deleted)
            {
             $this->session->set_flashdata('msg', 'Society deleted successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            } 
        redirect(base_url().'back/registration/allsociety');
    }
    public function houseView() {
        $sess_id = $this->session->userdata('role_id');
        $sess_scid = $this->session->userdata('society_id');
        $data['houseType'] = $this->society_house_charges_model->where('society_id',$sess_scid)->find_all();

        if($sess_id==SUPERADMIN) {
        $data['society'] = $this->society_master_model->find_all();
        } else {
          $data['society'] = $this->society_master_model->where('id',$sess_scid)->find_all();
        }
        $this->load->model('society_house_charges_model');
        $check_house_type_Added = $this->society_house_charges_model->where('society_id',$sess_scid)->find_all();

        
        /* home limit exceed start*/
        $society_details = $this->society_master_model->where('id',$sess_scid)->find_all();
        $house_allover = $society_details[0]->no_of_house;
        $house_details = $this->house_master_model->column('count(id) as total_house')->where('society_id',$sess_scid)->where('is_deleted','N')->find_all();
        $house_created = $house_details[0]->total_house;
        
        if($house_created >= $house_allover)
        {
           $this->session->set_flashdata('msg','You can not create house ..total house limit exceed');
           $this->session->set_flashdata('msg_type','danger');
           redirect(base_url().'admin/dashboard');
        }
        /* home limit exceed end*/
        if($check_house_type_Added)
        {
           load_back_view('admin/house/houseView',$data);
        }
        else
        {
          redirect(base_url().'back/societycharges');
        }
    }
    public function addHouse() {

     $role = $this->session->userdata('role_id');
      $ses_scid = $this->session->userdata('society_id');
         if($role!= SUPERADMIN){
              $scid =  $ses_scid;
            } else {
              $scid =  $input['society_name'];
            }
     $input = $this->input->post();
     if($input && !empty($input)) {
     $pic = $_FILES['house_pic']['name'];
     $size = $_FILES['house_pic']['size'];
     $ext = pathinfo($pic, PATHINFO_EXTENSION);
     $allowed =  array('gif','png' ,'jpg','jpeg','');
       if(!in_array($ext,$allowed) ) {
        show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
       }
       else {
             if($size>'2000000') {
                 show('Max Allowed Image Size is 2MB',1);
             }
             else {
                 $block  = $input['block'];
             $tpname=explode('.', $pic);
             $pic=$tpname[0].'_'.$block.'.'.$tpname[1];
             $tmp_name = $_FILES['house_pic']['tmp_name'];
                 $path = $_SERVER['DOCUMENT_ROOT'].'/dashboard/upload/houseImages';
             move_uploaded_file($tmp_name, $path.'/'.$pic);
                
                 $house_data  = array(
                 'society_id'        =>  $scid,
                 'building'          =>  $input['building'],
                 'wing'              =>  $input['wing'],
                 'block'             =>  $input['block'],
                 'house_type'        =>  $input['house_type'],
                 'details'           =>  $input['details'],
                 'is_deleted'        =>  'N',
                 'on_lease'          =>  $input['on_lease'],
                 'house_picture'     =>  $pic,
                 'created_by'        =>  $this->session->userdata('id'),
                 'created_date'      =>  date('Y-m-d H:m:i'),
                 'updated_by'        =>  $this->session->userdata('id'),
                 'updated_date'      =>  date('Y-m-d H:m:i')
             );   
            
                
                }           
            }
    }
        $inserted = $this->house_master_model->addHouse($house_data);
        if($inserted)
            {
             $this->session->set_flashdata('msg', 'House Added Successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            }       
        redirect(base_url().'back/registration/allhouse');  
    }
    public function editHouse($id) {

        //$data['society'] = $this->society_master_model->allSociety();
      $sess_scid = $this->session->userdata('society_id');
      $data['houseType'] = $this->society_house_charges_model->editHouseType($id,$sess_scid);

        $data['house'] = $this->house_master_model->editHouse($id,$sess_scid);
         
        load_back_view('admin/house/editHouse',$data);

    }
    public function updateHouse() {
      
        $role = $this->session->userdata('role_id');
        $ses_scid = $this->session->userdata('society_id');
           
           if($role!= SUPERADMIN)
           {
                $scid =  $ses_scid;
              } 
              else 
              {
                $scid =  $input['society_name'];
              }

        $input = $this->input->post();
       
        if($input && !empty($input))
         {
        $pic = $_FILES['house_pic']['name'];
        
        if($pic=="")
        {
          $pic = $input['house_img'];
        }
        $size = $_FILES['house_pic']['size'];
        $ext = pathinfo($pic, PATHINFO_EXTENSION);
        $allowed =  array('gif','png' ,'jpg','jpeg','');
          if(!in_array($ext,$allowed) ) {
           show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
          }
          else {
            if($size>'2000000') {
                show('Max Allowed Image Size is 2MB',1);
            }
            else {
                $house  = $input['wing'].$input['block'];
                if($_FILES['house_pic']['name']!=""){
                $tpname=explode('.', $pic);
                $pic=$tpname[0].'_'.getRandomId(5).'.'.$tpname[1];
                $tmp_name = $_FILES['house_pic']['tmp_name'];
              }
                $path = $_SERVER['DOCUMENT_ROOT'].'/dashboard/upload/houseImages';
                move_uploaded_file($tmp_name, $path.'/'.$pic);
                    
                    $building        =  $input['building'];
                    $wing            =  $input['wing'];
                    $block           =  $input['block'];

                    $house_data  = array(
                    'society_id'      =>  $scid,
                    'building'        =>  $building,
                    'wing'            =>  $wing,
                    'block'           =>  $block,
                    'house_type'      =>  $input['house_type'],
                    'on_lease'        =>  $input['on_lease'],
                    'details'         =>  $input['details'],
                    'is_deleted'      =>  'N',
                    'house_picture'   =>  $pic,
                    'created_by'      =>  $this->session->userdata('id'),
                    'created_date'    =>  date('Y-m-d H:m:i'),
                    'updated_by'      =>  $this->session->userdata('id'),
                    'updated_date'    =>  date('Y-m-d H:m:i')
                );   
                $hid = $input['hid'];
         }          
        }
        /*unlink image*/
         $unlink_img = $this->db->where('id',$hid)->get('house_master')->result();
          $unlink_img = $unlink_img[0]->house_picture;
          if($_FILES['house_picture']['name']!="")
          {
             unlink("upload/houseImages/".$unlink_img);
          }
        $updated = $this->house_master_model->updateHouse($hid,$house_data);
        if($updated)
            {

             $this->session->set_flashdata('msg', 'House Updated Successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            }       
        redirect(base_url().'back/registration/allhouse');  
    }
  }
    public function deleteHouse($id) {
        $deleted = $this->house_master_model->deleteHouse($id);

        if($deleted)
            {
             $this->session->set_flashdata('msg', 'House deleted successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            }       
        redirect(base_url().'back/registration/allhouse'); 
    }
    public function allHouse() {

        $data['house'] = $this->house_master_model->allHouse();
        load_back_view('admin/house/allHouse',$data);
    }
    public function showHouse($id) {

        $data['house'] = $this->house_master_model->showHouse($id);
        load_back_view('admin/house/showHouse',$data);
    }

        /* User Report*/
    // public function userReportView() {

    //     $data['role'] = $this->users_model->roleData();
    //     $data['user'] = $this->users_model->userReport();
    //     load_back_view('admin/Reports/userReport',$data);
    // }
    // public function userReportData() {
    //     $res = $this->users_model->userReport();
    //     echo json_encode($res);
    // }
    public function houseCsvView() {
      load_back_view('admin/house/houseCsv');
    }
    public function houseCsv() {

      $ses_id = $this->session->userdata('id');
      $inserted=''; 
      $invali_tbl='';
     if(isset($_POST["house"]))
      {
        $filename=$_FILES["file"]["tmp_name"];
         $csvname=$_FILES['file']['name'];
          $mystr=explode('.', $csvname);
            $mydt=strtolower($mystr[1]);
            
      if($_FILES["file"]["size"] > '0' && $mydt=='csv')
        {
          $file = fopen($filename, "r");
          $count = 0;
           while (($importdata = fgetcsv($file, 10000, ",")) !== FALSE)
           {
             
              $society_name = trim($importdata[0]);
              $building     = trim($importdata[1]);
              $wing         = trim($importdata[2]);
              $block        = trim($importdata[3]);
              $house_type   = trim($importdata[4]);
              $on_lease     = trim($importdata[5]);
              $details      = trim($importdata[6]);

               if($count==0) 
               {

                   if($society_name!='Society Name' && $building!='Building' && $wing!='Wing' && $block!='Block / Flat' && $house_type!='House Type' && $on_lease!='On Lease (Y/N)' && $details!='Details')
                   {
                    show('Invalid CSV Or CSV Headers',1);
                   }
               }
                $count++;
              
               if($count=='1')
                   {
                      continue;
                   }

            $society_id = $this->db->select('id')->where('name',$society_name)->get('society_master')->row()->id;

            $this->db->select('count(h.id) as c_hid,sm.no_of_house');
            $this->db->from('society_master sm');
            $this->db->join('house_master h','h.society_id=sm.id');
            $this->db->where(array('sm.id'=>$society_id,));
            $h_count = $this->db->get()->result();

            if($h_count)
            {
              if($h_count[0]->no_of_house < $h_count[0]->c_hid) {
                $base_url1 =base_url();
                echo "<center> <strong style='color:red;'>House Can not be more than the specified house in society <a href=".$base_url1."/back/registration/houseCsvView>back</a></strong></center>";exit();
                
              }
            }

            $h_type =  array('society_id' => $society_id,'house_type'=>$house_type);
            
            $hs_type = $this->db->select('id')->where($h_type)->get('society_house_charges')->row()->id;
            
          $validate_csv =array('society_id'=>$society_id,'building'=>$building,'wing'=>$wing,'block'=>$block);

          $invalid_csv = $this->house_master_model->validateHouseCsv($validate_csv);
          if($invalid_csv) {
            show('<h1>This House Is Already Exist, So Can not be duplicated</h1>');
            foreach($invalid_csv as $i_csv) {
              $invali_tbl .="<tr>
                              <td>Building</td>
                              <td>$i_csv->building</td>
                              </tr>
                              <tr>
                              <td>Wing</td>
                              <td>$i_csv->wing</td>
                              </tr>
                              <tr>
                              <td>Block</td>
                              <td>$i_csv->block</td>
                            </tr>";
            }
            show($invali_tbl,1);
          } else {

                  $data = array(
                      'society_id'    =>  $society_id,
                      'building'      =>  $building,
                      'wing'          =>  $wing,
                      'block'         =>  $block,
                      'house_type'    =>  $hs_type,
                      'on_lease'      =>  $on_lease,
                      'allocated'     =>  'N',
                      'details'       =>  $details,
                      'is_deleted'    =>  'N',
                      'house_picture' =>  'img1.png',
                      'updated_by'    =>  $ses_id,
                      'updated_date'  =>  date('Y-m-d H:m:s'),
                      'created_by'    =>  $ses_id,
                      'created_date'  =>  date('Y-m-d H:m:s')
                      );
           $inserted = $this->house_master_model->houseCSV($data);
           }    
        }   
          fclose($file);
        if($inserted) {
             $this->session->set_flashdata('msg', 'House Inserted Successfully');
             $this->session->set_flashdata('msg_type', 'success');   
             redirect(base_url().'back/registration/allhouse');
            }
        }
        else{
          if( $mydt!='csv'){
            
              $this->session->set_flashdata('msg', 'Only extention csv Allowed');
              $this->session->set_flashdata('msg_type', 'danger');  
              redirect(base_url().'back/registration/houseCsvView');
              
          } else{

              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');  
              redirect(base_url().'back/registration/houseCsvView');
          }
    }
  }
}
    public function userCsvView() {
      load_back_view('admin/user/userCsv');
    }
    public function userCsv() {

      $ses_id = $this->session->userdata('id');
      $inserted=''; 
      $invalid_csv='';
      $invali_tbl='';
     if(isset($_POST["user"]))
      {
        $filename=$_FILES["file"]["tmp_name"];
         $csvname=$_FILES['file']['name'];
          $mystr=explode('.', $csvname);
            $mydt=strtolower($mystr[1]);
      if($_FILES["file"]["size"] > 0 && $mydt=='csv')
        {
          $file = fopen($filename, "r");
          $count = 0;
           while (($importdata = fgetcsv($file, 10000, ",")) !== FALSE)
           {  
             
                $society_name = trim($importdata[0]);
                $building     = trim($importdata[1]);
                $wing         = trim($importdata[2]);
                $block        = trim($importdata[3]);
                $authority    = trim($importdata[4]);
                $role_name    = trim($importdata[5]);
                $first_name   = trim($importdata[6]);
                $last_name    = trim($importdata[7]);
                $username     = trim($importdata[8]);
                $password     = trim($importdata[9]);
                $email        = trim($importdata[10]);
                $phone        = trim($importdata[11]);
                $birth_date   = trim($importdata[12]);
                $total_member = trim($importdata[13]);


              if($count==0) 
               {

                   if($society_name!='society name' && $building!='Building' && $wing!='wing' && $block!='wing' && $authority!='authority' && $role_name!='role' && $first_name!='first_name' && $last_name!='first_name' &&
                  $username!='username' && $password!='password' && $email!='email' && $phone!='phone')
                   {
                      show('Invalid CSV Or CSV Headers',1);
                   }
               }
                  $count++;
              
                 if($count=='1')
                     {
                        continue;
                     }

                $user_dd = $this->db->where('username',$username)->get('users')->result();

                if($user_dd) {

                  show('Username '.$username.' is Already Exist Try agin with diffrent username',1);
                }
                
                $authority    = strtolower($authority);
                if(empty($authority))
                {
                  $authority_id = '0';
                }
                $authority_dd = $this->db->where('name',$authority)->get('member_authority')->result();

                if(!$authority_dd && $authority_id!='0') 
                {

                  show("Invalid Authority Provided",1);
                }
                else
                {
                   if(!empty($authority))
                    {
                      $authority_id = $authority_dd[0]->id;
                    }
                // $validate_csv =array('username'=>$username,'email'=>$email,'phone'=>$phone);

                // $invalid_csv = $this->users_model->validateUserCsv($validate_csv);
                // if($invalid_csv) {
                //   show('<h1>Email, phone and username can not be duplicated</h1>');
                //   foreach($invalid_csv as $i_csv) {
                //     $invali_tbl .="<tr>
                //                     <td>Username</td>
                //                     <td>$i_csv->username</td>
                //                     </tr>
                //                     <tr>
                //                     <td>Email</td>
                //                     <td>$i_csv->email</td>
                //                     </tr>
                //                     <tr>
                //                     <td>Phone</td>
                //                     <td>$i_csv->phone</td>
                //                   </tr>";
                //   }
                //   show($invali_tbl,1);
                // } else {
            
                 $society_id  = $this->db->select('id')->where('name',$society_name)->get('society_master')->row()->id;

                $house_data = array(
                  'society_id' => $society_id,
                  'building'   => $building,
                  'wing'       => $wing,
                  'block'      => $block
                );

                $house_id = $this->db->select('id')->where($house_data)->get('house_master')->row()->id;

                $role_id = $this->db->select('id')->where('role_name',$role_name)->get('user_roles')->row()->id;

                $salt     = getRandomId(8);
                $pass     = md5($salt.$password);

                  $data = array(
                      'society_id'          =>  $society_id,
                      'house_id'            =>  $house_id,
                      'role_id'             =>  $role_id,
                      'first_name'          =>  trim($first_name),
                      'last_name'           =>  trim($last_name),
                      'username'            =>  $username,
                      'password'            =>  $pass,
                      'email'               =>  $email,
                      'phone'               =>  $phone,
                      'profile_pic'         =>  'profileImages1.png',
                      'status'              =>  '1',
                      'authority_id'        =>  $authority_id,
                      'salt'                =>  $salt,
                      'birth_date'          =>  $birth_date,
                      'total_family_member' =>  $total_member,
                      'updated_by'          =>  $ses_id,
                      'updated_date'        =>  date('Y-m-d H:m:s'),
                      'created_by'          =>  $ses_id,
                      'created_date'        =>  date('Y-m-d H:m:s')
                      );
              if($house_id>0 || $house_id!='') {
                $inserted = $this->users_model->userCSV($data);
            
               if($inserted) {

                $this->house_master_model->allocateHouseCsv($house_id);
                   $snm  = strtoupper($society_name);
                   $name = ucfirst($first_name).' '.ucfirst($last_name);
                   $msg  ="Congratulation ".$name.", your login credentials are username: ".$username." password: ".$password." login link https://hamarisociety.co.in/dashboard thank you"; 

                  $smsres = sendSms($phone,$msg);

                  //email

                    $mail_replacement_array = array("{{useremail}}" => $first_name." ".$last_name,
                                                        "{{society_name}}" => $snm,
                                                        "{{username}}" => $username,
                                                        "{{password}}" => $password,
                                                       );
                        
                  $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),LOGIN_CREDENTIAL_FOR_USER);
                  
                  $params = array(
                    'to'        =>  $email,
                    'subject'   => 'Login Credentials',
                    'html'      =>  $data['mail_content'],
                    'from'      => $this->session->userdata('email'),
                );
                  $response = sendMail($params);
                 }
              }
               else {show('The House is not exist, Before adding users first The House need to be registered');}
            }
           }
          fclose($file);
        if($inserted) {
              $flash_msg = "";
              if($smsres) {
                $flash_msg="and SMS Send";
              }
             $this->session->set_flashdata("msg", "Users Added $flash_msg successfully");
             $this->session->set_flashdata("msg_type", "success");   
             redirect(base_url().'back/registration/alluser');
            }
        }
        else{
          if( $mydt!='csv'){
              $this->session->set_flashdata('msg', 'Only extention csv Allowed');
              $this->session->set_flashdata('msg_type', 'danger');  
              redirect(base_url().'back/registration/userCsvView');
              
          } else{

              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');  
              redirect(base_url().'back/registration/userCsvView');
          }
    }
}
}
    public function selfVerify() {

      $this->users_model->selfVerify();
    }

      public function userExistByEmail() {
        $email = $this->input->post('email');
         $res = $this->db->where('email',$email)->get('users');
         $dd = '';
        if($res->num_rows()>'0') {
          $dd = "<span>Email $email is Already Taken, Try Other Email<span/>";
          echo ($dd);
        }
    }
    public function user_password()
    {
      $password=$this->input->post('password');
      $res=preg_match("^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$^", $password);  
      print_r($res);
    }
      function is_mobile_no_registered() {
    $input = $this->input->post ( NULL, TRUE );
    $mobile = $input['mobile'];
    $is_exsit = $this->users_model->where('phone',$mobile)->find_all();
    if($is_exsit)
    {
      $flag = 'true';
    }else{
      $flag = 'false';
    }
    echo $flag;
  }
  
  function is_email_registered() {
    $input = $this->input->post ( NULL, TRUE );
    $email = $input['email'];
    $is_exsit = $this->users_model->where('email',$email)->find_all();
    if($is_exsit)
    {
      $flag = 'true';
    }else{
      $flag = 'false';
    }
    echo $flag;
  }

  function is_society_email_exist(){
    $val=$this->input->post('val');
    // echo $val;
    $data=$this->society_master_model->society_email_exit($val);
    if($data->num_rows()>0){
      echo "<span style='color:red'>Email Aleready Exit!!!</span>";
    }
  }


   function is_username_registered() {
    $input = $this->input->post ( NULL, TRUE );
    $username = $input['username'];
    $is_exsit = $this->users_model->where('username',$username)->find_all();
    if($is_exsit)
    {
      $flag = 'true';
    }else{
      $flag = 'false';
    }
    echo $flag;
  }

}
