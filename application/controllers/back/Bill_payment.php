<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bill_payment extends MY_Controller {

	public function __construct() {
		parent::__construct();
		is_logged_in();
		$this->load->model(array('users_model', 'bill_payment_model', 'wallet_transaction_model', 'wallet_model', 'net_banking_model','house_master_model','society_house_charges_model','parking_model','parkmaster_model','parking_charges_model','facility_model'));

	}
	public function index() {
		$data = array();
		load_back_view('admin/bill_payment/bill_payment', $data);
	}
	public function get_notices_data() {
		$session_data = $this->session->userdata();
		$role_id      = $session_data['role_id'];
		$user_id      = $session_data['id'];
		$this->datatables->select('1,id,DATE_FORMAT(created_at, "%d-%m-%Y"),title,subject');
		if ($role_id == SOCIETY_MEMBER) {
			$this->datatables->where('notice_to', $user_id);
		}
		$this->datatables->add_column('action', '<a href='.base_url().'back/notices/get_member_data/$1 title="View Notice Transacations" title="view notice" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-th-list"></i> </a>', 'id');
		$this->datatables->from('notices');

		$data = $this->datatables->generate();

		echo $data;
	}
	public function make_payment() {
		$data                       = array();
		$society_id                 = $this->session->userdata('society_id');
		$netbanking_details         = $this->db->where('society_id', $society_id)->get('net-banking-details')->result_array();
		$data['netbanking_details'] = $netbanking_details;
		load_back_view('admin/bill_payment/payment_request', $data);
	}
	public function save_payment() {
		$input = $this->input->post();
	
		if ($input) {
			if ($input['amount'] > 0) {
				$session_data = $this->session->userdata();
				$user_id      = $session_data['id'];
				$society_id   = $session_data['society_id'];

				$this->bill_payment_model->user_id    = $user_id;
				$this->bill_payment_model->society_id = $society_id;
				$transaction_date                     = $input['transaction_date'];
				$transaction_type                     = $input['transaction_type'];

				$comment = '';
				if ($input['comment']) {
					$comment = $input['comment'];
				}

				$this->bill_payment_model->amount           = $input['amount'];
				$this->bill_payment_model->transaction_date = $transaction_date;
				$this->bill_payment_model->transaction_type = $transaction_type;

				if($transaction_type = 'netbanking') {
					$bank_name   = $input['bank_name'];
					$acc_no      = $input['acc_no'];
					$branch_name = $input['branch_name'];
					$trans_ref   = '';
					$ifsc        = '';
					if ($input['trans_ref']) {
						$trans_ref = $input['trans_ref'];
					}
					if ($input['ifsc']) {
						$ifsc = $input['ifsc'];
					}

					$this->bill_payment_model->ifsc_code          = $ifsc;
					$this->bill_payment_model->bank_name          = $bank_name;
					$this->bill_payment_model->bank_acc_no        = $acc_no;
					$this->bill_payment_model->branch_name        = $branch_name;
					$this->bill_payment_model->transaction_ref_no = $trans_ref;
				}
				if($transaction_type = 'cheque') {

					$bank_name   = $input['bank_name'];
					$acc_no      = $input['acc_no'];
					
					$trans_ref   = '';
					$ifsc        = '';
					if ($input['check_no']) {
						$check_no = $input['check_no'];
					}
					$this->bill_payment_model->bank_name          = $bank_name;
					$this->bill_payment_model->bank_acc_no        = $acc_no;
					$this->bill_payment_model->transaction_ref_no = $check_no;
				}
				$this->bill_payment_model->created_by = $this->session->userdata('user_id');
				$this->bill_payment_model->payment_remark = $comment;
				$insert_request = $this->bill_payment_model->save();

				if ($insert_request) {
					$this->session->set_flashdata('msg', 'bill payment request sent successfully');
					$this->session->set_flashdata('msg_type', 'success');
				} else {
					$this->session->set_flashdata('msg', 'Error in insertion retry');
					$this->session->set_flashdata('msg_type', 'danger');
				}
				redirect(base_url().'back/bill_payment/');
			} else {
				$this->session->set_flashdata('msg', 'Trasnsaction amount Should be greater than zero');
				$this->session->set_flashdata('msg_type', 'danger');
				redirect(base_url().'back/bill_payment/make_payment');
			}
		}
		redirect(base_url().'back/bill_payment_model');
	}
	public function get_payment_data() {
		$data         = $this->input->post();
		$session_data = $this->session->userdata();
		$role_id      = $session_data['role_id'];
		$society_id   = $session_data['society_id'];
		$user_id      = $session_data['id'];
		$this->datatables->select('1,b.id,u.role_id,concat(u.first_name,"	",u.last_name) as full_name,b.amount,DATE_FORMAT(b.created_at, "%d-%m-%Y"),b.transaction_type,b.transaction_date,b.transaction_status,b.is_approved');
		$this->datatables->from('bill_payment b');
		$this->datatables->join('users u', 'u.id=b.user_id', 'left');

		if (isset($data['from_date']) && $data['from_date'] != '') {
			$this->datatables->where("DATE_FORMAT(b.created_at, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
		}

		if (isset($data['to_date']) && $data['to_date'] != '') {
			$this->datatables->where("DATE_FORMAT(b.created_at, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
		}
		if ($data['transaction_type']) {
			$this->datatables->where("b.is_approved", $data['transaction_type']);
		}

		if ($role_id == SOCIETY_SUPERUSER) {
			$this->datatables->where('b.society_id', $society_id);
			$this->datatables->add_column('action', '<a href='.base_url().'back/bill_payment/bill_payment_view/$1 title="View Transacations" title="view notice" class="views_wallet_trans margin" ref="$1"><i class="glyphicon glyphicon-pencil"></i> </a>', 'id');

		} elseif ($role_id == SOCIETY_ADMIN) {

			$this->datatables->where('b.society_id', $society_id);
			$this->datatables->add_column('action', '<a href='.base_url().'back/bill_payment/bill_payment_view/$1 title="View Transacations" title="view notice" class="views_wallet_trans margin" ref="$1"><i class="glyphicon glyphicon-pencil"></i> </a>', 'id');
		} else {
			$this->datatables->add_column('action', '<a href='.base_url().'back/bill_payment/bill_payment_view/$1 title="View Transacations" title="view notice" class="views_wallet_trans margin" ref="$1"><i class="glyphicon glyphicon-eye-open"></i> </a>', 'id');
		}
		if ($role_id == SOCIETY_MEMBER) {
			$this->datatables->where('b.user_id', $user_id);
		}

		$data = $this->datatables->generate();
		echo $data;
	}
	public function bill_payment_view($id) {
		if ($id) {
			$this->bill_payment_model->id = $id;
			$session_data                 = $this->session->userdata();
			$rdata                        = $this->bill_payment_model->select();
			$data['bill_payment']         = $rdata;
			if ($rdata->user_id == $this->session->userdata('user_id')) {
				$data['is_admin'] = false;
			} else {
				$data['is_admin'] = true;
			}
			if ($session_data['role_id'] == SOCIETY_MEMBER || $session_data['role_id'] == SUPERADMIN) {
				$data['is_admin'] = false;
			}
			load_back_view('admin/bill_payment/payment_request_view', $data);
		} else {
			redirect(base_url().'admin/bill_payment/payment_request');
		}
	}
	public function update_payment() {
		$msg_type = "success";
		$msg      = "please provide valid input";

		$input = $this->input->post();
		if ($input) {
			$id = $input['id'];

			$admin_remark = $input['comment'];
			$status       = $input['status'];

			if ($status == "approved") {
				$transaction_status = "success";
				$is_approved        = "Approved";
			} else {
				$transaction_status = "Rejected";
				$is_approved        = "Rejected";
			}
			$this->db->trans_begin();

			$data = array('transaction_status' => $transaction_status, 'is_approved' => $is_approved, 'admin_remark' => $admin_remark);
			$this->db->where('id', $id);
			$update_bill_payment = $this->db->update('bill_payment', $data);
			if ($update_bill_payment) {
				if ($status == 'approved') {
					$bill_details = $this->bill_payment_model->where('id', $id)->find_all();

					$bill_details   = $bill_details[0];
					$wallet_details = $this->wallet_model->where('user_id', $bill_details->user_id)->find_all();
					$wallet_details = $wallet_details[0];

					$wallet_balance      = $wallet_details->wallet_balance;
					$current_outstanding = $wallet_details->current_outstanding;
					$amount              = $bill_details->amount;

					if ($current_outstanding < $amount) {
						$wallet_bal              = ($amount-$current_outstanding)+$wallet_balance;
						$new_current_outstanding = 0;

					} elseif ($amount == $current_outstanding) {

						$new_current_outstanding = $amount-$current_outstanding;
						$wallet_bal              = $wallet_balance;

					} elseif ($amount < $current_outstanding) {
						$new_current_outstanding = $current_outstanding-$amount;
						$wallet_bal              = $wallet_balance;
					} else {}

					$wallet_transaction = array('user_id' => $bill_details->user_id,
						'w_id'                               => $wallet_details->id,
						'outstanding_amount_before'          => $current_outstanding,
						'amount'                             => $amount,
						'outstanding_amount_after'           => $new_current_outstanding,
						'wallet_balance_before'              => $wallet_balance,
						'wallet_balance_after'               => $wallet_bal,
						'bill_payment_id'                    => $bill_details->id
					);
					$transaction_insertion_id = $this->wallet_transaction_model->insert($wallet_transaction);

					if ($wallet_transaction) {
						$update_data = array('current_outstanding' => $new_current_outstanding, 'wallet_balance' => $wallet_bal);
						$this->db->where('user_id', $bill_details->user_id);
						$update_wallet = $this->db->update('wallet', $update_data);

						if ($update_wallet) {
							$data       = $this->users_model->where('id', $bill_details->user_id)->find_all();
							$first_name = $data[0]->first_name;
							$last_name  = $data[0]->last_name;
							$mobile     = $data[0]->phone;
							$full_name  = ucwords($first_name);
							$email      = $data[0]->email;

							$sc_dd = $this->db->get_where('society_master',array('id'=>$data[0]->society_id))->result();
                            $sc_nm = $sc_dd[0]->name;

							$mail_replacement_array = array("{{useremail}}" => $email,
								"{{amount}}"                                   => $bill_details->amount,
								"{{username}}"                                 => $first_name." ".$last_name,
								"{{amount}}"                                   => $amount,
								"{{topup_by}}"                                 => $bill_details->transaction_type,
								"{{bank_name}}"                                => $bill_details->bank_name ? $bill_details->bank_name : '-',
								"{{transaction_no}}"                           => $bill_details->transaction_ref_no ? $bill_details->transaction_ref_no : '-',
								"{{bank_acc_no}}"                              => $bill_details->bank_acc_no ? $bill_details->bank_acc_no : '-',
								"{{transaction_remark}}"                       => $admin_remark ? $admin_remark : '-',
								"{{transaction_date}}"                         => $bill_details->transaction_date,
								"{{reject_reason}}"                            => isset($admin_remark)?$admin_remark:'',
								"{{society_name}}"                             =>$sc_nm
							);


							$data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), BILL_PAYMENT_REQUEST);
							$params               = array(
								'to'      => $email,
								'subject' => 'Bill Payment Request',
								'html'    => $data['mail_content'],
								'from'    => ADMIN_EMAIL,
							);
							$response = sendMail($params);
							//send sms start
							$sesc_id  = $this->session->userdata('society_id');
							$snm_data = $this->db->get_where('society_master', array('id' => $sesc_id))->result();
							$snm      = $snm_data[0]->name;
							$msg      = "Dear ".$full_name.", your Payment of Rs. ".$bill_details->amount." Received, your Current Outstanding is:".$new_current_outstanding." and transaction Ref. number ".$bill_details->transaction_ref_no;

							if ($mobile != '') {
								sendSms($mobile, $msg);
							}
							//sms end
							$this->db->trans_commit();
							$msg = "bill payment update successfully";
						} else {
							$this->db->trans_rollback();
							$msg = "unable to proceed your request try again later";
						}
					}
				} else {
					$this->db->trans_commit();

					$bill_details = $this->bill_payment_model->where('id', $id)->find_all();

					$bill_details   = $bill_details[0];
					$wallet_details = $this->wallet_model->where('user_id', $bill_details->user_id)->find_all();

					$data       = $this->users_model->where('id', $bill_details->user_id)->find_all();
					$first_name = $data[0]->first_name;
					$last_name  = $data[0]->last_name;
					$full_name  = ucwords($first_name);
					$mobile     = $data[0]->phone;
					$email      = $data[0]->email;

				    $sc_dd = $this->db->get_where('society_master',array('id'=>$data[0]->society_id))->result();
                    $sc_nm = $sc_dd[0]->name;

					$mail_replacement_array = array("{{useremail}}" => $email,
						"{{amount}}"                                   => $bill_details->amount,
						"{{username}}"                                 => $first_name." ".$last_name,
						"{{topup_by}}"                                 => $bill_details->transaction_type,
						"{{bank_name}}"                                => $bill_details->bank_name ? $bill_details->bank_name : '-',
						"{{transaction_no}}"                           => $bill_details->transaction_ref_no ? $bill_details->transaction_ref_no :'-',
						"{{bank_acc_no}}"                              => $bill_details->bank_acc_no ? $bill_details->bank_acc_no : '-',
						"{{transaction_remark}}"                       => $admin_remark ? $admin_remark : '-',
						"{{transaction_date}}"                         => $bill_details->transaction_date,
						"{{reject_reason}}"                            => isset($admin_remark)?$admin_remark:'',
					    "{{society_name}}"                             =>$sc_nm

					);

					$data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), BILL_PAYMENT_REJECTED);
					$params               = array(
						'to'      => $email,
						'subject' => 'Bill Payment Request',
						'html'    => $data['mail_content'],
						'from'    => ADMIN_EMAIL,
					);
					$response = sendMail($params);
					//send sms
					$sesc_id  = $this->session->userdata('society_id');
					$snm_data = $this->db->get_where('society_master', array('id' => $sesc_id))->result();
					$snm      = $snm_data[0]->name;

					$trns_msg   = '';
					$admin_rmrk = '';
					if ($admin_remark != '') {
						$admin_rmrk = 'And admin Remark:'.$admin_remark;
					}
					if ($bill_details->transaction_ref_no != '') {
						$trns_msg = 'transaction Ref. number'.$bill_details->transaction_ref_no;
					}
					$msg = "Dear ".$full_name.", your Payment of Rs ".$bill_details->amount." Rejected ".$trns_msg.", ".$admin_rmrk;
					if ($mobile != '') {
						$smsres = sendSms($mobile, $msg);
					}
					//sms end
					$msg      = "Request Rejected successfully";
					$msg_type = "success";
				}
			}
		}
		$this->session->set_flashdata('msg', $msg);
		$this->session->set_flashdata('msg_type', $msg_type);
		redirect(base_url().'back/bill_payment');

	}

	public function net_banking() {
		$input      = $this->input->post();
		$society_id = $this->session->userdata('society_id');
		if ($input) {
			$acc_name  = $input['acc_name'];
			$acc_no    = $input['acc_no'];
			$bank_name = $input['bank_name'];
			$ifsc_code = $input['ifsc_code'];
			$branch    = $input['branch'];

			$this->net_banking_model->society_id = $society_id;
			$this->net_banking_model->acc_name   = $acc_name;
			$this->net_banking_model->acc_no     = $acc_no;
			$this->net_banking_model->bank_name  = $bank_name;
			$this->net_banking_model->ifsc_code  = $ifsc_code;
			$this->net_banking_model->branch     = $branch;

			$net_banking = $this->net_banking_model->save();
			if ($net_banking) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Netbanking details save successfully');

			} else {
				$this->session->set_flashdata('msg_type', 'danger');
				$this->session->set_flashdata('msg', 'failed');
			}
			redirect(base_url().'back/bill_payment');
		}
	}
	public function generate_bill() {
		$data            = array();
		$get_society     = $this->db->select('id,name')->from('society_master')->where('is_deleted', 'N')->get()->result();
		$data['society'] = $get_society;
		load_back_view('admin/bill_payment/generate_bill', $data);
	}

	public function bill_users() {
		$society_id = $this->input->post('society_id');
		$users_data = $this->bill_payment_model->get_users($society_id);

		foreach ($users_data as $opt) {
			$userdata .= "<option value=$opt->id>$opt->wing $opt->user_name</option>";
		}
		echo ($userdata);
	}
	public function get_users() {
		$society_id = $this->input->post('society_id');
		$users_data = $this->bill_payment_model->bill_users($society_id);
		foreach ($users_data as $opt) {
			$userdata .= "<option value=$opt->id>$opt->wing $opt->user_name</option>";
		}
		echo ($userdata);
	}

	public function regenerate_bill() {
		$input = $this->input->post();
		$this->load->model('my_bill_model');
		if ($input) {
			$society_id = $input['Society_id'];
			$user_id    = $input['user_id'];
			$month      = $input['transaction_date'];
			if ($user_id && $month) {
				$userdata   = $this->users_model->where('id', $user_id)->where('status', 'Y')->where('is_deleted', 'N')->find_all();
				$house_id   = $userdata[0]->house_id;
				$value      = $userdata[0];
				$month_new  = explode('-', $month);
				$month      = $month_new[1].'-'.$month_new[0];
				$first_name = $userdata[0]->first_name;
				$last_name  = $userdata[0]->last_name;
				$phone      = $userdata[0]->phone;
				$email      = $userdata[0]->email;

				$where = array('user_id' => $user_id, 'month' => $month);

				$current_outstanding = '0.00';
				$wallet_bal_before   = '0.00';

				$wallet_details = $this->wallet_model->where('user_id', $user_id)->find_all();
				if ($wallet_details) {
					$current_outstanding = $wallet_details[0]->current_outstanding;
					$wallet_bal_before   = $wallet_details[0]->wallet_balance;
				} else {
					$this->wallet_model->user_id             = $user_id;
					$this->wallet_model->current_outstanding = '0.00';
					$this->wallet_model->wallet_balance      = '0.00';

					$wallet_id = $this->wallet_model->save();
				}
				log_data('Cron/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $wallet_details, 'wallet_details');

				$fetch_house_Details = $this->house_master_model->where('id', $house_id)->find_all();

				log_data('Cron/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $fetch_house_Details, 'house details');

				if ($fetch_house_Details) {
					$main_amt   = "0.00";
					$house_type = $this->society_house_charges_model->where('id', $fetch_house_Details[0]->house_type)->find_all();

					log_data('Cron/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $house_type, 'house type');

					if ($house_type) {
						$main_amt        = $house_type[0]->maintenance_charges;
						$parking_amt     = '0.00';
						$parking_details = $this->parking_model->where('user_id', $user_id)->find_all();
						if ($parking_details) {
							$parking_amt = '';
							if (sizeof($parking_details) > '1') {
								foreach ($parking_details as $key => $value) {
									$parking_master_details  = $this->parkmaster_model->where('id', $value->pm_id)->find_all();
									$parking_charges_details = $this->parking_charges_model->where('id', $parking_master_details[0]->parking_charge_id)->find_all();
									$parking_amt += $parking_charges_details[0]->parking_amt;
								}
							} else {
								$parking_master_details = $this->parkmaster_model->where('id', $parking_details[0]->pm_id)->find_all();

								$parking_charges_details = $this->parking_charges_model->where('id', $parking_master_details[0]->parking_charge_id)->find_all();

								$parking_amt = $parking_charges_details[0]->parking_amt;
							}
						}
						log_data('Cron/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $parking_details, 'parking_details');

						log_data('Cron/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $parking_amt, 'parking amount');

						$facility_charges = '0.00';
						$facility_details = $this->facility_model->where('user_id', $user_id)->find_all();
						if ($facility_details) {
							$facility_charges = 0;
							if (sizeof($facility_details) > '1') {
								foreach ($facility_details as $key => $value) {
									$facility_charges += $value->total_fees;
								}
							} else {
								$facility_charges = $facility_details[0]->total_fees;
							}
						}
						$other_charges   = '0.00';
						$total_amt       = $main_amt+$parking_amt+$facility_charges;
						$net_payable_amt = $total_amt;

						if ($wallet_bal_before > 0) {
							if ($total_amt > $wallet_bal_before) {
								$total_amt         = $total_amt-$wallet_bal_before;
								$wallet_bal_before = $wallet_bal_before;
								$wallet_bal_after  = "0.00";
							} else if ($total_amt == $wallet_bal_before) {
								$total_amt         = "0.00";
								$wallet_bal_before = $wallet_bal_before;
								$wallet_bal_after  = "0.00";
							} else {
								$wallet_bal_before = $wallet_bal_before;
								$wallet_bal_after  = $wallet_bal_before-$total_amt;
								$total_amt         = "0.00";
							}
						} else {
							$wallet_bal_after = $wallet_bal_before;
						}

						log_data('Cron/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $facility_details, 'facility_details');

						log_data('Cron/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $facility_charges, 'facility charges');

						$this->db->trans_begin();

						$this->my_bill_model->society_id             = $society_id;
						$this->my_bill_model->user_id                = $user_id;
						$this->my_bill_model->maintenance_amt        = $main_amt;
						$this->my_bill_model->parking_amt            = $parking_amt;
						$this->my_bill_model->other_charges          = $other_charges;
						$this->my_bill_model->facility_charges       = $facility_charges;
						$this->my_bill_model->total_amt              = $total_amt;
						$this->my_bill_model->net_payable_amt        = $net_payable_amt;
						$this->my_bill_model->outstanding_amt_before = $current_outstanding;

						$this->my_bill_model->outstanding_amt_after = $current_outstanding+$total_amt;
						$this->my_bill_model->wallet_bal_before     = $wallet_bal_before;
						$this->my_bill_model->wallet_bal_after      = $wallet_bal_after;

						$check_Exist = $this->my_bill_model->where($where)->find_all();


						if ($check_Exist) {
							$bill_data = array(
								'society_id'             => $society_id,
								'user_id'                => $user_id,
								'month'                  => $month,
								'maintenance_amt'        => $main_amt,
								'parking_amt'            => $parking_amt,
								'other_charges'          => $other_charges,
								'facility_charges'       => $facility_charges,
								'total_amt'              => $total_amt,
								'net_payable_amt'        => $net_payable_amt,
								'outstanding_amt_before' => $current_outstanding,

								'outstanding_amt_after' => $current_outstanding+$total_amt,
								'wallet_bal_before'     => $wallet_bal_before,
								'wallet_bal_after'      => $wallet_bal_after,
							);
							$this->db->where('id', $check_Exist[0]->id);
							$insert_my_bill = $this->db->update('my_bill', $bill_data);
						} else {
							$insert_my_bill = $this->my_bill_model->save();
						}

						$update_array = array();
						if ($insert_my_bill) {
							$update_array = array('current_outstanding' => $current_outstanding+$total_amt, 'wallet_balance' => $wallet_bal_after);

						$society_nam = $this->db->get_where('society_master',array('id'=>$society_id))->result();

							$society_name =$society_nam[0]->name;


								$this->db->select('1,concat(u.first_name," ",u.last_name) as full_name,u.email,u.phone,u.id as uid,hm.building,hm.wing,hm.block,hm.house_type');
						    	$this->db->from('house_master hm');
						        $this->db->join('users u','u.house_id=hm.id','left');
						        $this->db->where('u.id',$user_id);
						       $society_data=$this->db->get()->result();

						       $MonthINWord = date('M',strtotime($month)).'-'.date("Y", strtotime($month));
						      
							$this->db->where('user_id', $user_id);
							$update_wallet = $this->db->update('wallet', $update_array);
							if ($update_wallet) {

								$mail_replacement_array = array("{{username}}" => $first_name." ".$last_name,
									"{{today}}"                                =>date('d-m-Y'),
									"{{month}}"                                => $MonthINWord,
									"{{maintenance_amt}}"                         => $main_amt,
									"{{parking_amt}}"                             => $parking_amt,
									"{{other_charges}}"                           => $other_charges,
									"{{facility_charges}}"                        => $facility_charges,
									"{{total_amt}}"                               => $total_amt,
									"{{address}}"                             =>$society_data[0]->building.','. $society_data[0]->wing.','.$society_data[0]->block,
									
									"{{society_name}}"                         =>$society_name,
									"{{net_payable_amt}}"                         => $net_payable_amt,
									"{{outstanding_remaining}}"              => $current_outstanding,
									"{{net_payable_amt}}"=>$current_outstanding + $total_amt
								);
								$data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), MONTHLY_BILL_REMAINDER);
								$params               = array(
									'to'      => "jay1137b@gmail.com",
									'subject' => 'Monthly Bill Details',
									'html'    => $data['mail_content'],
									'from'    => ADMIN_EMAIL,
								);
								

								$response = sendMail($params);
								
								//sms
								$mobile   = $value->phone;
								$fname    = $value->first_name;
								$username = $value->first_name;
								$sc_dd    = $this->db->get_where('society_master', array('id' => $society_id))->result();
								$sc_nm    = $sc_dd[0]->name;
								if (empty($mobile)) {

									log_data('sms/'.date('d-m-Y').'/username'.'.log', $username, 'username');
								} else {

									$snm   = strtoupper($sc_nm);
									$fname = ucfirst($fname);
									$msg   = "Dear ".$fname.", your bill generated for ".$snm." society, payable amount is: ".$net_payable_amt." Current Outstanding: ".$current_outstanding;

									$smsres = sendSms($mobile, $msg);


								}
								//sms
								$this->db->trans_commit();
								$this->session->set_flashdata('msg', 'Bill Generate successfully');
								$this->session->set_flashdata('msg_type', 'success');
							} else {
								$this->db->trans_rollback();
							}
						}
					} else {
						$msg = "unable to fetch house type details";
					}
				} else {
					$msg = "unble to fetch house details";
				}
			} else {

			}
		}
		redirect(base_url().'back/bill_payment/generate_bill');
	}

	public function pdf($id)
	  {
	      $data = array();
	       $this->db->select('1,mb.id,mb.month as transaction_date,concat(u.first_name," ",u.last_name) as full_name,u.email,u.phone,u.id as uid,mb.maintenance_amt,mb.parking_amt,mb.facility_charges,mb.other_charges,mb.total_amt,mb.outstanding_amt_after,mb.outstanding_amt_before,mb.net_payable_amt,mb.created_at,hm.building,hm.wing,hm.block,hm.house_type');
	       	$this->db->from('my_bill mb');    
	        $this->db->join('users u','u.id=mb.user_id','left');
	       $this->db->join('wallet w','w.user_id=mb.user_id','left');
	       $this->db->join('house_master hm','hm.id=u.house_id','left');
	       $this->db->where('mb.id',$id);
	       $data['society']=$this->db->get()->result();
	      
	      $this->load->library('Pdf');
	      load_back_view('admin/reports/pdf',$data);
	  }
}
?>
