<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notices extends MY_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        if(!$this->session->userdata('id'))
        {
        	redirect(base_url());
        }
        $this->load->model(array('users_model','notices_model'));
        
    }
    public function index()
	{
		$data = array();
		$session_data = $this->session->userdata();
		$get_data = $this->users_model->where('society_id',$session_data['society_id'])->where('is_deleted','N')->where_not_in('id',$session_data['id'])->where_not_in('role_id',SUPERADMIN)->find_all();
        
		$data['member_data'] = $get_data;
		load_back_view('notices',$data);
	}
	public function add_notice()
	{
		$input = $this->input->post();
		if($input)
		{
			$notices_title = $input['notices_title'];
			$notices_subject = $input['notices_subject'];
			$notices_to = $input['notices_to'];
			$society_id = $this->session->userdata('society_id');
			$notice_body = $input['notice_body'];
			$notice_id = getRandomId(4,"numeric");
            foreach ($notices_to as $key => $value) {

                    $where_array = array('society_id' => $society_id,'id'=>$this->session->userdata('id'));
                    $member_data = $this->db->where($where_array)->get('users')->result();
                    
				if($value == 'all')
				{
				
					if($member_data)
					{
						foreach ($member_data as $key => $value) {
				            $this->notices_model->notice_to = $value->id;
							$this->notices_model->society_id = $society_id;
							$this->notices_model->notice_id = $notice_id;
						    $this->notices_model->notice_from = $this->session->userdata('id');
						    $this->notices_model->title = $notices_title;
						    $this->notices_model->subject = $notices_subject;
						    $this->notices_model->notice_description = $notice_body;
						    $this->notices_model->created_by = $this->session->userdata('id');

						    $insert_notices = $this->notices_model->save();	

                            if($insert_notices)
                            {
                                $society_data = $this->db->where('id',$society_id)->get('society_master')->result();
                                $user_data    = $this->db->where('id',$member_data[0]->id)->get('users')->result();
                                $fullname     = $user_data[0]->first_name.' '.$user_data[0]->last_name;
                                $email        = $user_data[0]->email;
                                $mail_replacement_array = array(
                                                                "{{fullname}}"           => $fullname,
                                                                "{{notices_title}}"      => $notices_title,
                                                                "{{subject}}"            => $notices_subject,
                                                                "{{notice_description}}" => $notice_body,
                                                                "{{society_name}}"       => $society_data[0]->name
                                                                );
                                        
                                $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),NOTICES);
                                $params = array(
                                    'to'        =>  $email,
                                    'subject'   => 'Login Credentials',
                                    'html'      =>  $data['mail_content'],
                                    'from'      => ADMIN_EMAIL
                                );
                                
                                $response = sendMail($params);

                                if(!empty($mobile))
                                {
                                     $msg ="$full_name, you have one new notice, pls check here https://hamarisociety.co.in/dashboard/";
                                    // $smsres = sendSms($mobile,$msg);
                                }
                            }		
						}
					}
                }
				else
				{
                    $userdata = $this->users_model->where('id',$value)->find_all();

                    $email = $userdata[0]->email;
                    $mobile = $userdata[0]->phone;
                    $fullname = $userdata[0]->first_name." ".$userdata[0]->last_name;

                    $this->notices_model->notice_to = $value;
                    $this->notices_model->notice_id = $notice_id;
                    $this->notices_model->society_id = $society_id;
                    $this->notices_model->notice_from = $this->session->userdata('id');
                    $this->notices_model->title = $notices_title;
                    $this->notices_model->subject = $notices_subject;
                    $this->notices_model->notice_description = $notice_body;
                    $this->notices_model->created_by = $this->session->userdata('id');

                    $insert_notices = $this->notices_model->save();

                    if($insert_notices)
                    {
                        $society_data = $this->db->where('id',$society_id)->get('society_master')->result();
                        $user_data    = $this->db->where('id',$value)->get('users')->result();
                        $fullname     = $user_data[0]->first_name.' '.$user_data[0]->last_name;
                        $email        = $user_data[0]->email;
                        $mail_replacement_array = array(
                                                        "{{fullname}}"           => $fullname,
                                                        "{{notices_title}}"      => $notices_title,
                                                        "{{subject}}"            => $notices_subject,
                                                        "{{notice_description}}" => $notice_body,
                                                        "{{society_name}}"       => $society_data[0]->name
                                                        );
                                
                        $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),NOTICES);
                        $params = array(
                            'to'        =>  $email,
                            'subject'   => 'Notice',
                            'html'      =>  $data['mail_content'],
                            'from'      => ADMIN_EMAIL,
                        );
                        $response = sendMail($params);

                         if(!empty($mobile))
                            {
                                 $msg ="Dear ".$fullname.", you have one new notice, pls check here https://hamarisociety.co.in/dashboard/";
                                 $smsres = sendSms($mobile,$msg);
                            }
                    }
				}
			}
            
		}
	    $this->session->set_flashdata('msg', 'Notice send successfully');
	    $this->session->set_flashdata('msg_type', 'success');
		redirect(base_url().'back/notices');
	}
	public function view_template()
	{
		$input = $this->input->post();

        $this->notices_model->id = $input["id"];
        $rdata = $this->notices_model->select();

        $data["flag"] = '@#success#@';
        $data["template_data"] = $rdata;

        data2json($data);
	}
	public function view_template_member()
	{
		$input = $this->input->post();

        $this->notices_model->id = $input["id"];
        $rdata = $this->notices_model->select();
        
        if($rdata)
        {
        	$data['title'] = $rdata->title;
        	$data['notice_description'] = $rdata->notice_description;
        	$data['subject'] = $rdata->subject;
        }
        $data["flag"] = '@#success#@';
        $data["template_data"] = $data;

        data2json($data);
	}
	public function get_notices_data()
	{
		$session_data = $this->session->userdata();
		$role_id = $session_data['role_id'];
        $user_id = $session_data['id'];
        $this->datatables->select('1,id,DATE_FORMAT(created_at, "%d-%m-%Y") as date,DATE_FORMAT(created_at, "%h:%i") as time,title,subject');
        if($role_id == SOCIETY_MEMBER)
        {
        	$this->datatables->where('notice_to',$user_id);
        }
        $this->datatables->add_column('action', '<a href='.base_url() . 'back/notices/get_member_data/$1 title="View Notice Transacations" title="view notice" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-eye-open"></i> </a>', 'id');
        $this->datatables->where('society_id',$this->session->userdata('society_id'));
        $this->datatables->limit(100);
        $this->datatables->group_by('notice_id');
	    $this->datatables->from('notices');	
		
		$data = $this->datatables->generate();							
		
		echo $data;	
	}
	public function get_member_data($id)
	{
        $this->notices_model->id = $id;
        $session_data = $this->session->userdata();
        $rdata = $this->notices_model->select();
        if ($rdata) {
           $notice_to = $rdata->notice_to;
           if($notice_to == $this->session->userdata('id'))
           {
                $N_data = array(
		               'notice_view' => 'Y'
	            );
				$this->db->where('id', $id);
				$this->db->update('notices', $N_data); 
           }
        }
        if($session_data['role_id'] == SUPERADMIN || $session_data['role_id'] == SOCIETY_SUPERUSER || $session_data['role_id'] == SOCIETY_ADMIN)
        {
           $notice_id = $rdata->notice_id;
           $get_user_details = $this->notices_model->get_user_notices_count($notice_id);
           $data['notice_details'] = $get_user_details;
        }

        $data["template_data"] = $rdata;
        load_back_view('admin/notice/member_notice_view',$data);
	}
}

?>
