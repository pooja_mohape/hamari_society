<?php

/**
 * Description of Cron
 * 
 * @author jay butere<jayvant@stzsoft.com>
 * 
 * Monthly bill generation
 * 
 * 
 */

class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model(array('my_bill_model','wallet_model','users_model','house_master_model','society_house_charges_model','parking_model','parking_charges_model','parkmaster_model','facility_model'));
    }
    /*
     Author   : Jay B
     Function : generate_bill()
     Detail   : generate bill on monthly basic
    */
    public function generate_bill()
    {
      $msg = ''; 
      $society_id = $this->session->userdata('society_id');
      if($society_id)
      {
         $date = date('Y-m');
         $chk_monthly_bill_generation = $this->my_bill_model->where("DATE_FORMAT(created_at, '%Y-%m')=", date('Y-m', strtotime($date)))->where('society_id',$society_id)->find_all();
         if(empty($chk_monthly_bill_generation))
         {
            $userdata = $this->users_model->where('society_id',$society_id)->where('status','Y')->where('is_deleted','N')->where('role_id !=','2')->find_all();
            foreach ($userdata as $key => $value) {
                
                log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $value, 'userdata');

                $user_id = $value->id;
                $house_id = $value->house_id;
                $current_outstanding = '0.00';
                $wallet_bal_before = '0.00';

                $wallet_details = $this->wallet_model->where('user_id',$user_id)->find_all();
                if($wallet_details)
                {
                    $current_outstanding = $wallet_details[0]->current_outstanding;
                    $wallet_bal_before = $wallet_details[0]->wallet_balance;
                }
                else
                {
                  $this->wallet_model->user_id = $user_id;
                  $this->wallet_model->current_outstanding = '0.00';
                  $this->wallet_model->wallet_balance = '0.00';

                  $wallet_id  = $this->wallet_model->save();
                }
                log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $wallet_details, 'wallet_details');

                $fetch_house_Details = $this->house_master_model->where('id',$house_id)->find_all();

                log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $fetch_house_Details, 'house details');

                if($fetch_house_Details)
                {
                   $main_amt = "0.00"; 
                   $house_type = $this->society_house_charges_model->where('id',$fetch_house_Details[0]->house_type)->find_all();

                   log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $house_type, 'house type');

                   if($house_type)
                   {
                      if($fetch_house_Details[0]->on_lease == 'Y')
                      {
                        $lease_amt = $house_type[0]->lease_amt;
                      }else{
                        $lease_amt = 0.00;
                      }

                      $main_amt = $house_type[0]->maintenance_charges;
                      $parking_amt = '0.00';
                      $parking_details = $this->parking_model->where('user_id',$user_id)->find_all();
                      if($parking_details)
                      {
                         $parking_amt = '';
                         if(sizeof($parking_details) > '1')
                         {
                            foreach ($parking_details as $key => $value) {
                                $parking_master_details =$this->parkmaster_model->where('id',$value->pm_id)->find_all();
                                $parking_charges_details = $this->parking_charges_model->where('id',$parking_master_details[0]->parking_charge_id)->find_all();
                                $parking_amt+=$parking_charges_details[0]->parking_amt; 
                            }
                         }
                         else
                         {
                               $parking_master_details =$this->parkmaster_model->where('id',$parking_details[0]->pm_id)->find_all();

                                $parking_charges_details = $this->parking_charges_model->where('id',$parking_master_details[0]->parking_charge_id)->find_all();

                                $parking_amt = $parking_charges_details[0]->parking_amt;
                         }
                      }
                      log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $parking_details, 'parking_details');

                      log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $parking_amt, 'parking amount');

                      $facility_charges = '0.00';
                      $facility_details = $this->facility_model->where('user_id',$user_id)->find_all();
                      if($facility_details)
                      {
                         $facility_charges = 0;
                         if(sizeof($facility_details) > '1')
                         {
                            foreach ($facility_details as $key => $value) {
                                $facility_charges += $value->total_fees; 
                            }
                         }
                         else
                         {
                            $facility_charges = $facility_details[0]->total_fees;
                         }
                      }
                      $other_charges = $lease_amt;
                      $total_amt = $main_amt + $parking_amt + $facility_charges + $other_charges;
                      $net_payable_amt = $total_amt;
                      
                      if($wallet_bal_before > 0)
                      {
                          if($total_amt > $wallet_bal_before)
                          {
                            $total_amt = $total_amt - $wallet_bal_before;
                            $wallet_bal_before = $wallet_bal_before;
                            $wallet_bal_after = "0.00";
                          }
                          else if($total_amt == $wallet_bal_before) {
                            $total_amt = "0.00";
                            $wallet_bal_before = $wallet_bal_before;
                            $wallet_bal_after = "0.00";
                          }
                          else
                          {
                             $wallet_bal_before = $wallet_bal_before;
                             $wallet_bal_after = $wallet_bal_before - $total_amt;
                             $total_amt = "0.00";
                          }
                      }
                      else
                      {
                          $wallet_bal_after = $wallet_bal_before;
                      }

                      log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $facility_details, 'facility_details');

                      log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $facility_charges, 'facility charges');
                      
                      $this->db->trans_begin();      
                      
                      $this->my_bill_model->society_id = $society_id;
                      $this->my_bill_model->user_id = $user_id;
                      $this->my_bill_model->maintenance_amt = $main_amt;
                      $this->my_bill_model->parking_amt = $parking_amt;
                      $this->my_bill_model->other_charges = $other_charges;
                      $this->my_bill_model->facility_charges = $facility_charges;
                      $this->my_bill_model->total_amt = $total_amt;
                      $this->my_bill_model->net_payable_amt = $net_payable_amt;
                      $this->my_bill_model->outstanding_amt_before = $current_outstanding;

                      $this->my_bill_model->outstanding_amt_after = $current_outstanding + $total_amt;
                      $this->my_bill_model->wallet_bal_before = $wallet_bal_before;
                      $this->my_bill_model->wallet_bal_after = $wallet_bal_after;
                      
                      $insert_my_bill = $this->my_bill_model->save();
                      $update_array = array();
                      if($insert_my_bill)
                      {
                          $update_array = array('current_outstanding'=> $current_outstanding + $total_amt,'wallet_balance'=>$wallet_bal_after);
                          $this->db->where('user_id', $user_id);
                          $update_wallet = $this->db->update('wallet', $update_array);
                          if($update_wallet)
                          {

                              $mail_replacement_array = array("{{username}}" => $value->first_name." ".$value->last_name,
                                                        "{{maintenance_amt}}" => $main_amt,
                                                        "{{parking_amt}}" => $parking_amt,
                                                        "{{other_charges}}" => $other_charges,
                                                        "{{facility_charges}}" => $facility_charges,
                                                        "{{total_amt}}" => $total_amt,
                                                        "{{net_payable_amt}}" => $net_payable_amt,
                                                        "{{current_outstanding_amount}}" => $current_outstanding,
                                                       ); 
                              $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),MONTHLY_PAYMENT);
                              $params = array(
                                    'to'        =>  $value->email,
                                    'subject'   => 'Monthly Bill Details',
                                    'html'      =>  $data['mail_content'],
                                    'from'      => $this->session->userdata('email'),
                                );
                              $response = sendMail($params);
                        //sms
                            $mobile = $value->phone;
                            $fname  = $value->first_name;
                            $username  = $value->first_name;
                            $sc_dd = $this->db->get_where('society_master',array('id'=>$society_id))->result();
                            $sc_nm = $sc_dd[0]->name;
                                if(empty($mobile)) {
                                  
                                  log_data('sms/' .  date('d-m-Y'). '/username' . '.log', $username, 'username');
                                }
                                else {

                                   $snm = strtoupper($sc_nm);
                                   $fname  = ucfirst($first_name);
                                   $msg ="Dear ".$fname.", your bill generated for ".$snm." society, payable amount is: ".$net_payable_amt." Current Outstanding: ".$current_outstanding; 

                                    $smsres = sendSms($mobile,$msg);
                                }
                        //sms
                              $this->db->trans_commit();
                          }else{
                              $this->db->trans_rollback();
                          }
                      }
                   }
                   else
                   {
                    $msg = "unable to fetch house type details"; 
                   }
                }
                else
                {
                  $msg = "unble to fetch house details";
                }
            }
         }
         else
         {
            $msg = "bill already generated";
         }
      }
      echo $msg;
    }
     public function generate_monthly_bill()
    {
      $month = $this->uri->segment(4);
      $msg = ''; 
      $society_id = $this->session->userdata('society_id');
      if($society_id)
      {
         $date = date('Y-m');
         $chk_monthly_bill_generation = $this->my_bill_model->where("month =",$month)->where('society_id',$society_id)->find_all();
         if(empty($chk_monthly_bill_generation))
         {
            $userdata = $this->users_model->where('society_id',$society_id)->where('status','Y')->where('is_deleted','N')->where('role_id !=','2')->find_all();
            foreach ($userdata as $key => $value) {
                
                log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $value, 'userdata');

                $user_id = $value->id;
                $house_id = $value->house_id;
                $current_outstanding = '0.00';
                $wallet_bal_before = '0.00';

                $wallet_details = $this->wallet_model->where('user_id',$user_id)->find_all();
                if($wallet_details)
                {
                    $current_outstanding = $wallet_details[0]->current_outstanding;
                    $wallet_bal_before = $wallet_details[0]->wallet_balance;
                }
                else
                {
                  $this->wallet_model->user_id = $user_id;
                  $this->wallet_model->current_outstanding = '0.00';
                  $this->wallet_model->wallet_balance = '0.00';

                  $wallet_id  = $this->wallet_model->save();
                }
                log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $wallet_details, 'wallet_details');

                $fetch_house_Details = $this->house_master_model->where('id',$house_id)->find_all();

                log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $fetch_house_Details, 'house details');

                if($fetch_house_Details)
                {
                   $main_amt = "0.00"; 
                   $house_type = $this->society_house_charges_model->where('id',$fetch_house_Details[0]->house_type)->find_all();

                   log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $house_type, 'house type');

                   if($house_type)
                   { 
                      if($fetch_house_Details[0]->on_lease == 'Y'){
                        $lease_amt = $house_type[0]->lease_amt;
                      }else{
                        $lease_amt = 0.00;
                      }

                      $main_amt = $house_type[0]->maintenance_charges;
                      $parking_amt = '0.00';
                      $parking_details = $this->parking_model->where('user_id',$user_id)->find_all();
                      if($parking_details)
                      {
                         $parking_amt = '';
                         if(sizeof($parking_details) > '1')
                         {
                            foreach ($parking_details as $key => $value) {
                                $parking_master_details =$this->parkmaster_model->where('id',$value->pm_id)->find_all();
                                $parking_charges_details = $this->parking_charges_model->where('id',$parking_master_details[0]->parking_charge_id)->find_all();
                                $parking_amt+=$parking_charges_details[0]->parking_amt; 
                            }
                         }
                         else
                         {
                               $parking_master_details =$this->parkmaster_model->where('id',$parking_details[0]->pm_id)->find_all();

                                $parking_charges_details = $this->parking_charges_model->where('id',$parking_master_details[0]->parking_charge_id)->find_all();

                                $parking_amt = $parking_charges_details[0]->parking_amt;
                         }
                      }
                      log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $parking_details, 'parking_details');

                      log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $parking_amt, 'parking amount');

                      $facility_charges = '0.00';
                      $facility_details = $this->facility_model->where('user_id',$user_id)->find_all();
                      if($facility_details)
                      {
                         $facility_charges = 0;
                         if(sizeof($facility_details) > '1')
                         {
                            foreach ($facility_details as $key => $value) {
                                $facility_charges += $value->total_fees; 
                            }
                         }
                         else
                         {
                            $facility_charges = $facility_details[0]->total_fees;
                         }
                      }
                      $other_charges = $lease_amt;
                      $total_amt = $main_amt + $parking_amt + $facility_charges + $other_charges;
                      $net_payable_amt = $total_amt;
                      
                      if($wallet_bal_before > 0)
                      {
                          if($total_amt > $wallet_bal_before)
                          {
                            $total_amt = $total_amt - $wallet_bal_before;
                            $wallet_bal_before = $wallet_bal_before;
                            $wallet_bal_after = "0.00";
                          }
                          else if($total_amt == $wallet_bal_before) {
                            $total_amt = "0.00";
                            $wallet_bal_before = $wallet_bal_before;
                            $wallet_bal_after = "0.00";
                          }
                          else
                          {
                             $wallet_bal_before = $wallet_bal_before;
                             $wallet_bal_after = $wallet_bal_before - $total_amt;
                             $total_amt = "0.00";
                          }
                      }
                      else
                      {
                          $wallet_bal_after = $wallet_bal_before;
                      }

                      log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $facility_details, 'facility_details');

                      log_data('Cron/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $facility_charges, 'facility charges');
                      
                      $this->db->trans_begin();      
                      
                      $this->my_bill_model->society_id = $society_id;
                      $this->my_bill_model->user_id = $user_id;
                      $this->my_bill_model->maintenance_amt = $main_amt;
                      $this->my_bill_model->parking_amt = $parking_amt;
                      $this->my_bill_model->other_charges = $other_charges;
                      $this->my_bill_model->facility_charges = $facility_charges;
                      $this->my_bill_model->total_amt = $total_amt;
                      $this->my_bill_model->net_payable_amt = $net_payable_amt;
                      $this->my_bill_model->month = $month;

                      $this->my_bill_model->outstanding_amt_before = $current_outstanding;

                      $this->my_bill_model->outstanding_amt_after = $current_outstanding + $total_amt;
                      $this->my_bill_model->wallet_bal_before = $wallet_bal_before;
                      $this->my_bill_model->wallet_bal_after = $wallet_bal_after;
                      
                      $insert_my_bill = $this->my_bill_model->save();
                      $update_array = array();
                      if($insert_my_bill)
                      {
                          $update_array = array('current_outstanding'=> $current_outstanding + $total_amt,'wallet_balance'=>$wallet_bal_after);
                          $this->db->where('user_id', $user_id);
                          $update_wallet = $this->db->update('wallet', $update_array);
                          
                          $MonthINWord = date('M',strtotime($month)).'-'.date("Y", strtotime($month));
                          //sms
                            $mobile = $value->phone;
                            $fname  = $value->first_name;
                            $username  = $value->username;
                            $sc_id     = $value->society_id;

                            $sc_dd = $this->db->get_where('society_master',array('id'=>$sc_id))->result();
                            $sc_nm = $sc_dd[0]->name;
                            
                                if(empty($mobile)) {
                                  
                                  log_data('sms/' .  date('d-m-Y'). '/username' . '.log', $username, 'username');
                                }
                                else {

                                   $snm = strtoupper($sc_nm);
                                   $fname  = ucfirst($fname);
                                  
                                   $msg ="Dear ".$fname.", your bill generated for ".$MonthINWord." of ".$snm.", payable amount is: Rs.".$net_payable_amt; 
                                   
                                    $smsres = sendSms($mobile,$msg);
                                }
                        //sms
                                                  /* EMail start*/
                           $mail_replacement_array = array("{{today}}" => date('d-m-Y'),
                                                        "{{month}}" => $MonthINWord, 
                                                        "{{username}}" => $value->first_name." ".$value->last_name,
                                                        "{{address}}" => $fetch_house_Details[0]->block.','.$fetch_house_Details[0]->wing.','.$fetch_house_Details[0]->building,
                                                        "{{maintenance_amt}}" => $main_amt,
                                                        "{{parking_amt}}" => $parking_amt,
                                                        "{{facility_charges}}" => $facility_charges,
                                                        "{{other_charges}}" => $other_charges,
                                                        "{{total_amt}}" => $total_amt,
                                                        "{{society_name}}" => $sc_nm,
                                                        "{{outstanding_remaining}}"=>$current_outstanding,
                                                        "{{net_payable_amt}}"=>$current_outstanding + $total_amt
                                                       );

                          $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),MONTHLY_BILL_REMAINDER);
                          $params = array(
                                'to'        =>  $value->email,
                                'subject'   => 'Monthly Bill Details',
                                'html'      =>  $data['mail_content'],
                                'from'      =>  ADMIN_EMAIL,
                            );
                          $response = sendMail($params);
                        /* Email End*/           
                          
                          if($update_wallet)
                          {
                              $this->db->trans_commit();
                          }else{
                              $this->db->trans_rollback();
                          }
                      }
                   }
                   else
                   {
                    $msg = "unable to fetch house type details"; 
                   }
                }
                else
                {
                  $msg = "unble to fetch house details";
                }
            }
         }
         else
         {
            $msg = "bill already generated";
         }
      }
      echo $msg;
    }
}

//end of menu class