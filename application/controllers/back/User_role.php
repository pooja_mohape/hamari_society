<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Role
 * 
 * @author Jay butere <jayvant@stzsoft.com>
 * 
 * Revision History
 * 
 * 
 */
class User_role extends MY_Controller {

    public function __construct() {
        parent::__construct();

        is_logged_in();

        $this->load->model('user_role_model');
    }

    public function index() {

        $data['permission_list'] = "";
        load_back_view(USER_ROLES_VIEW, $data);
    }


    public function list_of_groups()
    {
        $action = '';
        $action_q = 
        $this->datatables->select("id,role_name,'dashboard',status,'back_end'");
        $this->datatables->from("user_roles");
        
      
        $action_q = '<a href="' . site_url('back/permission_menu/index/') . '/$1" class="btn btn-primary btn-xs" data-ref="$1" title="View Permission" ><i class="fa fa-fw fa-list-alt"></i> </a>';
        

        $action_q .= ' <a href="' . site_url('back/permission_menu/edit/') . '/$1" class="btn btn-primary btn-xs" data-ref="$1" title="Edit Permission"> <i class="fa fa-edit"></i></a>';
        
        $this->datatables->add_column('permissions', $action_q, 'id');

            $action .= '<a href="' . site_url('back/user_role/edit') . '/$1" class="btn btn-primary btn-xs edit_btn" data-ref="$1" title="view Role" > <i class="fa fa-eye"></i> </a>&nbsp;';


          $this->datatables->add_column('action', $action, 'id');
        
        $data = $this->datatables->generate('json');

        echo $data;
    }
   
   public function create_role()
   {
        load_back_view(CREATE_USER_ROLE__VIEW);
   } 
   
   public function save_role()
   {
        $input = $this->input->post();

        $config = array(
            array('field' => 'group_name', 'label' => 'Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter Role name.')),
            array('field' => 'homepage', 'label' => 'Homepage', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter Homepage for this user Role.')),
            array('field' => 'access', 'label' => 'Access', 'rules' => 'required', 'errors' => array('required' => 'Please Select Access level.')),
        );

        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();
            $this->session->set_flashdata('old', $this->input->post());
            $this->session->set_flashdata($data);

            redirect(site_url('back/user_role/create_role'));

            
            // echo load_back_view($this->viewPath . 'create', $data);
        } else {


            $group = [
                            'role_name'     => $input['group_name'],
                            'front_back_access'   => $input['access'],
                            'home_page'      => $input['homepage'],
                            'record_status' => 'Y',
                            'added_by' =>  $this->session->userdata('user_id'),
                            'added_on' =>  date('Y-m-d H:i:s')
                        ];

            //echo "<pre>"; print_r($group); die;

            $this->user_role_model->insert($group);
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Role Added Sucessfully.');

            redirect(site_url() . '/back/user_role');
        }
    }

    public function edit($id) {
        $data['group'] = $this->user_role_model->find($id);
        load_back_view(EDIT_USER_ROLE_VIEW,$data);
    }

    public function update($id) {

        $input = $this->input->post();
        //show($input,1);
        $data['pro_data'] = $this->user_role_model->find($id);

        $config = array(
            array('field' => 'role_name', 'label' => 'Role Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter Role name.')),
            array('field' => 'homepage', 'label' => 'Homepage', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter Homepage for this user Role.')),
            array('field' => 'access', 'label' => 'Access', 'rules' => 'required', 'errors' => array('required' => 'Please Select Access level.')),
        );
        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();
            // echo "<pre>";print_r($data['error']);die;
            $this->session->set_flashdata('old', $this->input->post());
            $this->session->set_flashdata($data);

            redirect(site_url('admin/user_role/edit/'.$id));
        } else {

            $groups = [
                'role_name' => $input['role_name'],
                'home_page' => $input['homepage'],
                'record_status' => $input['status'],
                'front_back_access' => $input['access'],
                'updated_on' => date('y-m-d H:i:s')            
            ];


            $this->user_role_model->update($input['role_id'], $groups);            
           

             $this->session->set_flashdata('msg_type', 'success');
             $this->session->set_flashdata('msg', 'Role update successfully.');

            redirect(site_url() . '/admin/user_role');
        }
    }

    public function delete($id) {
        $response = array();
        
        $role_data = $this->user_role_model
                        ->where("id", $id)
                        ->as_array()->find_all();
        //echo "<pre>"; var_dump(!$role_data); die;
       
        if ($role_data) {

            $this->db->where('id=', $id);
            $this->db->delete('user_roles');
            $this->session->set_flashdata('msg', 'Role delete sucessfully');

        } else {
            $this->session->set_flashdata('msg', 'Role already deleted');
        }

        redirect(site_url('admin/user_role'));
    }

}

//end of users class