<?php

class Facility extends MY_Controller {

    public function __construct() {
        parent::__construct();

        is_logged_in();

        $this->load->model(array('facility_model','users_model'));
    }
    public function index()
    {
      $data=array();
      load_back_view('admin/facility/facility',$data);
    }

    public function add_facility(){
     $facility_name = $this->input->post('facility_name');
     $ses_society = $this->session->userdata('society_id');
     if($facility_name)
     {
        $this->db->select('*');
        $this->db->from('facility_master');
        $this->db->where(array('facility_name'=>$facility_name,'society_id'=>$ses_society));
        $result = $this->db->get()->result();
        if($result)
        {
          $this->session->set_flashdata('msg','Facility Name Already Exist');
          $this->session->set_flashdata('msg_type','danger');
          redirect(base_url().'back/Facility');
        }
      }
      else
      {
          $this->session->set_flashdata('msg','Please provide valid input');
          $this->session->set_flashdata('msg_type','danger');
          redirect(base_url().'back/Facility');
      }
     $res= $this->facility_model->add_facility();
     if($res){

        $this->session->set_flashdata('msg','Facility Added Successfully');
        $this->session->set_flashdata('msg_type','success');
        $this->allFacilities();
      }
     
    }

    public function update_facility($id)
    {
      $update_facility['edit']=$this->facility_model->update_facility($id);
      
      load_back_view('admin/facility/update_facility',$update_facility);
    }

    public function edit_facility(){
      
      $edit_facility = $this->facility_model->edit_facility();
      if($edit_facility){
        $this->session->set_flashdata('msg','Facility Updated Successfully');
        $this->session->set_flashdata('msg_type','success');
        redirect(base_url().'back/societycharges');
      }
     
    }

    public function delete_facility($id){
      
      $data = $this->facility_model->delete_facility($id);
      if($data){
          $this->session->set_flashdata('msg','Facility Deleted Successfully');
          $this->session->set_flashdata('msg_type','success');
      }
      redirect(base_url().'back/societycharges');
    }

    public function allFacilities()
    {
      $data['facilities']= $this->facility_model->all_facility();
        load_back_view('admin/facility/edit_facility',$data);
    }

    public function view_facility($id){
      $data['view_facility']= $this->facility_model->view_facility($id);
      $id = $data['view_facility'][0]->id;
      if($id){
        load_back_view('admin/facility/view_facility',$data);
      }else{
        $this->edit_facility();
      }
    }
    
    //member facility
    public function memberFacility(){
      $data['facilities'] = $this->facility_model->get_facility();
      $data['users']= $this->facility_model->get_users();

      load_back_view('admin/facility/facility_member',$data);
    }

    public function addMember(){
      
      $input = $this->input->post();
      $total_member = $this->input->post('total_member');

      $uid = $this->input->post('user_id');
      $where = array('user_id' => $input['user_id'],'facilty_id'=>$input['facilty_id']);
      $chek_exist = $this->facility_model->where($where)->find_all(); 
     
      if($chek_exist)
      {
        $this->session->set_flashdata('msg','Facility Alredy allocate this user');
        $this->session->set_flashdata('msg_type','danger');
        redirect(base_url().'back/facility/memberFacility');
      }
      $data = $this->facility_model->add_member();
      if($data)
      {
        redirect(base_url().'back/facility/allMember');
      }
  }
        
    public function allMember()
    {
      $data['member']=$this->facility_model->all_member();
      load_back_view('admin/facility/add_member',$data);
    }

    public function showuser_facility($id,$fid,$uid)
    {
        $data['userFacility'] = $this->facility_model->view_userFacility($fid);
        $data['user_drop'] = $this->facility_model->user_dropdown($uid);
        load_back_view('admin/facility/view_Userfacility',$data);
    }

    public function get_user_fac($id,$fid,$uid){
      $data['user_fac'] = $this->facility_model->get_user_fac($fid);
      $data['facility_drop'] = $this->facility_model->dropdown($id);
      $data['user_drop'] = $this->facility_model->user_dropdown($uid);
      load_back_view('admin/facility/update_user_fac',$data);
    }

    public function update_user_fac(){
      $data= $this->facility_model->update_user_facility();
       if($data){
         $this->session->set_flashdata('msg', 'Update successully');
         $this->session->set_flashdata('msg_type', 'success');
         redirect(base_url().'back/facility/allMember');
      }
      else{
         $this->session->set_flashdata('msg', 'Updatation eror');
         $this->session->set_flashdata('msg_type', 'danger');
         redirect(base_url().'back/facility/allMember');
      }
    
  }

    public function delete_user_fac($id,$fid,$uid){ 
      $data['member']=$this->facility_model->all_member();
      $data['delete'] = $this->facility_model->delete_user_fac($fid);
      $this->user_failities($id);
    }

    public function user_failities(){
      $data['member']=$this->facility_model->all_member();
      $data['user_facility'] = $this->facility_model->user_facilities();
      load_back_view('admin/facility/add_member',$data);
    }

    public function check_value_exist()
    {
        $data = array();
        $facility_id = $this->input->post('facility_id');

        $get_Details = $this->db->select('*')->from('facility_master')->where('id',$facility_id)->get()->result();
        if($get_Details)
        {
          $adult_charges = $get_Details[0]->adult_charges;
          $child_Charges = $get_Details[0]->child_charges;
          $adult_status = 'false';
          $child_status = 'false';

          if($adult_charges > 0)
          {
             $adult_status = 'true';
          }
          if($child_Charges > 0)
          {
            $child_status = 'true';
          }
        }
        $data['facilty_id'] = $facility_id;
        $data['adult'] = isset($adult_status) ? $adult_status : '';
        $data['child'] = isset($child_status) ? $child_status : '';
        echo json_encode($data);
    }
}
?>