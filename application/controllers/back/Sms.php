<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms extends MY_Controller {

	public function __construct() {
		parent::__construct();
		is_logged_in();
		$this->load->model(array('society_master_model', 'Users_model', 'Wallet_model'));

	}

	public function index($id = '') {
		if ($id) {
			$user_list             = $this->Users_model->where('id', $id)->find_all();
			$wallet_amount         = $this->Wallet_model->where('user_id', $user_list[0]->id)->find_all();
			$data['wallet_amount'] = $wallet_amount;
			$data['user_list']     = $user_list;

			load_back_view('sms', $data);
		}
	}

	public function SmsView() {

		$data['society'] = $this->society_master_model->allSociety();
		load_back_view('admin/sms/smsView', $data);
	}
	public function fetchUsers() {

		$ses_role = $this->session->userdata('role_id');
		$input    = $this->input->post();
		$tbl      = '';
		if ($input) {

			if ($ses_role == SUPERADMIN) {
				$society_id = $input['society_id'];
			} else {
				$society_id = $this->session->userdata('society_id');
			}
			$sms_type    = '';
			$notify_type = $input['reason'];
			$group       = $input['group'];
			$result      = $this->Users_model->fetchUsers($society_id, $notify_type, $group);
			foreach ($result as $res) {

				$tbl .= "<option value=".$res->id.">".$res->name."</option>";
			}
			echo $tbl;
		}
	}

	public function wallet_sms() {
		$input = $this->input->post();

		if ($input) {
			$smsto   = $input['sms_to'];
			$subject = $input['subject'];
			$msg     = $input['msg'];
			foreach ($smsto as $id) {
				$userW  = $this->db->get_where('users', array('id' => $id))->result();
				$mobile = $userW[0]->phone;

				$usernm = $userW[0]->username;
				$smsres = '';
				if ($mobile == '') {

					log_data('sms/'.date('d-m-Y').'/sms'.'.log', $usernm, 'userdata');
				} else {
					$name   = ucfirst($userW[0]->first_name).' '.ucfirst($userW[0]->last_name);
					$msg    = "Hello ".$name.", ".$msg;
					$smsres = sendSms($mobile, $msg);
					redirect(base_url().'back/sms');
				}
				if (!isset($smsres->messages_not_sent)) {

					$this->session->set_flashdata('msg', 'SMS Send successfully');
					$this->session->set_flashdata('msg_type', 'success');

					redirect(base_url().'back/sms');
				} else {

					$this->session->set_flashdata('msg', 'Something Went Wrong');
					$this->session->set_flashdata('msg_type', 'danger');
				}
			}
		}
	}

	public function sendSms() {

		$input = $this->input->post();

		if ($input) {
			$sendto = $input['sendto'];
			$msg    = $input['msg'];
			foreach ($sendto as $id) {
				$userD  = $this->db->get_where('users', array('id' => $id))->result();
				$mobile = $userD[0]->phone;

				$usernm = $userD[0]->username;
				$smsres = '';
				if ($mobile == '') {

					log_data('sms/'.date('d-m-Y').'/sms'.'.log', $usernm, 'userdata');
				} else {
					$name   = ucfirst($userD[0]->first_name).' '.ucfirst($userD[0]->last_name);
					$msg    = "Hello ".$name.", ".$msg;
					$smsres = sendSms($mobile, $msg);
					redirect(base_url().'back/sms/smsView');
				}
				if (!isset($smsres->messages_not_sent)) {

					$this->session->set_flashdata('msg', 'SMS Send successfully');
					$this->session->set_flashdata('msg_type', 'success');

					redirect(base_url().'back/sms/smsview');
				} else {

					$this->session->set_flashdata('msg', 'Something Went Wrong');
					$this->session->set_flashdata('msg_type', 'danger');
				}
			}
		}
	}

	public function SmstoAll() {
		$society_id        = $this->session->userdata('society_id');
		$user['user_list'] = $this->Users_model->user_list($society_id);
		load_back_view('admin/sms_to_all', $user);
	}

	public function SendsmsAll() {
		$input   = $this->input->post();
		$subject = $input['subject'];
		$msg     = $input['msg'];
		$to      = $input['to'];
		if ($input) {
			foreach ($to as $key => $value) {
				$userinfo = $this->Users_model->where('id', $value)->find_all();
				$mobile   = $userinfo[0]->phone;
				$username = $userinfo[0]->username;
				$sms      = '';
				if ($mobile == '') {
					log_data('sms/'.date('d-m-Y').'/username'.'.log', $username, 'username');
				} else {
					$name    = ucfirst($userinfo[0]->first_name).' '.ucfirst($userinfo[0]->last_name);
					$message = "Hello ".$name.", ".$msg;
					$sms     = sendSms($mobile, $message);
				}
				if (!isset($sms->messages_not_sent)) {

					$this->session->set_flashdata('msg', 'SMS Send successfully');
					$this->session->set_flashdata('msg_type', 'success');

				} else {

					$this->session->set_flashdata('msg', 'Something Went Wrong');
					$this->session->set_flashdata('msg_type', 'danger');
				}
			}
			redirect(base_url().'back/sms/SmstoAll');

		}
	}
}