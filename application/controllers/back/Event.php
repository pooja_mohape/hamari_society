<?php

class Event extends MY_Controller {

    public function __construct() {
        parent::__construct();

        is_logged_in();

        $this->load->model('event_master_model');
    }
    public function index()
    {
       load_back_view('admin/welcome');
    }
    public function eventViewUser()
    {
        $data = array();
        load_back_view('admin/event/usercalender');
    }

    public function eventView() {
        $data = array();
        load_back_view('admin/event/admincalender');
    }
    
    public function allevent()
    {
        $result=$this->event_master_model->allEvent();
        //show($result,1);
        echo json_encode($result);
    }
    
    public function add_event()
        {   
            $mystartDate = new DateTime($this->input->post('starttime_db'));
            $myendate = new DateTime($this->input->post('endtime_db'));
            $starttime_db= $mystartDate->format('Y-m-d');
            $endtime_db= $myendate->format('Y-m-d');
            $title=$this->input->post('title');

            if($this->input->post('starttime') || $this->input->post('endtime')){
                 $startdate=$starttime_db." ".$this->input->post('starttime').':00';
                 $enddate=$endtime_db." ".$this->input->post('endtime').':00';
                }else
                {
                 $startdate=$starttime_db." ".'00:00:00';
                 $enddate=$endtime_db." ".'00:00:00';
                }

             $eventdata=array('title'=>$title,'start'=>$startdate,'end'=>$enddate,'society_id'=>$this->session->userdata('society_id'));
             $res=$this->event_master_model->addEvent($eventdata);
        }
        public function delete_event()
        {   $id=$this->input->post('id');
            $res=$this->event_master_model->deletEvent($id);
        }
        public function update_event()
        {
            $title=$this->input->post('title');
            $start=$this->input->post('start');
            $end=$this->input->post('end');
            $id=$this->input->post('id');
            $data=array('title'=>$title,'start'=>$start,'end'=>$end);
            $updateon=array('eid'=>$id);
            $id=$this->input->post('id');
            $res=$this->event_master_model->updateEvent($data,$updateon);
        }
    

}

//end of users class