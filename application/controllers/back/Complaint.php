<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint extends CI_Controller {

		public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model(array('complaint_model','users_model'));
        }

	public function index()
	{
		$session_data=$this->session->userdata();

		$data=array();
		load_back_view('complaints',$data);
	}
	public function add_complaint(){
		$input = $this->input->post();

		if($input){
			$complaint_type=$input['complaint_subject'];
			$complaint_details=$input['complaint_discription'];

			$society_id = $this->session->userdata('society_id');
			$user_id=$this->session->userdata('user_id');			
            $this->complaint_model->complaint_type = $complaint_type;
			$this->complaint_model->complaint_details = $complaint_details;
		   	$this->complaint_model->user_id =$user_id;
		   	$this->complaint_model->society_id =$society_id;
		    $this->complaint_model->created_by = $this->session->userdata('id');
			$insert_complaint = $this->complaint_model->save();
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Complaint sent successfully');
			redirect(base_url().'back/complaint');
		}

	}
	public function get_complaint_data()
	{
		$session_data = $this->session->userdata();
		$role_id = $session_data['role_id'];
        $user_id = $session_data['id'];
        $society_id = $session_data['society_id'];

      	$this->datatables->select('1,c.id,concat(u.first_name," ",u.last_name) as username,DATE_FORMAT(c.created_date,"%d-%m-%Y %H:%i:%s") as date,c.complaint_type,c.is_solved');
        if($role_id ==SOCIETY_MEMBER)
        {
        	$this->datatables->where('c.user_id',$user_id);
        	$this->datatables->add_column('action', '<a href='.base_url() . 'back/complaint/get_member_data/$1 title="View Complaint" class="btn btn-info btn-xs" ref="$1"><i  class="glyphicon glyphicon-eye-open"></i> </a>', 'id');
			$this->datatables->limit(100);
		}else{
        	$this->datatables->where('c.society_id',$society_id);
        	$this->datatables->add_column('action', '<a href='.base_url() . 'back/complaint/edit_complaint/$1 title="Edit Complaint" class="btn btn-warning btn-xs" ref="$1"><i  class="glyphicon glyphicon glyphicon-pencil"></i> </a>', 'id');
        }
	    $this->datatables->from('complaints c');	
		$this->datatables->join('users u','u.id = c.user_id','left');
		
		$cdata = $this->datatables->generate();	
		echo $cdata;
		
	}
	public function get_member_data($id){
		$this->complaint_model->id = $id;
        $rdata = $this->complaint_model->select();
        $data["template_data"] = $rdata;
        if($rdata)
        {
        	$data['userdata'] = $this->users_model->where('id',$rdata->user_id)->find_all();
        }
        load_back_view('admin/complaint/member_complaint_view',$data);
	}
	public function edit_complaint($id){
		$this->complaint_model->id = $id;
        $rdata = $this->complaint_model->select();       
        $data["template_data"] = $rdata;
        load_back_view('admin/complaint/edit_complaint',$data);

	}
	public function insert_complaint()
	{
		$input = $this->input->post();
		if($input)
		{
			$complaint_remark=$input['complaint_remark'];
			$id=$input['id'];
		    $array = array('admin_remark' =>$complaint_remark,
		    	            'is_solved'=>'Y');
			$update = $this->complaint_model->update($id,$array);
            if($update)
            {
               $this->session->set_flashdata('msg', 'reply send successfully');
			   $this->session->set_flashdata('msg_type', 'success');
			   redirect(base_url().'back/complaint');
            }
            else
            {
                 $this->session->set_flashdata('msg', 'Updatation eror');
			     $this->session->set_flashdata('msg_type', 'danger');
			     redirect(base_url().'back/complaint');
            }
			
		}
		else
		{
		   $this->session->set_flashdata('msg', 'please provide input');
			$this->session->set_flashdata('msg_type', 'danger');
			redirect(base_url().'back/complaint');	
		}
		
	}
}

