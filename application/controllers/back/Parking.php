<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parking extends CI_Controller {

		public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model("parking_model");
        $this->load->model("users_model");
        $this->load->model("parkmaster_model");
        $this->load->model("parking_charges_model");
        }

	public function index()
	{
		$society_id = $this->session->userdata('society_id');
         $this->db->select('u.id,u.first_name,u.last_name,h.building,h.wing,h.block');
         $this->db->from('users u');
         $this->db->join('house_master h','h.id=u.house_id');
         $this->db->where('u.society_id',$society_id);
         $this->db->where('u.role_id !=',SOCIETY_SUPERUSER);
         $this->db->where('u.is_deleted','N');
        $userdata = $this->db->get()->result(); 
        $data['userdata'] = $userdata;

        $parking_slot = $this->db->query("SELECT id,slot_no FROM parking_master WHERE id NOT IN (SELECT pm_id FROM parking) and society_id=$society_id")->result();
         $response = array();
        if($parking_slot)
       {
           foreach ($parking_slot as $key => $value) {
               $parking_master = $this->db->select('pm.id,pm.slot_no,shc.vehicle_type')->where('pm.id',$value->id)->from('parking_master pm')->join('parking_charges shc','pm.parking_charge_id=shc.id','left')->get()->result(); 
              
               array_push($response,$parking_master[0]);
           }
       }      
        $data['park_slot'] = $response;
        load_back_view('parking',$data);

	}
	public function add_parking(){
		$input = $this->input->post();
		if($input){
			$society_id=$this->session->userdata('society_id');
			$user_id=$this->session->userdata('user_id');
			$members=$input['members'];	
			
			$slot_no=$input['slot_no'];
			
			$this->parking_model->pm_id = $slot_no;
			$this->parking_model->user_id = $members;
		   	$this->parking_model->society_id = $society_id;
		   	$this->parking_model->created_by = $user_id;
		   	$insert_parking = $this->parking_model->save();
		   	$this->session->set_flashdata('msg', 'Parking Details Assigned successully');
        	$this->session->set_flashdata('msg_type', 'success');
			redirect(base_url().'back/parking');
		}
	}
	public function get_parking_data()
	{
		$session_data = $this->session->userdata();
		$role_id = $session_data['role_id'];
        $user_id = $session_data['id'];
  	    $society_id = $session_data['society_id'];
 
        $this->datatables->select('1,p.id,concat(u.first_name," ",u.last_name) as fullname,pm.slot_no,pc.vehicle_type');
        if($role_id ==SOCIETY_MEMBER)
        {
        	$this->datatables->where('u.id',$user_id);
        	$this->datatables->add_column('action', '<a href='.base_url() . 'back/parking/view_parking/$1 title="View Assign Slot" class="btn btn-warning btn-xs" ref="$1"><i  class="glyphicon glyphicon-eye-open"></i> </a>', 'id');
        }else{
        	$this->datatables->where('p.society_id',$society_id);
       		$this->datatables->add_column('action', '<a href='.base_url() . 'back/parking/view_parking/$1 title="view assign slot" class="btn btn-info btn-xs" ref="$1"><i  class="glyphicon glyphicon-eye-open"></i> </a>|
        		<a href='.base_url() .'back/parking/edit_parking/$1 title="Update Parking Slot" class="btn btn-warning btn-xs" ref="$1"><i  class="glyphicon glyphicon glyphicon-pencil"></i> </a> |
        		 <a href='.base_url() .'back/parking/delete_parking/$1 title="Remove Assign Slots" onClick="return doconfirm();" class="btn btn-danger btn-xs" ref="$1"><i  class="fa fa-trash-o"></i></a>', 'id');
        }
        $this->datatables->from('parking p');	
        $this->datatables->join('users u','u.id=p.user_id','left');
        $this->datatables->join('parking_master pm','pm.id=p.pm_id','left');	
        $this->datatables->join('parking_charges pc','pc.id=pm.parking_charge_id','left');   		
		$data = $this->datatables->generate();	
		echo $data;	
	}
	public function view_parking($id){
		$this->parking_model->id = $id;
        $rdata = $this->parking_model->select();
        $data["template_data"] = $rdata;        
        $data['user_name'] = $this->users_model->where('id',$rdata->user_id)->find_all();
        $data['parking_master'] = $this->db->select('*')->from('parking_master')->where('id',$rdata->pm_id)->get()->result();
        load_back_view('admin/parking/view_parking',$data);

	}
	public function edit_parking($id){
		$res = $this->parking_model->editParking($id);
        $data["parkd"] = $res;
        $society_id = $this->session->userdata('society_id');
        $data['allNames'] =$this->parking_model->allNames();
        $society_id = $this->session->userdata('society_id');
        $data['edit_slot'] = $this->db->query('SELECT id,slot_no FROM parking_master WHERE id NOT IN (SELECT pm_id FROM parking) and society_id='.$society_id.'')->result();
        
        $data['p_id'] = $id;
        $data['selected_parking'] = $this->parkmaster_model->where('id',$res[0]->pm_id)->find_all();
       load_back_view('admin/parking/edit_parking',$data);
	}
	public function update(){
		$input=$this->input->post();
		if($input)
		{
			$pid = $input['p_id'];
			$uid=$input['members'];
			$slot_name=$input['slot_no'];
			$pdata = array(
				 'pm_id'=>$slot_name
		    );
		    //show($pdata,1);
			$updated=$this->parking_model->updatePark($pid,$pdata);
			if($updated){
				 $this->session->set_flashdata('msg', 'Update successfully');
			   $this->session->set_flashdata('msg_type', 'success');
			   redirect(base_url().'back/parking');
			}
			else{
				 $this->session->set_flashdata('msg', 'Updatation eror');
			     $this->session->set_flashdata('msg_type', 'danger');
			     redirect(base_url().'back/parking');
			}
		}

	}
	public function delete_parking($id)
	{
		$delete = $this->parking_model->delete_park($id);
		if($delete)
		{
			 $this->session->set_flashdata('msg', 'deleted successfully');
			   $this->session->set_flashdata('msg_type', 'success');
			   redirect(base_url().'back/parking');
		}else
		{
			 $this->session->set_flashdata('msg', 'deletion eror');
			    $this->session->set_flashdata('msg_type', 'danger');
			    redirect(base_url().'back/parking');
		}

	}
	public function add_slot(){
        $data = array();
        $data['parking_details'] = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->as_array()->find_all();
		load_back_view('admin/parking/add_slot',$data);
	}
	public function insert_slot(){
		$input=$this->input->post();
		if($input)
		{	
			$society_id=$this->session->userdata('society_id');
			$slot_number=$input['slot_name'];
			$slot_for=$input['vehicle_type'];

			$check_slot = $this->parkmaster_model->where('slot_no',$slot_number)->where('society_id',$society_id)->find_all();
			if($check_slot)
			{
				$this->session->set_flashdata('msg', 'Parking slot Already Exist');
	        	$this->session->set_flashdata('msg_type', 'danger');
				redirect(base_url().'back/parking/add_slot');
			}
			else
			{
                $this->parkmaster_model->society_id = $society_id;
				$this->parkmaster_model->slot_no = $slot_number;
				$this->parkmaster_model->parking_charge_id = $slot_for;
				$insert_park_slot = $this->parkmaster_model->save();
				
				$this->session->set_flashdata('msg', 'Parking slot Added  Successfully');
	        	$this->session->set_flashdata('msg_type', 'success');
				redirect(base_url().'back/Parking/park_slot_details');
			}
		}
	}
	public function park_slot_details(){
		$society_id=$this->session->userdata('society_id');
		$id=$this->session->userdata('user_id');
		$res['data']=$this->parking_model->park_slot($society_id);

		  $parking_slot = $this->db->query("SELECT id,slot_no FROM parking_master WHERE id NOT IN (SELECT pm_id FROM parking) and society_id=$society_id")->result();

        $res['park_slot'] = $parking_slot;
       
        $res['parking_details'] = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->as_array()->find_all();

		load_back_view('admin/parking/parking_slot',$res);

	}
	public function delete_parkslot(){
		$id=$this->uri->segment(4);
		$this->db->where('id',$id);
		$res=$this->db->delete('parking_master');
		if($res){
		 $this->session->set_flashdata('msg', 'deleted successfully');
	   $this->session->set_flashdata('msg_type', 'success');
		redirect(base_url().'back/parking/park_slot_details');

		}
	}

	
}

