<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function dbBkp()
	{
		 if(true)
		{
		    $this->load->helper('url');
            $this->load->helper('file');
            $this->load->helper('download');
            $this->load->library('zip');

            $this->load->dbutil();
			$db_format=array('format'=>'zip','filename'=>'my_db_backup.sql');
			$backup=& $this->dbutil->backup($db_format);
			$dbname='backup-on-'.date('Y-m-d').'.zip';
			$save=base_url().'assets/db_backup/'.$dbname;
			write_file($save,$backup);
			force_download($dbname,$backup);
		}
		else
		{
           show('access denied',1);
		}
	}
	function checkenv()
	{
		 
		    show($this->input->ip_address(),'','Ip address');
			show(ENVIRONMENT,'','ENVIRONMENT');
			show($this->config,'','config');
			show($this->db->hostname,'','Host');
			show($this->db->database,'','Database');

			echo phpinfo();
	}
	public function test_mail()
	{
		 $params = array(
                    'to'        =>  'jayvant@stzsoft.com',
                    'subject'   => 'test',
                    'html'      =>  'its test mail',
                    'from'      => 'poojamohape1996@gmail.com',
                );
             
        $response = sendMail($params);
	}
		public function sendsms()
	{
		       $mobile = "8888161315";
               $reg_sms_array = array("{{username}}" => "jay",
                                      "{{password}}" => "password",
                                      "{{society_name}}"=> "vrindavan",
                                      "{{name}}"=> "jay butere");
               
               $msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array),MEMBER_REGISTRATION);

               $sendsms = sendSms($mobile,$msg);
	}
}
