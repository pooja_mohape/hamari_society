<?php

class Permission_menu extends CI_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model(array('permission_model', 'user_role_model', 'action_model', 'menu_model'));
    }

    public function index($id) {

        $data['group_id'] = $id;
        //  $data["rolename"] = $this->user_role_model->find_all();
        $data["rolename"] = $this->user_role_model->find($id);
        $data["permission_array"] = "";
        $not_in = array('1,2,3,4');
        $data['menu_parent'] = $this->permission_model->parent_menu_data($data['group_id']);
        $data['sub_sub_menu'] = $this->permission_model->sub_sub_menu($data['group_id']);

        $data['sub_menu_view'] = $this->permission_model->sub_menu_data($data['group_id']);

        $data["menu_action_mapping"] = $this->action_model
                        ->where("id > ", '4')
                        ->as_array()->find_all();
        $data["actual_permission"] = $this->permission_model->get_data();
        
        load_back_view(INDEX_VIEW, $data);
    }

    public function create() {


        $data["rolename"] = $this->user_role_model->find_all();
        $data["permission_array"] = "";
        $not_in = array('1,2,3,4');
        $data["menu_parent"] = $this->menu_model
                        ->where("parent", '0')
                        ->where("record_status", 'Y')
                        ->as_array()->find_all();
        $data["menu_action_mapping"] = $this->action_model
                        ->where("actionid > ", '4')
                        ->as_array()->find_all();
        $data["actual_permission"] = $this->permission_model->get_data();

        load_back_view($this->viewPath . 'create', $data);
    }

    public function sub_menu_view($menu_id) {
        $data = $this->menu_model->sub_menu_view($menu_id);

        data2json($data);
    }

    public function store() {
        
        $input = array();
        $input = $this->input->post();
        $config = array(
            array('field' => 'group_id', 'label' => 'Group Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please Select Group Name.'))
        );

        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();

            $this->session->set_flashdata('old', $this->input->post());
            $this->session->set_flashdata($data);

            redirect(site_url() . 'back/permission_menu/edit/' . $input['group_id']);
        } else {
            /* Delete Original data */

            for ($i = 0; $i < count($input['sub_main_menu']); $i++) {
                $del_sub_menu = [
                    'group_id' => $input['group_id'],
                    'menu_id' => $input['sub_main_menu'][$i],
                        /* 'record_status'=>'Y' */
                ];
                $this->permission_model->delete_where($del_sub_menu);
            }
           
            /**** Save the sub sub menu data   ****/
            if(isset($input['sub_sub_main_menu']) && count($input['sub_sub_main_menu'])>0){
                for ($i = 0; $i < count($input['sub_sub_main_menu']); $i++) {
                        $del_sub_sub_menu = [
                            'group_id' => $input['group_id'],
                            'menu_id' => $input['sub_sub_main_menu'][$i],
                                /* 'record_status'=>'Y' */
                        ];
                        $this->permission_model->delete_where($del_sub_sub_menu);
                     //   show(last_query(),1);
                    }
                for ($i = 0; $i < count($input['sub_sub_main_menu']); $i++) {
                    $final_too = array();
                    $fin_name = array();
                    
            
                    for ($j = 0; $j < count(array_unique($input['sub_sub_tool_main'])); $j++) {
                        $tool_name = "sub_sub_tool_" . $input["sub_sub_tool_main"][$j] . "_" . $input["sub_sub_main_menu"][$i];
                        if (isset($input[$tool_name]) && $input['sub_sub_tool_main'][$j] > 5) {
                            $final_too[] = $input['sub_sub_tool_main'][$j];

                            $fin_tool_name = $this->action_model
                                            ->where("id", $input['sub_sub_tool_main'][$j])
                                            ->as_array()->find_all();

                            $fin_name[] = $fin_tool_name[0]['name'];
                    }
                }
                
                   $sub_view = '';
                    $sub_create ='';
                    $sub_edit ='';
                    $sub_delete = '';

                    $sub_view = ((isset($input['sub_sub_main_view_' . $input['sub_sub_main_menu'][$i]]) && $input['sub_sub_main_view_' . $input['sub_sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' );
                    $sub_create = ((isset($input['sub_sub_main_create_' . $input['sub_sub_main_menu'][$i]]) && $input['sub_sub_main_create_' . $input['sub_sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' );

                   $sub_edit = ((isset($input['sub_sub_main_edit_' . $input['sub_sub_main_menu'][$i]]) && $input['sub_sub_main_edit_' . $input['sub_sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' );
                   $sub_delete = ((isset($input['sub_sub_main_delete_' . $input['sub_sub_main_menu'][$i]]) && $input['sub_sub_main_delete_' . $input['sub_sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' );
              
                
                if ($sub_view == 'Y'){
                    $sub_view_name = 'view';
                    array_push($fin_name, 'view');
                    array_push($final_too, '1');
                    
                }
                if ($sub_create == 'Y'){
                    $sub_create_name = 'create';
                    array_push($fin_name, 'create');
                    array_push($final_too, '2');
                    
                }
                if ($sub_edit == 'Y'){
                    $sub_edit_name = 'edit';
                    array_push($fin_name, 'edit');
                    array_push($final_too, '3');
                    
                }
                if ($sub_delete == 'Y'){
                    $sub_del_name = 'delete';
                    array_push($fin_name, 'delete');
                    array_push($final_too, '4');
                
                }
                
                
                
                $save_tool_menu = array_unique($final_too);
                $fin_tool = implode(',', $save_tool_menu);

                $save_tool_name = array_unique($fin_name);
                $fin_tool_name = implode(',', $save_tool_name);

                // $fin_tool_id=trim($fin_tool);
               if($sub_view !='N' ||  $sub_create !='N' ||  $sub_edit!='N' || $sub_delete!='N'){
                    $sub_menu = [
                        'group_id' => $input['group_id'],
                        'menu_id' => $input['sub_sub_main_menu'][$i],
                        'action_id' => $fin_tool,
                        'action_name' => $fin_tool_name,
                        'view' => ((isset($input['sub_sub_main_view_' . $input['sub_sub_main_menu'][$i]]) && $input['sub_sub_main_view_' . $input['sub_sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                        'add' => ((isset($input['sub_sub_main_create_' . $input['sub_sub_main_menu'][$i]]) && $input['sub_sub_main_create_' . $input['sub_sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                        'edit' => ((isset($input['sub_sub_main_edit_' . $input['sub_sub_main_menu'][$i]]) && $input['sub_sub_main_edit_' . $input['sub_sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                        'delete' => ((isset($input['sub_sub_main_delete_' . $input['sub_sub_main_menu'][$i]]) && $input['sub_sub_main_delete_' . $input['sub_sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                    ];
                    $this->permission_model->insert($sub_menu);
                }
             }
        }

            /* save the sub menu data */
            for ($i = 0; $i < count($input['sub_main_menu']); $i++) {
                $final_too = array();
                $fin_name = array();
                for ($j = 0; $j < count(array_unique($input['sub_tool_main'])); $j++) {
                    $tool_name = "sub_tool_" . $input["sub_tool_main"][$j] . "_" . $input["sub_main_menu"][$i];
                    if (isset($input[$tool_name]) && $input['sub_tool_main'][$j] > 5) {
                        $final_too[] = $input['sub_tool_main'][$j];

                        $fin_tool_name = $this->action_model
                                        ->where("id", $input['sub_tool_main'][$j])
                                        ->as_array()->find_all();

                        $fin_name[] = $fin_tool_name[0]['name'];
                    }
                }
                    $sub_view = '';
                    $sub_create ='';
                    $sub_edit ='';
                    $sub_delete = '';

                    $sub_view = ((isset($input['sub_main_view_' . $input['sub_main_menu'][$i]]) && $input['sub_main_view_' . $input['sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' );
                    $sub_create = ((isset($input['sub_main_create_' . $input['sub_main_menu'][$i]]) && $input['sub_main_create_' . $input['sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' );

                   $sub_edit = ((isset($input['sub_main_edit_' . $input['sub_main_menu'][$i]]) && $input['sub_main_edit_' . $input['sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' );
                   $sub_delete = ((isset($input['sub_main_delete_' . $input['sub_main_menu'][$i]]) && $input['sub_main_delete_' . $input['sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' );
              
                
                if ($sub_view == 'Y'){
                    $sub_view_name = 'view';
                    array_push($fin_name, 'view');
                    array_push($final_too, '1');
                    
                }
                if ($sub_create == 'Y'){
                    $sub_create_name = 'create';
                    array_push($fin_name, 'create');
                    array_push($final_too, '2');
                    
                }
                if ($sub_edit == 'Y'){
                    $sub_edit_name = 'edit';
                    array_push($fin_name, 'edit');
                    array_push($final_too, '3');
                    
                }
                if ($sub_delete == 'Y'){
                    $sub_del_name = 'delete';
                    array_push($fin_name, 'delete');
                    array_push($final_too, '4');
                
                }
                    
                $save_tool_menu = array_unique($final_too);
                $fin_tool = implode(',', $save_tool_menu);

                $save_tool_name = array_unique($fin_name);
                $fin_tool_name = implode(',', $save_tool_name);
               
                
                 
                if($sub_view !='N' ||  $sub_create !='N' ||  $sub_edit!='N' || $sub_delete!='N'){

                    $sub_menu = [
                        'group_id' => $input['group_id'],
                        'menu_id' => $input['sub_main_menu'][$i],
                        'action_id' => $fin_tool,
                        'action_name' => $fin_tool_name,
                        'view' => ((isset($input['sub_main_view_' . $input['sub_main_menu'][$i]]) && $input['sub_main_view_' . $input['sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                        'add' => ((isset($input['sub_main_create_' . $input['sub_main_menu'][$i]]) && $input['sub_main_create_' . $input['sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                        'edit' => ((isset($input['sub_main_edit_' . $input['sub_main_menu'][$i]]) && $input['sub_main_edit_' . $input['sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                        'delete' => ((isset($input['sub_main_delete_' . $input['sub_main_menu'][$i]]) && $input['sub_main_delete_' . $input['sub_main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                    ];
                    $this->permission_model->insert($sub_menu);
               }
            }


            for ($i = 0; $i < count($input['main_menu']); $i++) {


                $del_menu = [
                    'group_id' => $input['group_id'],
                    'menu_id' => $input['main_menu'][$i],
                ];
                $this->permission_model->delete_where($del_menu);


                    $sub_view = '';
                    $sub_create ='';
                    $sub_edit ='';
                    $sub_delete = '';
                    $fin_name=array();
                    $final_too=array();
                    $sub_view = ((isset($input['main_view_' . $input['main_menu'][$i]]) && $input['main_view_' . $input['main_menu'][$i]] == 'on') ? 'Y' : 'N' );
                    $sub_create = ((isset($input['main_create_' . $input['main_menu'][$i]]) && $input['main_create_' . $input['main_menu'][$i]] == 'on') ? 'Y' : 'N' );

                   $sub_edit = ((isset($input['main_edit_' . $input['main_menu'][$i]]) && $input['main_edit_' . $input['main_menu'][$i]] == 'on') ? 'Y' : 'N' );
                   $sub_delete = ((isset($input['main_delete_' . $input['main_menu'][$i]]) && $input['main_delete_' . $input['main_menu'][$i]] == 'on') ? 'Y' : 'N' );
              
                
                if ($sub_view == 'Y'){
                    $sub_view_name = 'view';
                    array_push($fin_name, 'view');
                    array_push($final_too, '1');
                    
                }
                if ($sub_create == 'Y'){
                    $sub_create_name = 'create';
                    array_push($fin_name, 'create');
                    array_push($final_too, '2');
                    
                }
                if ($sub_edit == 'Y'){
                    $sub_edit_name = 'edit';
                    array_push($fin_name, 'edit');
                    array_push($final_too, '3');
                    
                }
                if ($sub_delete == 'Y'){
                    $sub_del_name = 'delete';
                    array_push($fin_name, 'delete');
                    array_push($final_too, '4');
                
                }
                    
                $save_tool_menu = array_unique($final_too);
                $fin_tool = implode(',', $save_tool_menu);

                $save_tool_name = array_unique($fin_name);
                $fin_tool_name = implode(',', $save_tool_name);
                
                /*show($sub_view);
                  show($sub_create);
                    show($sub_edit);
                      show($sub_delete);*/
                 if($sub_view !='N' ||  $sub_create !='N' ||  $sub_edit!='N' || $sub_delete!='N'){
                
                    $provider = [
                        'group_id' => $input['group_id'],
                        'menu_id' => $input['main_menu'][$i],
                        'action_id' => $fin_tool,
                        'action_name' => $fin_tool_name,
                        'view' => ((isset($input['main_view_' . $input['main_menu'][$i]]) && $input['main_view_' . $input['main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                        'add' => ((isset($input['main_create_' . $input['main_menu'][$i]]) && $input['main_create_' . $input['main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                        'edit' => ((isset($input['main_edit_' . $input['main_menu'][$i]]) && $input['main_edit_' . $input['main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                        'delete' => ((isset($input['main_delete_' . $input['main_menu'][$i]]) && $input['main_delete_' . $input['main_menu'][$i]] == 'on') ? 'Y' : 'N' ),
                    ];
                    $this->permission_model->insert($provider);
                }
               
            } 
            /* add action mapping menu */
        }
        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Permission added successfully!');
        show(base_url(),1);
        redirect(base_url() . 'back/user_role');
    }

    public function delete($id) {

        $response = array();
        $data = array("record_status" => "N ");
        $this->db->where('group_id=', $id);
        $query = $this->db->update('permission', $data);
        if($query)
        {
        /* $tool_menu = array('menu_id' => '');

          $this->db->where('actionid > ','6');
          $this->db->update('actionmapping', $tool_menu); */
         $this->session->set_flashdata('msg_type', 'success');  
        $this->session->set_flashdata('msg', 'Permission deleted successfully');

        redirect(site_url() . '/back/user_role');
       }
       else
       {
        redirect(site_url().'back/user_role/edit/'.$id);
       }
    }

    public function edit($id) {
        $data['group_id'] = $id;
        $data["rolename"] = $this->user_role_model->find($id);

        $data["permission_array"] = "";
        $not_in = array('1,2,3,4');
//        $data["menu_parent"] = $this->menu_model
//                        ->where("parent", '0')
//                        ->where("record_status", 'Y')
//                        ->as_array()->find_all();
        $data['menu_parent'] = $this->permission_model->parent_menu_data($data['group_id']);
        $data['sub_sub_menu'] = $this->permission_model->sub_sub_menu($data['group_id']);
        $data['sub_menu_view'] = $this->permission_model->sub_menu_data($data['group_id']);

        $data["menu_action_mapping"] = $this->action_model
                        ->where("id > ", '4')
                        ->as_array()->find_all();
        // show($data,1);
        $data["actual_permission"] = $this->permission_model->get_data();
        load_back_view(EDIT_MENU_VIEW,$data);
    }
}
