<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('users_model'));
        
    }
    public function index()
    {
        $data = array();
        load_front_view(FRONT_PAGES.'welcome',$data);
    }
    public function login()
    {

     $data = '';

    }
    public function login_authencticate()
    {
    	$input = $this->input->post();
    	$username = $input['username'];
    	$password = $input['password'];
    	$config = array(
              array(
                    'field' => 'username',
                    'label' => 'username',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'username is required')
                    ),
              array(
                    'field'  => 'password',
                    'label'  => 'password',
                    'rules'  => 'trim|required',
                    'errors' => array('required' => 'password is required')
                    )
              );
       if (form_validate_rules($config) == TRUE)
       {
           $this->auth->login($username,$password);
       }
       else
       {
          $this->session->set_flashdata('msg_type', 'danger');
          $this->session->set_flashdata('msg', 'Please Provide username and password.');  
          redirect(base_url());
       }
    }
    public function logout()
    {
    	$this->auth->logout();
    }
}

?>
