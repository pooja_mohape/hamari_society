<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sapi extends CI_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Headers: Content-Type');
		header('Content-Type: text/html; charset=utf-8');
		header('Content-Type: application/json; charset=utf-8');
		$this->load->model(array('notices_model','users_model','house_master_model','complaint_model','meetings_model','facility_model'));

		$data = array();
		$data1 = array();
        $data['url'] = current_url();
        $data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $data['post'] = json_decode(file_get_contents('php://input'));
        $data['ip'] = $this->input->ip_address();
	}

	public function facility_list() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") 
		{
			$input = json_decode(file_get_contents('php://input'));
			$society_id = $input->society_id;
			// $society_id=$this->input->post('society_id');			Form data
			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code'] = 201;
			}else {
				$res=$this->db->select('id,society_id,facility_name,adult_charges,child_charges,maintenance_cost,is_deleted')->get_where('facility_master',array('society_id'=>$society_id,'is_deleted'=>'N'))->result();
				if($res)
				{
                    $response['facility_details'] = 'No data found';
				    $response['status'] = false;
				}
				else
				{
					$response['message'] = 'No data found';
				    $response['status'] = false;
                }
			}
		}else 
		{
			$response['message'] = 'No direct script is allowed.';
			$response['code'] = 204;
		}
		echo json_encode($response);
	}

	public function allocate_facility(){
		$response = array('status'=>false, 'message'=>'');
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$input = json_decode(file_get_contents('php://input'));
			$facilty_id = $input->facilty_id;
			$user_id = $input->user_id;
			$adult_count = $input->adult_count;
			$child_count = $input->child_count;
			$total_member = $input->total_member;

			if(empty($user_id)){
				$response['message'] = "User Id is required.";
				$response['code'] = 201;
			}else if(empty($facilty_id)){
				$response['message'] = "Facility Id is required.";
				$response['code'] = 201;
			}else{
				$data = array(
						'facilty_id' => $this->input->post('facilty_id'),
						'user_id' => $this->input->post('user_id'),
						'adult_count' => $this->input->post('adult_count'),
						'child_count' => $this->input->post('child_count'),
						'total_member'=> $this->input->post('total_member')
				);
				$allocate_user = $this->db->insert('facilty',$data);
				if($allocate_user == 0){
					$response['message'] = 'No record Found';
				}else{
					$response['message'] = 'Record inserted Successfully';
					$response['code'] = 200;
				}
			} 
		}
		else{
			$response['message']= 'No direct script is allowed.';
			$response['code'] = 204;
		}
		$data = '';
		echo json_encode($response);
	}

	public function fac_allocation_list(){	
		$response = array('status'=> false,'message'=>'');

		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$input = json_decode(file_get_contents('php://input'));
			$society_id = $input->society_id;

			if(empty($society_id)){
				$response['message'] = "Society Id is required.";
				$response['code'] = 201;
			}else{
				$this->db->select('concat(u.first_name," ",u.last_name) as username,fm.facility_name,f.*');
				$this->db->from('facilty f');
				$this->db->join('facility_master fm','fm.id = f.facilty_id','left');
				$this->db->join('users u','u.id = f.user_id','left');
				$this->db->where('f.society_id',$society_id);
				$res = $this->db->get()->result();
				if($res == 0){
					$response['message'] = 'No record Found.';
				}else{
					$response['status'] = 200;
					$result = $res;
					$response['fac_allocation_list']= array();

					$fac_allocation_list['username'] = $result[0]->username;
					$fac_allocation_list['facility_name'] = $result[0]->facility_name;
					$fac_allocation_list['society_id'] = $result[0]->society_id;
					$fac_allocation_list['total_member'] = $result[0]->total_member;
					$fac_allocation_list['adult_count'] = $result[0]->adult_count;
					$fac_allocation_list['child_count'] = $result[0]->child_count;
					$fac_allocation_list['total_fees'] = $result[0]->total_fees;
					array_push($response['fac_allocation_list'],$fac_allocation_list);
				}
			}
		}else{
			$response['message'] = 'No direct script is allowed.';
			$response['code'] = 204;
		}
		echo json_encode($response);
	}
}

?>
