<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth
{
	private $error = array();

	function __construct()
	{
		$this->ci =& get_instance();

        $this->ci->load->database();
        $this->ci->load->model(array('users_model'));
	}

	/**
	 * Login user on the site. Return TRUE if login is successful
	 * (user exists and activated, password is correct), otherwise FALSE.
	 *
	 * @param	string	(username or email or both depending on settings in config file)
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function login($username, $pass) {

        $udata = $this->ci->users_model->where('username', $username)->find_all();
		$users_action_log = array();
		if (!empty($udata)) 
		{
            $user_array = $udata[0];
            if ($user_array->status == "Y") 
			{				
				$salt = $user_array->salt;
                $user_password = $user_array->password;
                if ($pass != "")
				{
                    $password = md5($salt . $pass);
                  
                    if ($password == $user_password)
					{                        
                         $session_id = session_id();
                         $role_name = '';
						 $role_name = $this->ci->users_model->getRoleName($user_array->role_id);
                         $user_data = array(
	                            "id" => $user_array->id,
	                            "user_id" => $user_array->id,
	                            "role_id" => $user_array->role_id,
	                            "username" => $user_array->username,
	                            "email" => $user_array->email,
	                            "first_name" => $user_array->first_name,
	                            "last_name" => $user_array->last_name,
	                            "mobile_no" => $user_array->phone,
	                            "society_id"=> $user_array->society_id,
	                            "house_id" => $user_array->house_id,
	                            "role_name"=> $role_name,
	                            "session_id" => $session_id,
	                            "profile_pic" => $user_array->profile_pic
                         );

                        $this->ci->session->set_userdata($user_data);
                        $this->ci->session->set_flashdata('msg_type', 'success');
                        $this->ci->session->set_flashdata('msg', 'Welcome To SMS.');	
                        redirect(base_url()."admin/dashboard");	
                    } 
                    else 
					{                        
						$msg = 'Password is incorrect.';								
						$this->ci->session->set_flashdata('msg_type', 'error');
						$this->ci->session->set_flashdata('msg', $msg);						
						redirect(base_url());						
                    }
                } 
                else
				{					
                    $msg = 'Password is empty';		
					$this->ci->session->set_flashdata('msg_type', 'error');
					$this->ci->session->set_flashdata('msg', $msg);						
					redirect(base_url());
				}
            } 
            else 
			{			
               	$this->ci->session->set_flashdata('msg_type', 'error');
				$this->ci->session->set_flashdata('msg', 'Your account is disabled.');		
				redirect(base_url());
			}				
        } 
        else
        {
        	$this->ci->session->set_flashdata('msg_type', 'danger');
            $this->ci->session->set_flashdata('msg', 'Incorrect Username. please provide valid username');	
        	redirect(base_url());
        }
	}
	function logout()
	{
		$this->ci->session->set_userdata(array('user_id' => '', 'username' => '', 'status' => ''));
		session_destroy();
		redirect(base_url());
	}

	
}

