<div class="" id="add_edit_popup" tabindex="-1" role="" aria-hidden="false">

        <div class="modal-content">
<?php
$attributes = array("method" => "POST", "id" => "sms_template_form", "name" => "sms_template_form");
echo form_open(base_url().'back/sms/wallet_sms', $attributes);
?>
            <div class="modal-body">
               <div class="form-group">
                <center><h4><b><u>INSTANT SMS<u></h4></b></center>
                <br>
                <input type="hidden" name="mail_to" value="<?php echo $user_list[0]->email;?>"/>
                    <label for="title">Username:- </label>
                    <input type="text" value="<?php echo $user_list[0]->first_name." ".$user_list[0]->first_name;?>" readonly required/>

                    <label for="title">Current Outstanding:- </label>&nbsp;
                    <input type="text" value="<?php echo $wallet_amount[0]->current_outstanding;?>" readonly/>

                    <label for="title">Wallet_balance :- </label>
                    <input type="text" value="<?php echo $wallet_amount[0]->wallet_balance;?>" readonly required/>
                </div>
                 <div class="form-group">
                    <label for="title">Subject</label>&nbsp;
                    <input type="text" placeholder="subject" name="subject" id="title" class="form-control" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter title" required/>
                </div>
                <div class="form-group">
                    <label for="class">Message:</label>
                    <textarea class="textarea" name="msg" placeholder="Message To send" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <a href="<?php echo base_url().'back/bill_payment_report/outstanding_report'?>">
                     <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Cancel</button>
                </a>
                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-envelope">&nbsp;</i>send</button>
            </div>
<?php echo form_close();?>
        </div>

</div>
<script src="<?php echo base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    var myckeditor = CKEDITOR.replace('mail_body',{
        enterMode : CKEDITOR.ENTER_BR,
        entities : false,
        basicEntities : false,
        entities_greek : false,
        entities_latin : false,
        htmlDecodeOutput : false
   });

     myckeditor.on('key', function(evt){
        $('#sms_template_form').valid();
    });
});
</script>
