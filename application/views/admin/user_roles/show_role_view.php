

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;&nbsp;Details of Role
             <!--<small>advanced tables</small>-->
        </h3>
       
    </section>
<?php //show($pro_data,1);?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form action="{{ url('admin/group/update/'.$group->id) }}"  id="group_create_form" method="post" name="group_create_form">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Name:</label>
                            <div class="col-lg-6">
                                <input name="group_id" type="hidden" id="group_id" class="form-control" value="<?php echo $group->id?>">
                                <input name="group_name" type="text" id="group_name" class="form-control" value="<?php echo $group->name ?>" readonly="readonly">
                               
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Description: </label>
                            <div class="col-lg-6">
                                <input name="group_desc" type="text" maxlength="40" value="<?php echo $group->description ?>" id="group_desc" class="form-control" readonly="readonly">
                               
                            </div>
                        </div>
                        
                          <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Status: </label>
                            <div class="col-lg-6">
                                <select class="form-control" id="status" name="status" disabled="disabled">
                                  <?php if($group->status == '0') {?>
                                  <option value="0" selected="">Active</option>
                                  <option value="1">Deactivate</option>
                                 <?php  }?>
                               <?php if($group->status == '1') {?>
                                  <option value="0">Active</option>
                                  <option value="1" selected="">Deactivate</option>percentage</option>
                                 <?php  }?> 
                                </select>  
                            </div>
                        </div>
                       
                       
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">

                                <!--<button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit">Save</button>--> 
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>

                         
                    </div>
                <!-- /.box-body -->
                </div></form><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>

    $(document).ready(function() {

    $(document).off('click', '.back').on('click', '.back', function(e)
    {

        window.location.href = BASE_URL + 'admin/group/';
    });
    });
</script>
