</div>
<footer class="main-footer" style="text-align: center;">
    <div class="pull-right hidden-xs">
      <!-- <b>Version</b> 2.3.8 -->
    </div>
    <strong>Copyright &copy; <?= date('Y'); ?> <a href="https://bdsserv.com">BDS STZ Techno Systems Pvt. Ltd. </a></strong> All Rights
    Reserved.
</footer>
  <div class="control-sidebar-bg"></div>
</div>
<?php  $this->load->view('admin/inc/inc_footer_script'); ?>
</body>
</html>
