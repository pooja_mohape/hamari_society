<section class="content">
  <div class="row">
    <form role="form" method="post" id="profile_image" action="<?php echo base_url();?>back/userprofile/myProfile_pic" enctype="multipart/form-data">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Update Image</h3>
          </div>
              <div class="row">   
                <div class="col-md-6 form-group">
                <input type="hidden" name="image_name" id="image_name" value="<?php //echo $img[0]->profile_pic;?>">
                <label class="col-md-3 control-label">Image Upload</label>
                <div class="col-md-9">
                    <label><img src="<?php echo base_url().'upload/profileImages/'. $img[0]->profile_pic;?>" id="image" width="150" height="100">
                    </label>
                    <input type="hidden" name="fname" value="<?php echo $details[0];?>">
                    <input type="file" name="profile_pic" id="profile_pic" required="required">
                    <br>
                    <button type="submit" class="btn btn-primary">Image Change</button>
                </div>
                </div>
                    <!-- <div class="col-md-4 col-md-offset-4">
                      <div class="form-group">
                        <button type="cancel" class="btn btn-primary" id="cancel">Cancel</button>
                          
                      </div>
                    </div> --> 
                </div>
        </div>
      </div>
    </form>
  <!-- </div> -->

    <!-- <form method="post" id="detail_form" action=""> -->
      <div class="col-md-12">
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update Details</h3>
            </div>
              <div class="box-body">
                <form role="form" method="post" id="update_details" action="<?php echo base_url();?>back/userprofile/update_details">
                    <div class=" form-group">
                      <label for="password">First Name</label>
                      <input type="text" class="form-control" name="first_name" value="<?php echo $details[0];?>" placeholder="Enter First name"> 
                    </div>
                    <div class=" form-group">
                      <label for="password">Last Name</label>
                      <input type="text" class="form-control" name="last_name" value="<?php echo $details[1];?>" placeholder="Enter Last name"> 
                    </div>
                    <div class=" form-group">
                      <label for="password">Email</label>
                      <input type="text" class="form-control" name="email" value="<?php echo $details[2]; ?>" placeholder="Enter New email"> 
                    </div>
                    <div class="col-md-offset-4">
                      <div class="form-group">
                          <button type="submit" class="btn btn-primary">Update Details</button>
                      </div>
                    </div>
                </form>
              </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update Password</h3>
            </div>
              <div class="box-body">
                <form role="form" method="post" action="<?php echo base_url();?>back/userprofile/match_pass" enctype="multipart/form-data" id="update_password">
                    <div class=" form-group">
                      <label for="password">Old Password</label>
                      <input type="text" class="form-control" name="old_password" value="" placeholder="old password" > 
                    </div>
                    <div class=" form-group">
                      <label for="password">Enter New Password</label>
                      <input type="text" class="form-control" name="new_password" value="" placeholder="new password" minlength="8"> 
                    </div>
                    <div class=" form-group">
                      <label for="password">Enter Confirm Password</label>
                      <input type="text" class="form-control" name="confirm_password" value="" placeholder="confirm password" minlength="8"> 
                    </div>
                    <div class="col-md-4 col-md-offset-4">
                      <div class="form-group">
                          <button type="submit" class="btn btn-primary">Update Password</button>
                      </div>
                    </div>
                </form>
              </div>
          </div>
        </div>
      </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {

    $("#update_details").validate({
        rules: {
            first_name: {
                required: true,
                lettersonly:true,
                maxlength:15
            },
            last_name: {
                required: true,
                lettersonly:true,
                maxlength:15
            },
            email: {
                required: true,
                email: true,
                email_exists: true
            }
        },
        messages: {
                    
            first_name: {
                required: "Please Enter first name",
                lettersonly:"Please Enter Valid First Name"
            },
            last_name: {
                required: "Please Enter last name",
                lettersonly:"Please Enter Valid Last Name"
            },
            email: {
                required: "Please Enter Email Address",
                email_exists:"Email id already in use"
            }
        }
    });
    
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
      }, "Letters only please");

});
</script>

<script type="text/javascript">
    $(document).ready(function () {

    $("#update_password").validate({
        rules: {
            old_password: {
                required: true
            },
            new_password: {
                required: true
            },
            confirm_password: {
                required: true
            }
        },
        messages: {
                    
            old_password: {
                required: "Please Enter Old Password"
            },
            new_password: {
                required: "Please Enter New Password"
            },
            confirm_password: {
                required: "Please Enter Confirm Password"
            }
        }
    });
});

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#cancel').click(function(){
      $('#image').remove();
    });
  });
</script>
