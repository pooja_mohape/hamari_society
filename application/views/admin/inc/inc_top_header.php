 <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url().'admin/dashboard'; ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>HS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>HAMARI SOCIETY</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php if($this->session->userdata('profile_pic')) {
                  
                ?>
              <img src="<?php echo base_url();?>upload/profileImages/<?php echo $this->session->userdata('profile_pic')?>" class="user-image" alt="User Image">
              <?php }else{?>
              <img src="<?php echo base_url();?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <?php }?>
              <span class="hidden-xs"><?php echo  ucfirst($this->session->userdata('username')); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php if($this->session->userdata('profile_pic')) {?>
                <img src="<?php echo base_url();?>upload/profileImages/<?php echo $this->session->userdata('profile_pic')?>" class="img-circle" alt="User Image">
                <?php }else{?>
                  <img src="<?php echo base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                <?php }?>

                <p>
                  <?php echo  ucfirst($this->session->userdata('username')); ?>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url();?>back/userprofile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url();?>home/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li> 
        </ul>
      </div>
    </nav>
  </header>