  <meta charset="utf-8">
  <title>Welcome Society Management System | BDS Serv</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <!-- Bootstrap 3.3.6 -->

  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link href="<?php echo base_url();?>assets/plugins/jquery-notifications/css/messenger.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url();?>assets/plugins/jquery-notifications/css/messenger-theme-flat.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url();?>assets/custom_file/custom_css.css" rel="stylesheet" type="text/css" media="screen"/>
<!-- Chosen selct start -->
<!-- End -->
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url()?>assets/plugins/timepicker/jquery-timepicker.min.css"> -->
<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/plugins/timepicker/jquery.ptTimeSelect.css">
<!-- fav- icon -->
 <link rel="shortcut icon" href="<?= base_url()?>assets/images/favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?= base_url()?>assets/images/favicon.ico" type="image/x-icon">
<style>
    .error {
      color: #D8000C;
      }
      #hidden{
        display: none;
      }
    .chosen-container-single .chosen-single {
         height: 35px !important;
         line-height: 35px !important;
    }
    .chosen-container-single {
        width: 150px !important;
    }
</style>

<script type="text/javascript" src="<?php echo base_url().'assets/common_js/jquery.min.js'?>"></script>
<!-- <script type="text/javascript" src="<?php //echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script> -->
<link href="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.css'?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url().'assets/common_js/global.js'?>"></script>
<script type="text/javascript">
  var base_url = "<?php echo base_url();?>";
</script>
<style type="text/css">
  .open > .dropdown-menu {
    display: block !important;
    overflow: scroll !important;
    height: 250px !important;
</style>



