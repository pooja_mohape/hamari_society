<?php
$res = $this->db->where('user_id', $this->session->userdata('user_id'))->get('wallet')->result();
?>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7">
                                <h3 style="text-align: center;">
                                    Regenerate Bill
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">

                          <form id="payment_request" name="payment_request" method="post" action="<?php echo base_url().'back/bill_payment/regenerate_bill'?>">
                              <div class="form-group">
                                    <label class="col-lg-2 control-label">Society</label>
                                        <div class="col-lg-4">
                                         <select class="status form-control" name="Society_id" id="Society_id" required="">
                                    <option value="">Select Society</option>
                                   <?php if ($society) {?>
                           <?php foreach ($society as $key => $value) {?>
                         <option value="<?php echo $value->id;?>"><?php echo $value->name;
          		              ?></option>
                            <?php }?>
                                    <?php }?> 
                                        </select>
                                        </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                              <div class="form-group">
                                    <label class="col-lg-2 control-label">Society Users</label>
                                        <div class="col-lg-4">
                                         <select class="status form-control" name="user_id" id="user_id" required="">
                                                <option value="">Select user</option>
                                        </select>
                                        </div>
                                </div>
                            <div class="clearfix trans_ref" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Billing Month </label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="transaction_date" id="transaction_date" value="">
                                    </div>
                                </div>

                              <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                        <div class="col-lg-offset-3">
                                            <button class="btn btn-success" type="submit">Generate Bill</button>
                                        </div>
                                </div>
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<script>
  $( function() {
       $(document).ready(function(){

    $("#transaction_date").datepicker({
        dateFormat: 'mm-yy',
        changeMonth: true,
        changeYear: true,
        maxDate: 0,
        showButtonPanel: true,

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        }
    });

    $("#transaction_date").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
              my: "center top",
              at: "center bottom",
              of: $(this)
            });
    });

});
  } );
</script>


<script type="text/javascript">
  $(document).ready(function(){
    $('#Society_id').change(function(){
    $("#user_id").html('');
    var society_id = $('option:selected').val();
    $.ajax({
      type:'POST',
      data :{society_id:society_id},
      url:'<?php echo base_url();?>back/bill_payment/get_users',
      success:function(data){
        $("#user_id").append(data);
      }
    })
  });
  });
</script>