<?php if($is_admin){?>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    List Of Bill Payment                                     
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">
                         <div class="form-group">
                                    <label class="col-lg-2 control-label">Transaction Type</label>
                                        <div class="col-lg-4"> 
                                         <select class="status form-control" name="transaction_type" id="transaction_type" required="" readonly>
                                                <option value="<?php echo $bill_payment->transaction_type?>"><?php echo $bill_payment->transaction_type?></option>
                                        </select>
                                        </div>
                                </div>

                            <?php if($bill_payment->transaction_type != 'cash'){?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                               <div class="form-group amount">
                                     <label class="col-lg-2 control-label">Amount </label>
                                     <div class="col-lg-4">
                                         <input type="text" class="form-control" name="amount" id="amount" value="<?php echo $bill_payment->amount?>" readonly>
                                    </div>
                               </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group bank_name">
                                    <label class="col-lg-2 control-label" for="bank name">Bank Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="bank_name" id="bank_name" value="<?php echo $bill_payment->bank_name?>" readonly>
                                    </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group acc_no">
                                    <label class="col-lg-2 control-label" for="bank name">Bank Acc no</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="acc_no" id="acc_no" value="<?php echo $bill_payment->bank_acc_no?>"  readonly>
                                    </div>
                                </div>

                                <?php if($bill_payment->transaction_type == 'netbanking'){?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group branch_name">
                                    <label class="col-lg-2 control-label" for="bank name">Branch Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="branch_name" id="branch_name" value="<?php echo $bill_payment->branch_name?>" readonly>
                                    </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group ifsc">
                                    <label class="col-lg-2 control-label" for="bank name">Ifsc code</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="ifsc" id="ifsc" value="<?php echo $bill_payment->ifsc_code;?>" readonly>
                                    </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group trans_ref">
                                    <label class="col-lg-2 control-label" for="trans_ref">Transaction Ref No</label>
                                    <div class="col-lg-4">
                                       <input type="text" class="form-control" name="trans_ref" id="trans_ref" value="<?php echo $bill_payment->transaction_ref_no;?>" readonly>
                                    </div>
                                </div>
                             <?php }?>
                             <?php if($bill_payment->transaction_type == 'cheque'){?>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group check_no">
                                    <label class="col-lg-2 control-label" for="trans_ref">Check No</label>
                                    <div class="col-lg-4">
                                       <input type="text" class="form-control" name="check_no" id="check_no" value="<?php echo $bill_payment->transaction_ref_no?>" readonly>
                                    </div>
                                </div>
                              <?php }?>
                            <?php }?>

                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Payment Date </label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="transaction_date" id="transaction_date" value="<?php echo $bill_payment->transaction_date?>" readonly>
                                    </div>
                                </div>
                                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                                   <div class="form-group"  id="reason">
                                         <label class="col-lg-2 control-label">Payment Remark: </label>
                                         <div class="col-lg-4">
                                               <textarea id="comment" class="form-control" style="height: 120px;border: 1px outset" placeholder="<?php echo $bill_payment->payment_remark?>" name="comment" row="3" readonly></textarea>
                                         </div>
                                   </div>

                                   <div class="clearfix" style="height: 10px;clear: both;"></div>
                                  <?php if($is_admin == true){?>  
                               <div class="form-group"  id="reason">
                                         <label class="col-lg-2 control-label">Admin Remark: </label>
                                         <div class="col-lg-4">
                                               <textarea id="comment" class="form-control" style="height: 120px;border: 1px outset" placeholder="<?php echo $bill_payment->admin_remark?>" name="comment" row="3" readonly></textarea>
                                         </div>
                                   </div>  
                               <?php } ?>
                                 <div class="clearfix" style="height: 10px;clear: both;"></div>

                                <form id="payment_request" name="payment_request" method="post" action="<?php echo  base_url().'back/bill_payment/update_payment'?>">
                                  <input type="hidden" name="id" value="<?php echo $bill_payment->id?>">
                                  <?php if($bill_payment->is_approved == "Pending"){?>
                                 <div class="form-group">
                                    <label class="col-lg-2 control-label">Status</label>
                                        <div class="col-lg-4"> 
                                         <select class="status form-control" name="status" id="status" required="">
                                          <option value="">Select Status</option>
                                          <option value="approved">Approved</option>
                                          <option value="rejected">Rejected</option>

                                        </select>
                                        </div>
                                </div>
                                <?php }else{?>
                                     <div class="form-group">
                                    <label class="col-lg-2 control-label">Status</label>
                                        <div class="col-lg-4"> 
                                         <select class="status form-control" name="status" id="status" required="" readonly>
                                          <option value=""><?php echo $bill_payment->is_approved?></option>
                                        </select>
                                        </div>
                                </div>
                                <?php }?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                                  <?php if($bill_payment->is_approved == "Pending"){?>  
                               <div class="form-group"  id="reason">
                                         <label class="col-lg-2 control-label">Admin Remark: </label>
                                         <div class="col-lg-4">
                                               <textarea id="comment" class="form-control" style="height: 120px;border: 1px outset" placeholder="write your comment here" name="comment" row="3"></textarea>
                                         </div>
                                   </div>  
                              <div class="clearfix" style="height: 10px;clear: both;"></div>
                              <?php }?>
                                <div class="form-group">
                                        <div class="col-lg-offset-3">
                                            <?php if($bill_payment->is_approved == "Pending"){?>  
                                           <button class="btn btn-primary" type="submit">Update</button> 
                                          <?php }?>&nbsp;&nbsp;
                                         
                                            <a class="btn btn-primary" href="<?php echo base_url().'back/bill_payment'?>" type="button">Back</a> 
                                        </div>
                                </div>
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php }else  {?>
<section class="content" id="user_view">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Payment Request
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">
                         <div class="form-group">
                                    <label class="col-lg-2 control-label">Transaction Type</label>
                                        <div class="col-lg-4"> 
                                         <select class="status form-control" name="transaction_type" id="transaction_type" required="" readonly>
                                                <option value="<?php echo $bill_payment->transaction_type?>"><?php echo $bill_payment->transaction_type?></option>
                                        </select>
                                        </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                               <div class="form-group amount">
                                     <label class="col-lg-2 control-label">Amount </label>
                                     <div class="col-lg-4">
                                         <input type="text" class="form-control" name="amount" id="amount" value="<?php echo $bill_payment->amount?>" readonly>
                                    </div>
                               </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                               
                               <?php if($bill_payment->transaction_type != 'cash'){?>
                                <div class="form-group bank_name">
                                    <label class="col-lg-2 control-label" for="bank name">Bank Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="bank_name" id="bank_name" value="<?php echo $bill_payment->bank_name?>" readonly>
                                    </div>
                                </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="form-group acc_no">
                                    <label class="col-lg-2 control-label" for="bank name">Bank Acc no</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="acc_no" id="acc_no" value="<?php echo $bill_payment->bank_acc_no?>"  readonly>
                                    </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <?php if($bill_payment->transaction_type == 'netbanking') {?>
                            <div class="form-group branch_name">
                                    <label class="col-lg-2 control-label" for="bank name">Branch Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="branch_name" id="branch_name" value="<?php echo $bill_payment->branch_name?>" readonly>
                                    </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group ifsc">
                                    <label class="col-lg-2 control-label" for="bank name">Ifsc code</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="ifsc" id="ifsc" value="<?php echo $bill_payment->ifsc_code?>" readonly>
                                    </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group trans_ref">
                                    <label class="col-lg-2 control-label" for="trans_ref">Transaction Ref No</label>
                                    <div class="col-lg-4">
                                       <input type="text" class="form-control" name="trans_ref" id="trans_ref" value="<?php echo $bill_payment->transaction_ref_no?>" readonly>
                                    </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                                               <?php }?>

                                <?php if($bill_payment->transaction_type == 'cash') {?>                               
                                <div class="form-group check_no">
                                    <label class="col-lg-2 control-label" for="trans_ref">Check No</label>
                                    <div class="col-lg-4">
                                       <input type="text" class="form-control" name="check_no" id="check_no" value="<?php echo $bill_payment->transaction_ref_no?>" readonly>
                                    </div>
                                </div>
                            <?php }?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <?php }?>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Payment Date </label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="transaction_date" id="transaction_date" value="<?php echo $bill_payment->transaction_date?>" readonly>
                                    </div>
                                </div>
                                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                                   <div class="form-group"  id="reason">
                                         <label class="col-lg-2 control-label">Admin Remark: </label>
                                         <div class="col-lg-4">
                                               <textarea id="comment" class="form-control" style="height: 120px;border: 1px outset" placeholder="<?php echo $bill_payment->admin_remark?>" name="comment" row="3" readonly></textarea>
                                         </div>
                                   </div> 

                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                   <div class="form-group"  id="reason">
                                         <label class="col-lg-2 control-label">Payment Remark: </label>
                                         <div class="col-lg-4">
                                               <textarea id="comment" class="form-control" style="height: 120px;border: 1px outset" placeholder="<?php echo $bill_payment->payment_remark?>" name="comment" row="3" readonly></textarea>
                                         </div>
                                   </div>   
                              <div class="clearfix" style="height: 10px;clear: both;"></div>
                                 <div class="form-group">
                                    <label class="col-lg-2 control-label">Status</label>
                                        <div class="col-lg-4"> 
                                         <select class="status form-control" name="status" id="status" required="" readonly>
                                          <option value=""><?php echo $bill_payment->is_approved?></option>
                                        </select>
                                        </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                                 
                                <div class="form-group">

                                        <div class="col-lg-offset-3">
                                            <a class="btn btn-primary" href="<?php echo base_url().'back/bill_payment'?>" type="button">Back</a> 
                                        </div>
                                </div>
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
    <?php }?>
<!-- <script type="text/javascript">
    var transaction_type = $("#transaction_type").find('option:selected').attr('value');
      if(transaction_type == 'cheque')
      {
       $(".bank_name").show();
        $(".trans_ref").hide();
        $(".acc_no").show();
        $(".ifsc").hide();
        $(".branch_name").hide();
        $(".check_no").show();    
      }
    else if(transaction_type == 'netbanking')
      {
        $(".bank_name").show();
        $(".trans_ref").show();
        $(".acc_no").show();
        $(".ifsc").show();
        $(".branch_name").show();
        $(".check_no").hide();
      }
      else if(transaction_type == 'netbanking')
      {
        $(".bank_name").hide();
        $(".trans_ref").hide();
        $(".acc_no").hide();
        $(".ifsc").hide();
        $(".branch_name").hide();
        $(".check_no").hide();
      }
</script>
 -->