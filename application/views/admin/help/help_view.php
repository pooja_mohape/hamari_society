<!DOCTYPE html>
<html>
<head>
<title>Tree Structure</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    $(function () {
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
    });
});
});

function myFunction() {
    var popup = document.getElementById("house");
    popup.classList.toggle("show");
}
</script>
<style type="text/css">
.popup {
    position: relative;
    display: inline-block;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* The actual popup */
.popup .popuptext {
    visibility: hidden;
    width: 160px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 8px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -80px;
}

/* Popup arrow */
.popup .popuptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.popup .show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;}
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}
    .tree {
    min-height:20px;
    padding:19px;
    margin-bottom:20px;
    background-color:#fbfbfb;
    border:1px solid #999;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
}
.tree li {
    list-style-type:none;
    margin:0;
    padding:10px 5px 0 5px;
    position:relative
}
.tree li::before, .tree li::after {
    content:'';
    left:-20px;
    position:absolute;
    right:auto
}
.tree li::before {
    border-left:1px solid #999;
    bottom:50px;
    height:100%;
    top:0;
    width:1px
}
.tree li::after {
    border-top:1px solid #999;
    height:20px;
    top:25px;
    width:25px
}
.tree li span {
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border:1px solid #999;
    border-radius:5px;
    display:inline-block;
    padding:3px 8px;
    text-decoration:none
}
.tree li.parent_li>span {
    cursor:pointer
}
.tree>ul>li::before, .tree>ul>li::after {
    border:0
}
.tree li:last-child::before {
    height:30px
}
.tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
}
</style>

</head>
<body>

    <h3 align="center">Hamari Society Help</h3>
    <div class="tree well">
    <ul>
        <li>
            <span><i class="icon-folder-open"></i> My Account</span>
            <ul>
                <li>
                    <span><i class="icon-leaf"></i> My Profile</span>
                    &nbsp;
                            <a href="<?php echo base_url();?>back/userprofile" title="Go To View House"><i class="fa fa-anchor"></i></a>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Update Profile Image</span><!-- &nbsp;
                            <a href="<?php echo base_url();?>//back/userprofile" title="Go To View House"><i class="fa fa-anchor"></i></a> -->
                            <ul>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Upload Profile Image of Society</span>
                                </li>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Change Profile Image of Society</span>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Update Member Details</span>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Update Personal Details</span>
                            <ul>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Update First Name</span>
                                </li>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Update Last Name</span>
                                </li>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Update Email</span>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> Update Password</span>
                            <!-- &nbsp;
                            <a href="" title="Go To View House"><i class="fa fa-anchor"></i></a> -->
                            <ul>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Enter Old Password</span>
                                </li>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Enter New Password</span>
                                </li>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Enter Confirm Password</span>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <span><i class="icon-folder-open"></i> Registration</span>
            <ul>
                <li>
                    <span><i class="icon-minus-sign"></i> House</span>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Add House</span>&nbsp;
                            <a href="<?php echo base_url()?>back/registration/houseView" title="Go To View House"><i class="fa fa-anchor"></i></a>
                            <ul>
                                <li>
                                    <p><i class="icon-leaf"></i>Add House Type
                                        <ul>
                                            <li>
                                            <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                            <span>Add House Type of User House</span>
                                            </li>
                                        </ul>
                                    </p>
                                </li>
                                <li>
                                    <p><i class="icon-leaf"></i>Add Maintenance Amount
                                        <ul>
                                            <li>
                                            <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                            <span>Add Maintenance Amount of User House</span>
                                            </li>
                                        </ul>
                                    </p>
                                </li>
                                <li>
                                    <p><i class="icon-leaf"></i>Add Lease Amount
                                       <ul>
                                            <li>
                                            <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                            <span>Add Lease Amount of User House</span>
                                            </li>
                                        </ul>
                                    </p>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> View House</span>&nbsp;
                                <a href="<?php echo base_url();?>back/registration/allHouse" title="Go To View House"><i class="fa fa-anchor"></i></a>
                                <p>
                                    <ul>
                                        <li>
                                            <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                            <span>View of User House List</span>
                                        </li>
                                    </ul>
                                </p>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> Import House</span>&nbsp;
                                <a href="<?php echo base_url();?>back/registration/houseCsvView" title="Go To Import House"><i class="fa fa-anchor"></i></a>
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Download the sample CSV file.</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Enter the data in the downloaded file.</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Keep the data same as it's in the website <br/>e.g. society name in CSV should be exact same as website, etc.</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Only Csv Format Allowed </span>
                                    </li>
                                </ul>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> View Society</span>
                            <a href="<?php echo base_url();?>back/registration/allSociety" title="Go To Import House"><i class="fa fa-anchor"></i></a>
                            <p>
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>View Society Details</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>
                <li>
                    <span><i class="icon-minus-sign"></i> Parking</span>
                    <a href="<?php echo base_url();?>back/societycharges" title="Go To Import House"><i class="fa fa-anchor"></i></a>
                    <ul>
                        <li>
                            <p><i class="icon-leaf"></i>Vehicle Type
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>Add Vehicle Type of Society Member</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                        <li>
                            <p><i class="icon-leaf"></i> Parking Charges Per Vehicle
                               <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>Add Parking Charges Per Vehicle of Society Member</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>
                <li>
                    <span><i class="icon-minus-sign"></i> Facility</span>
                    <a href="<?php echo base_url();?>back/societycharges" title="Go To Import House"><i class="fa fa-anchor"></i></a>
                    <ul>
                        <li>
                            <p><i class="icon-leaf"></i>Facility Name
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>Add Facility Name</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                        <li>
                            <p><i class="icon-leaf"></i> Adult Charges
                               <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>Add Adult Charges</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                        <li>
                            <p><i class="icon-leaf"></i> Child Charges
                               <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>Add Child Charges</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <span><i class="icon-folder-open"></i> Users List</span>
            <ul>
                <li>
                    <span><i class="icon-leaf"></i> Add Member</span>
                    <a href="<?php echo base_url();?>back/registration/userView" title="Go To Import House"><i class="fa fa-anchor"></i></a>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Add User & Allocate House</span>
                            <ul>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Enter Personal Details</span>
                                </li>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                    <span style="color: #337ab7">Select House</span>
                                </li>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Assign Role to Admin Or Member</span>
                                </li>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Upload Image of House</span>
                                </li>
                                <li>
                                    <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Member Authority</span>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Members List</span>
                     <a href="<?php echo base_url();?>back/registration/alluser" title="Go To Members List"><i class="fa fa-anchor"></i></a>
                    <p>
                        <ul>
                            <li>
                                <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                <span>View of Members List</span>
                            </li>
                        </ul>
                    </p>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Import Users</span>
                    <a href="<?php echo base_url();?>back/registration/userCsvView" title="Go To Members List"><i class="fa fa-anchor"></i></a>
                    <ul>
                        <li>
                            <i class="fa fa-arrow-right" style="font-size:20px"></i>
                            <span style="color: #337ab7">Download the sample CSV file.</span>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" style="font-size:20px"></i>
                            <span style="color: #337ab7">Enter the data in the downloaded file.</span>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" style="font-size:20px"></i>
                            <span style="color: #337ab7">Keep the data same as it's in the website <br/>e.g. society name in CSV should be exact same as website, etc.</span>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" style="font-size:20px"></i>
                            <span style="color: #337ab7">Only Csv Format Allowed </span>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <span><i class="icon-folder-open"></i> My Payment</span>
            <ul>
                <li>
                    <span><i class="icon-leaf"></i> Bill Payment</span>
                    <a href="<?php echo base_url();?>back/bill_payment" title="Go To Bill Payment"><i class="fa fa-anchor"></i></a>
                        <p>
                        <ul>
                            <li>
                                <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                <span>View of Payment List According to From Date to Till Date with Transaction Status</span>
                            </li>
                        </ul>
                    </p>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Monthly Bill Payment</span>
                    <a href="<?php echo base_url();?>back/bill_payment_report" title="Go To Bill Payment"><i class="fa fa-anchor"></i></a>
                    <p>
                        <ul>
                            <li>
                                <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                <span>View of Monthly Bill Payment List According to Users of Society with Selected Month</span>
                            </li>
                        </ul>
                    </p>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Net Banking</span>
                    <a href="<?php echo base_url();?>back/netbanking" title="Go To Net Banking"><i class="fa fa-anchor"></i></a>
                    <ul>
                        <li>
                            <p><i class="icon-leaf"></i>Add Net Banking
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>Add Net Banking Details</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                        <li>
                            <p><i class="icon-leaf"></i>Net Banking Details
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>View of Net Banking Details List</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Outstanding Report</span>
                    <a href="<?php echo base_url();?>back/bill_payment_report/outstanding_report" title="Go To Net Banking"><i class="fa fa-anchor"></i></a>
                    <ul>
                        <li>
                            <p><i class="icon-leaf"></i>View Transaction
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>View of Users Transaction Passbook / List by Selecting Users</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                        <li>
                            <p><i class="icon-leaf"></i>Send Mail Remainder
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Select User</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Add Subject</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Add Mail Content</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <span><i class="icon-folder-open"></i> SMS</span>
            <a href="<?php echo base_url();?>back/sms/smsview" title="Go To SMS"><i class="fa fa-anchor"></i></a>
            <ul>
                <li>
                    <span><i class="icon-leaf"></i> SMS Notification</span>
                        <p>
                        <ul>
                            <li>
                                <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                <span>Select SMS Notification</span>
                            </li>
                        </ul>
                    </p>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Message To</span>
                    <p>
                        <ul>
                            <li>
                                <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                <span>Select Contact Person to Send SMS</span>
                            </li>
                        </ul>
                    </p>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Users To Send</span>
                    <p>
                        <ul>
                            <li>
                                <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                <span>Select & Unselect Users to Send SMS</span>
                            </li>
                        </ul>
                    </p>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Message</span>
                        <p>
                            <ul>
                                <li>
                                    <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                    <span>Message to Send</span>
                                </li>
                            </ul>
                        </p>
                </li>
            </ul>
        </li>
        <li>
            <span><i class="icon-folder-open"></i> Services</span>
            <ul>
                <li>
                    <span><i class="icon-leaf"></i> Facility</span>
                    <a href="<?php echo base_url();?>back/Facility/allMember" title="Go To Net Banking"><i class="fa fa-anchor"></i></a>
                    <ul>
                        <li>
                            <p><i class="icon-leaf"></i>Allocate Facility
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Select Facility</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Select User</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Add Adult Count</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Add Child Count</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Add Total Members in the House</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                        <li>
                            <p><i class="icon-leaf"></i>Facility Allocation List
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>View of Facility Allocation List</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Parking</span>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Slot Details</span>
                            <ul>
                                <li>
                                    <p><i class="icon-leaf"></i>Add Parking Slot
                                        <ul>
                                            <li>
                                                <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                                <span style="color: #337ab7">Add Slot Number</span>
                                            </li>
                                            <li>
                                                <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                                <span style="color: #337ab7">Add Vehicle Type</span>
                                            </li>
                                        </ul>
                                    </p>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> Assign Parking Slot</span>
                            <p>
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Select Users</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrow-right" style="font-size:20px"></i>
                                        <span style="color: #337ab7">Select Parking Slot / Assign Parking Slot</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> Assign Parking Slot List</span>
                            <p>
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>View of Assign Parking Slots List</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Document Collection</span>
                    <a href="<?php echo base_url();?>back/document_collection" title="Go To Document Collection"><i class="fa fa-anchor"></i></a>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Add Documents</span>
                            <ul>
                                <p>
                                    <ul>
                                        <li>
                                            <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                            <span>Add Documents Details</span>
                                        </li>
                                    </ul>
                                </p>
                            </ul>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> All Documents</span>
                            <p>
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>View All Documents Details List</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>

                <li>
                    <span><i class="icon-leaf"></i> Notices</span>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Send Notice</span>
                                <p>
                                    <ul>
                                        <li>
                                            <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                            <span>Add Documents Details</span>
                                        </li>
                                    </ul>
                                </p>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> Notice List</span>
                            <p>
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>View Notice List</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>

                <li>
                    <span><i class="icon-leaf"></i> Complaint</span>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Complaint List</span>
                                <p>
                                    <ul>
                                        <li>
                                            <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                            <span>View Complaint List</span>
                                        </li>
                                    </ul>
                                </p>
                        </li>
                    </ul>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Meetings</span>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Create Meeting</span>
                                <p>
                                    <ul>
                                        <li>
                                            <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                            <span>Add Meeting Details &<br/>Send SMS & Mail OR Both</span>
                                        </li>
                                    </ul>
                                </p>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> Meeting List</span>
                            <p>
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>View Meeting List</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>
                <li>
                    <span><i class="icon-leaf"></i> Event</span>
                    <ul>
                        <li>
                            <span><i class="icon-leaf"></i> Add Event</span>
                                <p>
                                    <ul>
                                        <li>
                                            <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                            <span>Add Event Title, Date, Time</span>
                                        </li>
                                    </ul>
                                </p>
                        </li>
                        <li>
                            <span><i class="icon-leaf"></i> View Event</span>
                            <p>
                                <ul>
                                    <li>
                                        <i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                                        <span>View Arrange Event in Calender</span>
                                    </li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
</div>

</body>
</html>