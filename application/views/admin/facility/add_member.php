<section class="content">
  <div class="row">
	    <div class="col-md-12">
	        <div class="col-md-6">
	           <h3 style="">View Users Details</h3>
	        </div>
	         <div class="col-md-6">
	         	<?php $ses_id = $this->session->userdata('role_id');if( $ses_id== SOCIETY_SUPERUSER || $ses_id==SOCIETY_ADMIN) {?>
	         	 <a href="<?php echo base_url();?>back/registration/userView" class="btn btn-primary add_btn">&nbsp;Add User</i></a>&nbsp;&nbsp;
	           <a href="<?php echo base_url();?>back/facility/memberfacility" class="btn btn-primary add_btn">Allocate Facility</i></a>
	           &nbsp;&nbsp;
	           <a href="<?php echo base_url();?>back/parking/index" class="btn btn-primary add_btn">Allocate Parking</i></a>&nbsp;&nbsp;
	           <?php }?>
	           <a href="<?php echo base_url();?>back/registration/alluser" class="btn btn-danger add_btn"><i class="fa fa-arrow-left">&nbsp;Back</i></a>
	        </div>
	    </div>
  </div>
      <div class="row">
 			<div class="col-md-12">

 			  <div class="box box-primary" id="box-primary">
 				<table class="table table-condensed table-hover table-responsive" id="alluser">
 					<thead>
 						<tr>
 							<th>Sr. No</th>
 							<th>Facility Name</th>
 							<th>Users</th>
 							<th>Adult Count</th>
 							<th>Child Count</th>
 							<th>Total Persons</th>
 							<th>Total Fees</th>
 							<th>Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php $count ='0';

 						foreach($member as $members){
 							$id = $members->fid;
 							$fid = $members->id;
 							$uid = $members->uid;
 							
 							$count++;?>
 						<tr>
 							<td><?= $count; ?></td>
 							<td><?= $members->facility_name;?></td>
 							<td><?= $members->username;?></td>
 							<td><?= $members->adult_count;?></td>
 							<td><?= $members->child_count;?></td>
 							<td><?= $members->total_member;?></td>
 							<td><?= $members->total_fees;?></td>
 							<?php if ($this->session->userdata('role_id')==SOCIETY_MEMBER){?>
 								<td><a href="<?= base_url().'back/Facility/showuser_facility/'.$id.'/'.$fid.'/'.$uid; ?>" class="btn btn-info" id="update" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a></td>
 							<?php } else{?>
 							<td> <a href="<?= base_url().'back/Facility/get_user_fac/'.$id.'/'.$fid.'/'.$uid; ?>" class="btn btn-warning" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> | <a href="<?= base_url().'back/Facility/showuser_facility/'.$id.'/'.$fid.'/'.$uid; ?>" class="btn btn-info" id="update" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a> | <a href="<?= base_url().'back/Facility/delete_user_fac/'.$id.'/'.$fid.'/'.$uid;?>" onclick="confirm('Are You Sure ?')" class="btn btn-danger" id="delete" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a></td>
 						<?php } ?>
 						</tr>
 						<?php } ?>
 					</tbody>
 				</table>
 			   </div>
 			</div>
      </div>
 </section>
 <style type="text/css">
 	#box-primary{
 		padding:10px;
 	}
 </style>
 	<script type="text/javascript">
 	$(document).ready(function(){
 		$('#alluser').DataTable();
 		// $('#delete').click(function() {
 		// var choice = confirm('Are You Sure, To Delete this User');
 		// if(choice!=true){
 		// 	return false;
 		// 	}
 		// });
 	});
 </script>