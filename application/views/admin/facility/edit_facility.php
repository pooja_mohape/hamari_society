<?php //print_r($facility);?>
<section class="content">
      <div class="row">
 			<div class="col-md-12">
 				<div class="row">
 					<div class="col-md-12">
 						<div class="col-md-6">
 							 <h3 style="">Facility List</h3>
 						</div>
 						<div class="col-md-6">

 							<?php if($this->session->userdata('role_id') == SOCIETY_SUPERUSER){?>
 							<a href="<?php echo  base_url().'back/Facility'?>">
 							 <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-primary add_btn">Add Facility</button></span>
 							</a>
 							<?php }?>
 						</div>
 					</div>
 				</div>
 			  <div class="box box-primary" id="box-primary">
 				<table class="table table-condensed table-hover table-responsive" id="alluser">
 					<thead>
 						<tr>
 							<th>Sr. No</th>
 							<th>Facility Name</th>
 							<th>Adult Charges</th>
 							<th>Child Charges</th>
 							<!-- <th>Maintenance Cost</th> -->
 							<th>Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php $count ='0'; foreach($facilities as $user){
 							$id = $user->id;
 							$count++;?>
 						<tr>
 							<td><?= $count; ?></td>
 							<td><?= $user->facility_name;?></td>
 							<td><?= $user->adult_charges;?></td>
 							<td><?= $user->child_charges;?></td>
							 <!-- <td><?= $user->maintenance_cost;?></td> -->
							<?php if($this->session->userdata('role_id')== SOCIETY_MEMBER){?>
								<td><a href="<?= base_url().'back/Facility/view_facility/'.$id; ?>" class="btn btn-info" id="update" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a></td>
							<?php } else {?>
 							<td>
								 <a href="<?= base_url().'back/Facility/update_facility/'.$id; ?>" 
								 class="btn btn-warning" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> |
								<a href="<?= base_url().'back/Facility/view_facility/'.$id; ?>" class="btn btn-info" id="update" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a> |
								<a href="<?= base_url().'back/Facility/delete_facility/'.$id; ?>" onclick="facility_namereturn confirm('Are You Sure ?')" class="btn btn-danger" id="delete" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
							<?php } ?>
							</td>
 						</tr>
 						<?php } ?>
 					</tbody>
 				</table>
 			   </div>
 			</div>
      </div>
 </section>
 <style type="text/css">
 	#box-primary{
 		padding:10px;
 	}
 </style>
 <script type="text/javascript">
 	$(document).ready(function(){
 		$('#alluser').DataTable();
 		$('#delete').click(function() {
 		var choice = confirm('Are You Sure, To remove this facility');
 		if(choice!=true){
 			return false;
 			}
 		});
 	});
 </script>