<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Allocate Facility
                                     <hr>
                                </h3>
                            </div>
                        </div>
                        <div class="box-body">
                         
                            <form id="facility_member" name="facility_member" method="post" action="<?php echo  base_url().'back/facility/update_user_fac'?>">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?php echo $user_fac[0]->id;?>">
                                    <input type="hidden" name="fid" value="<?php echo $facility_drop[0]->id;?>">
                                    <label class="col-lg-2 control-label">Facility</label>
                                        <div class="col-lg-4"> 
                                            <select class="facility form-control" name="facilty_id" id="facilty_id" readonly="readonly">
                                                <option value="">Select Facility</option>
                                                <?php foreach($facility_drop as $row){?>
                                                    <option value="<?php echo $row->id?>"
                                                     <?php if($row->id == $user_fac[0]->facilty_id){ ?>
                                                        selected='selected'
                                                        <?php }?> >
                                                        <?php echo $row->facility_name;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Users</label>
                                        <div class="col-lg-4"> 
                                            <select class="users form-control" name="user_id" id="user_id" readonly="readonly">
                                            <option value="">Select Users</option>
                                            <?php foreach($user_drop as $value){?>
                                            <option value="<?php echo $value->id?>"
                                                <?php if($value->id == $user_fac[0]->user_id) { ?>
                                                selected='selected'
                                                <?php }?>>
                                                <?php echo $value->first_name . $value->last_name;?>
                                            </option>
                                            <?php }?>
                                            </select>
                                        </div>
                                </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="adult_count">Adult Count</label>
                                        <div class="col-lg-4">
                                            <?php if($user_fac[0]->adult_count == '0'){?>
                                            <input type="text" class="num form-control" name="adult_count" id="adult_count" readonly="readonly" value="<?php echo $user_fac[0]->adult_count?>">
                                            <?php }else{?>
                                            <input type="text" class="num form-control" name="adult_count" id="adult_count" value="<?php echo $user_fac[0]->adult_count?>">
                                            <?php }?>
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="child_count">Child Count</label>
                                        <div class="col-lg-4">
                                            <?php if($user_fac[0]->child_count == '0'){?>
                                            <input type="text" class="num form-control" name="child_count" id="child_count" readonly="readonly" value="<?php echo $user_fac[0]->child_count?>">
                                            <?php } else{?>
                                            <input type="text" class="num form-control" name="child_count" id="child_count" value="<?php echo $user_fac[0]->child_count?>">
                                            <?php }?>
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="total_member">Total Members</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="total_member" id="total_member" value="<?php echo $user_fac[0]->total_member?>">
                                        </div>
                               </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-1 control-label" for="total_fees"></label>
                                        <div class="col-lg-5">
                                            <div class="box-footer" style="padding-left: 80px;" align="center">
                                                <button type="submit" class="btn btn-primary" id="add_member">Update</button>
                                                <a class="btn btn-danger" href="<?php echo  base_url().'back/Facility/allMember'?>" type="button">Back</a> 
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

<script type="text/javascript">
    $(document).ready(function () {

     $("#facility_member").validate({
        rules: {
            adult_count: {
                required: true,
                number:true,
                min:0,
                minlength:1,
                maxlength:4
            },
            child_count: {
                number: true,
                min:0,
                minlength:1,
                maxlength:4
            }
        },

        errorPlacement: function (error, element) {
                   
            if(element.attr("name") === "facilty_id") {
                error.appendTo(element.parent("div") );
            } else if(element.attr("name") === "user_id") {
                error.appendTo(element.parent("div") );
            } else {
                error.insertAfter(element);
            }
                
        },
        messages: {        
            adult_count: {
                required: "Please Enter Numbers Only",
                number: "Please Enter valid Adult Count",
                min: "Total member can not be less than 0",
                minlength:"Minimum 1 Digit",
                maxlength:"Maximum 4 Digit Allowed"
            },
            child_count: {
                number: "Please Enter valid Child Count",
                min: "Total member can not be less than 0",
                minlength:"Minimum 1 Digit",
                maxlength:"Maximum 4 Digit Allowed"
            }
        }
    });
});

</script>
<script type="text/javascript">
        $(document).ready(function(){
        var adult_count = '';
        var child_count = '';
        var total='';
        $('#adult_count').blur(function(){
             adult_count = $('#adult_count').val();
             child_count = $('#child_count').val();
              if(adult_count==''){
                adult_count=0;
             }
              if(child_count==''){
                child_count=0;
             }
        });

        $('#child_count').blur(function(){
             adult_count = $('#adult_count').val();
             child_count = $('#child_count').val();
              if(adult_count==''){
                adult_count=0;
             }
              if(child_count==''){
                child_count=0;
             }
        });
        
        $('#child_count').on('foucusout blur',function() {
            total = parseInt(child_count) + parseInt(adult_count);
            if(total == 0){
                $('#count').html('Both count should not be zero');
            } else {
                $('#count').html('');
            }
            $('#total_member').val(total);
        });

         $('#adult_count').on('foucusout blur',function(){
            total = parseInt(child_count) + parseInt(adult_count);
            if(total == 0){
                $('#count').html('Both count should not be zero');
            } else {
                $('#count').html('');
            }
            $('#total_member').val(total);
        });
    });
</script>