<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" method="post" action="<?=base_url();?>back/societycharges" id="user_form" enctype="multipart/form-data">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">View Facility </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div class="form-group row" id="facility_name">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="facility_name">Facility Name</label>
                    <div class="col-sm-5 col-sm-offset-1">
                      <input type="text" class="form-control" name="facility_name" id="facility_name" value="<?php echo $view_facility[0]->facility_name?>" readonly>
                   </div>
                </div>

                <div class="form-group row" id="adult_charges">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="adult_charges">Adult Charges</label>
                    <div class="col-sm-5 col-sm-offset-1">
                      <input type="text" class="form-control" name="adult_charges" id="adult_charges" value="<?php echo $view_facility[0]->adult_charges?>" readonly>
                   </div>
                </div>

                <div class="form-group row" id="child_charges">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="child_charges">Child Charges</label>
                    <div class="col-sm-5 col-sm-offset-1">
                      <input type="text" class="form-control" name="child_charges" id="child_charges" value="<?php echo $view_facility[0]->child_charges?>" readonly>
                   </div>
                </div>

                <div class="form-group row" id="maintenance_cost" style="display: none;">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="maintenance_cost">Maintenance Cost</label>
                    <div class="col-sm-5 col-sm-offset-1">
                      <input type="text" class="form-control" name="maintenance_cost" id="maintenance_cost" value="<?php echo $view_facility[0]->maintenance_cost?>" readonly>
                   </div>
                </div>
              </div>
              <div class="box-footer" align="center" style="padding-left: 0px;">
                <a href="<?php echo base_url().'back/societycharges'?>">
                <button class="btn btn-danger">Back</button>
                </a>
              </div>
            </div>
          </div>
        </div>
            </form>
          </div>
        </div>
      </div>
</section>
