<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" method="post" action="<?= base_url();?>back/registration/updateuser" id="user_form" enctype="multipart/form-data">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit User Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="first_name">First Name *</label>
                  <div class="col-sm-5">
                  <input type="text" class="form-control" name="first_name" id="first_name" value="<?= $user[0]->first_name ;?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Last Name *</label>
                    <div class="col-sm-5">
                    <input type="text" class="form-control" name="last_name" id="last_name" value="<?= $user[0]->last_name ;?>">
                  </div>
                </div>
                <div class="form-group row">
                <label class="col-sm-3 col-form-label col-sm-offset-1" for="user_role">User Role *</label>
                  <div class="col-sm-5">
                    <input type="radio"  name="user_role" value="4" id="member">Member
                    <input type="radio" name="user_role" value="3" id="admin">
                    Admin
                  </div>
              </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="username">Username *</label>
                  <div class="col-sm-5">
                  <input type="hidden" name="uid" value="<?= $user[0]->id;?>" />
                  <input type="text" class="form-control" name="username" id="username" value="<?= $user[0]->username;?>" readonly>
                </div>
              </div>
              <?php $myrole = $this->session->userdata('role_id'); if($myrole==SUPERADMIN){?>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_id"> Society Name *</label>
                <div class="col-sm-5">
                <select name="society_id" class="form-control" id="society_id">
                  <option value="<?= $user[0]->society_id?>"><?= $user[0]->society_name?></option>
                   <?php if($this->session->userdata('role_id')==SUPERADMIN){?><option value="">Select Society</option>
                      <?php }?>
                      <?php foreach($society as $society){?>
                            <option value="<?= $society->id ?>"><?= $society->name; ?></option>
                      <?php } ?>
                </select>
              </div>
            </div>
            <?php } ?>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label col-sm-offset-1" for="house">Select House</label>
                <div class="col-sm-5">
                <select name="house_id" class="form-control" id="house_id">
                  <option value="<?= $user[0]->house_id ?>" id="myhouse"><?= $user[0]->building.' '.$user[0]->wing.' '.$user[0]->block;?></option>
                   <?php if($h_option){?>
                        <?php foreach ($h_option as $value) {?>
                          <option value="<?php echo $value->id ?>"><?php echo $value->building.' '.$value->wing.' '.$value->block ?></option>
                        <?php }}?>
                </select>
                <input type="hidden" name="old_house" value="<?= $user[0]->house_id ?>">
              </div>
            </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="email">Email address *</label>
                  <div class="col-sm-5">
                  <input type="email" class="form-control" name="email" id="email" value="<?= $user[0]->email; ?>" readonly>
                 </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Mobile *</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="mobile" id="mobile" value="<?= $user[0]->phone; ?>">
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="birth_date">Birth Date *</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="birth_date" id="birth_date" value="<?= $user[0]->birth_date; ?>">
                 </div>
              </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label col-sm-offset-1" for="total_family_member">Total Family Member *</label>
              <div class="col-sm-5">
              <input type="number" class="form-control" name="total_family_member" id="total_family_member" value="<?= $user[0]->total_family_member; ?>" style="width: 41%">
            </div>
          </div>
   <!--      <div class="form-group row">
          <label class="col-sm-3 col-form-label col-sm-offset-1" for="house">User Type</label>
          <div class="col-sm-5">
          <select name="user_type" class="form-control" id="house">
            <option value="0">Society Member</option>
            <option value="1">Admin</option>
          </select>
        </div> -->
        <div class="form-group row">
          <label class="col-sm-3 col-form-label col-sm-offset-1" for="profile_pic">Profile Picture</label>
          <div class="col-sm-5">
          <input type="file" name="profile_pic" id="profile_pic" value="<?= $user[0]->profile_pic; ?> ">
          <img src="<?= base_url().'upload/userImages/'.$user[0]->profile_pic;?>" width="150px;"/>
          <input type="hidden" name="user_img" value="<?= $user[0]->profile_pic; ?>" /><br>
          <p class="help-block">Your House Picture.</p>
        </div>
      </div>

    <div class="clearfix" style="height: 10px;clear: both;"></div>
                      <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="house_id">Member Authority</label>
                    <div class="col-sm-5">
                      <select name="authority" class="form-control" id="authority" name="authority">
                       
                          <?php if($authority){?>
                        <?php foreach($authority as $key => $value){?>
                        <option <?php if($user[0]->authority_id == $value->id) echo 'selected';?> value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                        <?php }?>
                       <?php }?>    
                      </select>
                    </div>
                </div>

        <div class="form-group row">
          <label class="col-sm-3 col-form-label col-sm-offset-1">Account Status</label>
          <div class="col-sm-5">

            <label>Active&nbsp;&nbsp;<input type="radio" id="active" name="status" value="1" checked></label>
            <label>In-Active&nbsp;&nbsp;<input type="radio" id="inactive" name="status" value="0" /></label>
         </div>
      </div>

  <!-- /.box-body -->

        <div class="box-footer" style="padding-left: 350px;">
          <button type="submit" class="btn btn-primary">Update User</button>
           <a href="<?php echo base_url()?>back/registration/alluser" type="submit" class="btn btn-danger">Back</a
        </div>

      </form>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    console.log("<?= $user[0]->status?>");
    $('#society_id').on('change',function(){
      var society_id=$('#society_id').val();
      var myh_id = "<?= $user[0]->house_id ?>";
      var myh_nm = "<?= $user[0]->wing.' '.$user[0]->block;?>";
      var myhouse = "<option value="+myh_id+">"+myh_nm+"</option>";
      $("#house_id").empty();
        $.ajax({
          url: '<?php echo base_url();?>back/registration/houseSociety',
          type:'POST',
          data:{society_id:society_id},
          success:function(data){
            $('#house_id').append(myhouse);
            $('#house_id').append(data);
          }
        });
    });
     if("<?= $user[0]->role_id?>"=='4'){
      $('#member').attr('checked','checked');
    } else {
      $('#admin').attr('checked','checked');
    }
    
    if("<?= $user[0]->status?>"=='Y'){
      $('#active').attr('checked','checked');
    } else {
      $('#inactive').attr('checked','checked');
    }
    $('#society_id').trigger('change');

   $('#birth_date').datepicker({
      dateFormat:'dd-mm-yy',
      maxDate: new Date(Date.now() - 864e5)
  });
  });
</script>
