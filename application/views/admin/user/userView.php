<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" method="post" action="<?= base_url();?>back/registration/adduser" id="user_form" enctype="multipart/form-data">
        <div class="col-md-12">
          <div class="box box-primary">
             <div class="box-header with-border">
                <h3 class="box-title">Add User &amp; Allocate House </h3>
             </div>
             <div class="box-body">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="first_name" pattern="[A-Za-z]">First Name *</label>
                      <div class="col-sm-5">  
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name">
                     </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Last Name *</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name">
                    </div>
               </div>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="username">Username *</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="username" id="user_name" placeholder="Enter Userame">
                        <p id="existing_user" style="color:red;"></p>
                   </div>
                </div>
                <div class="form-group row">
                <label class="col-sm-3 col-form-label col-sm-offset-1" for="password">Password *</label>
                    <div class="col-sm-5">
                      <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password">
                        <div id="pass_strong" style="color:green;"></div>
                        <div id="pass_weak" style="color:red;"></div>
                    </div>
              </div>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="user_role">User Role *</label>
                    <div class="col-sm-5">
                      <input type="radio"  name="user_role"  value="4" checked>Member
                      <input type="radio" name="user_role" value="3">
                      Admin
                    </div>
              </div>
              <?php $myrole = $this->session->userdata('role_id'); if($myrole==SUPERADMIN){?>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_id">Select Society *</label>
                  <div class="col-sm-5">
                    <select name="society_id" class="form-control" id="society_id">
                      <?php if($this->session->userdata('role_id')==SUPERADMIN){?><option value="">Select Society</option>
                      <?php }?>
                      <?php foreach($society as $society){?>
                            <option value="<?= $society->id ?>"><?= $society->name; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <?php } ?>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="house_id">Select House *</label>
                    <div class="col-sm-5">
                      <select name="house_id" class="form-control" id="house_id">
                        <option value="">Select House</option>
                        <?php if($h_option){?>
                        <?php foreach ($h_option as $value) {?>
                          <option value="<?php echo $value->id ?>"><?php echo $value->building.' '.$value->wing.' '.$value->block ?></option>
                        <?php }}?>
                      </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="email">Email Address *</label>
                      <div class="col-sm-5">
                      <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email Id">
                       <p id="email_error" style="color:red;"></p>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Mobile *</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Enter Mobile Number">
                    </div>
                </div>
                 <div class="form-group row">
                      <label class="col-sm-3 col-form-label col-sm-offset-1" for="birth_date">Birth Date *</label>
                        <div class="col-sm-5">
                        <input type="text" class="form-control" name="birth_date" id="birth_date" placeholder="Enter Birth Date" min="2000-03-20">
                      <p style="color:red" id="bday"></p>
                </div>
              </div>
               <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="total_family_member">No. Of Family Members *</label>
                    <div class="col-sm-5">
                     <input type="number" class="form-control" name="total_family_member" id="total_family_member" placeholder="Enter No. Of Family Members" style="width: 41%">
                  </div>
                </div>
               <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="profile_pic">Profile Picture</label>
                      <div class="col-sm-5">
                       <input type="file" name="profile_pic" id="profile_pic">
                        <p class="help-block">Your Profile Picture.(Only png,jpg,jpeg and gif allowed)</p>
                      </div>
                </div>
                 <!-- <hr> -->
<!--                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="house_id">Assign Parking Slot</label>
                    <div class="col-sm-5">
                      <select name="park_slot" class="form-control" id="park_slot" >
                       <option value="">Parking Slot</option>
                       <?php if($park_slot){?>
                        <?php foreach($park_slot as $key => $value){?>
                        <option value="<?php echo $value->id;?>"><?php echo $value->slot_no;?></option>
                        <?php }?>
                       <?php }?>                      
                      </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="house_id">Facility</label>
                    <div class="col-sm-5">
                      <select name="facility" class="form-control" id="facility">
                        <option value=""><span id="empty_option">Select Facility</span></option>   
                          <?php if($facility_name){?>
                        <?php foreach($facility_name as $key => $value){?>
                        <option value="<?php echo $value->id;?>"><?php echo $value->facility_name;?></option>
                        <?php }?>
                       <?php }?>                    
                      </select>
                    </div>
                </div> -->

                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group row" id="adult_count12">
                                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="house_id">Adult Count*</label>
                                        <div class="col-lg-5">
                                            <input type="text" class="form-control adult_child" name="adult_count" id="adult_count" placeholder="Please Enter Adult Count" value="">
                                        </div>
                                </div>
                      <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group row" id="child_count12">
                                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="house_id">Child Count*</label>
                                        <div class="col-lg-5">
                                            <input type="text" class="form-control adult_child" name="child_count" id="child_count" placeholder="Please Enter Child Count" value="">
                                        </div>
                                </div>

                      <div class="clearfix" style="height: 10px;clear: both;"></div>
                      <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="house_id">Member Authority</label>
                    <div class="col-sm-5">
                      <select name="authority" class="form-control" id="authority" name="authority">
                        <option value="">Select Authority</option>   
                          <?php if($authority){?>
                        <?php foreach($authority as $key => $value){?>
                        <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                        <?php }?>
                       <?php }?>                    
                      </select>
                    </div>
                </div>
                  <div class="clearfix" style="height: 10px;clear: both;"></div>
                       <div class="form-group">
                                <label class="col-lg-5 control-label" for="commission from"></label>
                                <div class="col-lg-5">
                                      <button type="submit" class="btn btn-primary">Add User</button>&nbsp;&nbsp;
                                    <a href="javascript:window.history.go(-1);" class="btn red btn-danger"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                                </div>
                            </div>
            <!--   <div class="box-footer" style="padding-left: 350px;">

                  <button type="submit" class="btn btn-primary">Add User</button> -->
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    var role = "<?php $this->session->userdata('role_id'); ?>";
    if(role=='1'){
    $('#society_id').on('blur',function(){
      var society_id=$('#society_id').val();
      $("#house_id").html("<option value=''>Select House</option>");

        $.ajax({
          url: '<?php echo base_url();?>back/registration/houseSociety',
          type:'POST',
          data:{society_id:society_id},
          success:function(data){
            console.log(data);
            $('#house_id').append(data);
          }
        }); 
    });
    $('#society_id').trigger('change');
}
    // End
       $(function () {
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 18);

    $('#birth_date').datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: start,
        maxDate: end,
        yearRange: start.getFullYear() + ':' + end.getFullYear()
    });
   });

   $("#birth_date").keydown(function(e) { 
        if(e.keyCode == 8)
        {
          return true;
        }else{
          return false;
        }
    });
   
      // $.validator.addMethod("mobileno_exists", function (value, element) {
      //       var flag;

      //       $.ajax({
      //           url: base_url + 'back/registration/is_mobile_no_registered',
      //           data: {mobile: value},
      //           async: false,
      //           type: 'POST',
      //           success: function (r) {
      //               flag = (r === 'true') ? false : true;
      //           }
      //       });
      //       return flag;
      //   }, 'Mobile no. already exsist with us');
    /******** mobile validation start here ********/
    /******** email validation start here ********/
     // $.validator.addMethod("email_exists", function (value, element) {
     //        var flag;
     //        $.ajax({
     //            url: base_url + 'back/registration/is_email_registered',
     //            data: {email: value},
     //            async: false,
     //            type: 'POST',
     //            success: function (r) {
     //                flag = (r === 'true') ? false : true;
     //            }
     //        });

     //        return flag;
     //    }, 'Email id already exsist with us');

      $.validator.addMethod("username_exists", function (value, element) {
            var flag;
            $.ajax({
                url: base_url + 'back/registration/is_username_registered',
                data: {username: value},
                async: false,
                type: 'POST',
                success: function (r) {
                    flag = (r === 'true') ? false : true;
                }
            });

            return flag;
        }, 'Username already exsist with us');
  });
</script>
<script type="text/javascript">
          $("#adult_count12").hide();
              $("#child_count12").hide();

     $(document).off('change','#facility').on('change','#facility', function() {
      if ( this.value >= 0)
      {
          $("#adult_count12").show();
          $("#child_count12").show();
      }
      else
      {
          $("#adult_count12").hide();
          $("#child_count12").hide();
      }
    });
</script>



