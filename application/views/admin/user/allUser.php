<section class="content">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
           <h3 style="">View Users Details</h3>
        </div>
         <div class="col-md-6">
         	<?php $ses_id = $this->session->userdata('role_id');if( $ses_id== SOCIETY_SUPERUSER || $ses_id==SOCIETY_ADMIN) {?>
         	 <a href="<?php echo base_url();?>back/registration/userView" class="btn btn-primary add_btn">&nbsp;Add User</i></a>&nbsp;&nbsp;
           <a href="<?php echo base_url();?>back/facility/memberfacility" class="btn btn-primary add_btn">Allocate Facility</i></a>
           &nbsp;&nbsp;
           <a href="<?php echo base_url();?>back/parking/index" class="btn btn-primary add_btn">Allocate Parking</i></a>&nbsp;&nbsp;
            <?php }?>
          <a href="javascript:window.history.go(-1);" class="btn btn-danger"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
        </div>
    </div>
  </div>
    <div class="row">
   		<div class="col-md-12 table-responsive">
 			<!-- <h3 style="padding-left: 10px;">View Users Details</h3> -->
 			<a href="<?php echo base_url();?>back/registration/alluser" class="btn btn-primary add_btn pull-right"><i class="fa fa-arrow-left">&nbsp;Back</i></a>
 			  <div class="box box-primary" id="box-primary">
 			  	<?php $ses_id = $this->session->userdata('id'); if($ses_id==SOCIETY_SUPERUSER){?>
 			  	<a href="<?php echo base_url()?>back/registration/userView" class="btn btn-primary pull-right">Add User</a><br><br>
 			  	<?php } ?>
 			  	<div class="table-responsive">
				<table class="table table-responsive" id="alluser">
					<thead>
						<tr>
							<th>Sr. No</th>
							<th>Username</th>
							<th>Full Name</th>
							<th>Email</th>
							<th>Role</th>
							<th>Society</th>
							<th>House</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<?php $count ='0'; foreach($user as $user){
						$id = $user->uid;
						$count++;?>
						<tr>
							<td><?= $count; ?></td>
							<td><?= $user->username;?></td>
							<td><?= $user->name;?></td>
							<td><?= $user->email;?></td>
						    <td><?= $user->role_name; ?></td> 
							<td><?= $user->society_name; ?></td>
							<td><?= $user->building.' '.$user->wing.' '.$user->block; ?></td>
							<td>
								<?php if($user->status == 'Y'){?>
								Active
								<?php }else{?>
									Inactive
							    <?php }?>		
                            </td>
							<?php $role_id = $this->session->userdata('role_id'); if($role_id == SUPERADMIN || $role_id == SOCIETY_SUPERUSER){?>
							<td><a href="<?= base_url().'back/registration/edituser/'.$id; ?>" class="btn btn-sm btn-warning" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> | <a href="<?= base_url().'back/registration/showuser/'.$id; ?>" class="btn btn-sm btn-info" id="update" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a> | <a href="<?= base_url().'back/registration/deleteuser/'.$id; ?>" class="btn btn-sm btn-danger" id="delete" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a></td>
							<?php } else {?>
							<td><a href="<?= base_url().'back/registration/showuser/'.$id; ?>" class="btn btn-sm btn-info" id="show" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a></td>
							<?php }?>
						</tr>
						<?php } ?>
					</tbody>
				</table>
 			</div>
	 	 </div>
	</div>
</div>
 </section>
 <script type="text/javascript">
 	$(document).ready(function(){
 		$('#alluser').DataTable({
 			columnDefs: [
            { width: 100, targets: 8 }
        ],
        fixedColumns: true
 		});
 		$('#delete').click(function() {
 		var choice = confirm('Are You Sure, To Delete this User');
 		if(choice!=true){
 			return false;
 			}
 		});
 	});
 </script>