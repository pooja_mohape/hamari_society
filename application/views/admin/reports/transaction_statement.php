<?php 
if($this->session->userdata('role_id') == SOCIETY_MEMBER)
{
 $style = "display: none";
}
else
{
 $style = "display: block;";
}
?>
<section class="content">
    <div id="mail_template_wrapper">
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="row">
              <h3 class="box-title" style="margin-left:25px;">Transaction Passbook</h3>
            </div>
            <div class="box-header">
                 <div class="row">
                           <div class="col-md-2">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="text" value="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Till Date</label>
                                    <input class="form-control " id="till_date" name="till_date" placeholder="Till Date" type="text" value=""/>
                                </div>
                            </div>
                          <?php if(empty($transactions_by_id)){?>
                           <div class="col-md-2" style="<?php echo $style?>">   
                                <div style="position: static;" class="form-group">
                                    <label for="select-1">User List</label>
                                    <select class="form-control chosen" id="user_id" name="user_id">
                                    <option value="">All</option>
                                   <?php if($userdata){?>
                                    <?php foreach($userdata as $key => $value){?>
                                      <option value="<?php echo $value->id ?>"><?php echo $value->first_name ." ".$value->last_name?></option>
                                    <?php }?>
                                   <?php }?>  
                                    </select>
                                </div>
                            </div>
                            <?php }?>
                            <div class="col-md-2">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2" style="visibility: hidden;">test</label>
                                        <button type="submit" class="form-control btn btn-primary" id="generate_btn">Submit</button>
                                    </div>  
                            </div>
                            
                    </div>
                                                                           
               
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="mail_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">#</th>
                            <th>Id</th>
                            <th>Transaction Date</th>
                            <th>Full Name</th>
                            <th>Transaction Type</th>
                            <th>Outstanding Amount Before</th>
                            <th>Transaction Amount </th>
                            <th>Current Outstanding Amount</th>
                            <th>Wallet Amount Before</th>
                            <th>Wallet Amount After</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
</section>
<script type="text/javascript">
</script>
<script>
/*  $( function() {
       $( "#from_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
       $( "#till_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );*/
  $(document).ready(function(){
    $("#from_date").datepicker({
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#till_date").datepicker("option","minDate", selected)
        }
    });
    $("#till_date").datepicker({ 
        numberOfMonths: 1,
        onSelect: function(selected) {
           $("#from_date").datepicker("option","maxDate", selected)
        }
    });  
    $("#from_date").keydown(function(e) { 
        if(e.keyCode == 8)
        {
          return true;
        }else{
          return false;
        }
    });
    $("#till_date").keydown(function(e) { 
        if(e.keyCode == 8)
        {
          return true;
        }else{
          return false;
        }
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "back/bill_payment_report/get_transaction_statement",
            "type": "POST",
            "data": "json",
            data   : function (d) {
                        d.from_date    =  $("#from_date").val();
                        d.to_date    =  $("#till_date").val();
                        d.user_id    =  $("#user_id").val();
                        d.transaction_by_id = "<?php echo $transactions_by_id; ?>";
                     }
        },
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        //        "paginate": true,
        "paging": true, 
        "searching": true,
        "order": [[1, "desc"]],
        "aoColumnDefs": [
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false

                    }
                ],

         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();  
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);  
            if (aData[8] == 'Approved' || aData[8] == 'Rejected')
            {
                $("td:eq(8)", nRow).html('<a href='+base_url+'back/bill_payment/bill_payment_view/'+aData[1]+' class="views_wallet_trans margin"><i class="glyphicon glyphicon-eye-open"></i></a>');
            }       
            return nRow;
        },
    };

    var oTable = $('#mail_display_table').dataTable(tconfig); 
    $(document).off('click', '#generate_btn').on('click', '#generate_btn', function (e) { 
            from_date    =  $("#from_date").val();
            to_date    =  $("#till_date").val();
            
            if(from_date === '' && to_date)
            {
                alert("please provide from date");
            }
            else if(from_date && to_date === '')
            {
                alert("please provide Till date");
            }
            else{
                oTable.fnDraw(); 
            }    
  }); 
});
</script>



