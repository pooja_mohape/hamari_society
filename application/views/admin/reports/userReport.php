<section class="content">
    <div class="row">
 		<div class="col-md-12 col-sm-12 col-xs-12">
 				<h1></h1>
 		<div class="box box-primary" id="box-primary">
		    <div class="box-header with-border">
    		  <h3 class="box-title">Member Details</h3>
   		 	</div>
	        <div class="box-body">
	        	<div class="col-sm-4 col-md-3">
	         		<div class="form-group">
		              <label class="col-md-12 control-label" for="filter_by">Select Designation For Details</label>
		                <select name="filter_by" class="form-control" id="filter_by">
		                  <option value="">Select Designation</option>
<?php foreach ($role as $role) {?>
				                  <option value="<?=$role->id;?>"><?=$role->role_name;
	?></option>
	<?php }?>
</select>
		        	</div>
		        </div>
		        <div class="col-sm-4 col-md-3">
		        	<div class="form-group">
		              <label class="col-md-12 control-label" for="filter_by">Select Society</label>
		                <select name="filter_list" class="form-control" id="filter_list">
		                  <option value="">Select Society</option>
<?php if ($society) {
	foreach ($society as $society) {?>
						                 	<option value="<?=$society->id?>"><?=$society->name;?></option>
		<?php }}?>
		                </select>
		        	</div>
		        </div>
		        <div class="col-sm-4 col-md-3">
		        	<div class="form-group">
		              	<button type="submit" class="btn btn-info" id="report_id" style="margin-top: 24px;">Go</button>
		            </div>
		        </div>
	       		 <br/><br/>
	        </div>
	        <div class="row">
	        	<div class="col-md-12 col-xs-12 col-sm-12">
	        		<div class="table-responsive">
 					<table id="user_report1" class="table table-responsive table-condensed table-hover">
 						<thead>
 							<tr>
 								<!-- <th>Sr No</th> -->
	 							<th>Name</th>
	 							<th>Username</th>
	 							<th>Society</th>
	 							<th>House</th>
	 							<th>Email</th>
	 							<th>Phone</th>
	 							<th>Birth Date</th>
	 							<th>Total Member</th>
	 							<th>A/C Status</th>
	 							<th>Created Date</th>
 							</tr>
 						</thead>
		 					<tbody id="tbd">

		 					</tbody>
 					</table>
 				</div>
 				</div>
 			</div>
 		</div>
 	</div>
</div>
 </section>
 <script type="text/javascript">
 	$(document).ready(function(){

 		$('#report_id').on('click',function(){
 			var filter_by = $('#filter_by').val();
 			var filter_list = $('#filter_list').val();
 			if(filter_list==''){
 			$('tbody').empty();
 			$.ajax({
 				url:'<?=base_url();?>back/report/userReport',
 				type:'POST',
 				data:{filter_by:filter_by},
 				success:function(res) {
 					$('tbody').append(res);
 				}
 			});
 			}else
 			{
 				$('tbody').empty();
 				$.ajax({
 				url:'<?=base_url();?>back/report/userReportByBoth',
 				type:'POST',
 				data:{filter_by:filter_by,filter_list:filter_list},
 				success:function(res) {
 					console.log(res);
 					$('tbody').append(res);
 				}
 			});
 			}
 		});
 		// $('#user_report1').DataTable({"bSearchable":true});
 	});
 </script>