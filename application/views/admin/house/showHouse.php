<section class="content">
      <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
               <h3 style="">View House </h3>
            </div>
             <div class="col-md-6">
               <a href="<?php echo base_url();?>back/registration/allHouse" class="btn btn-danger add_btn pull-right"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
      </div>
      <div class="row">
         <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                  <div class="box-body">
                   <img src="<?= base_url().'upload/houseImages/'.$house[0]->house_picture; ?>" class="img-responsive center-block" width="300px;" height="300px;"/>
                   <p class="text-center" style="padding:5px;"><strong><?= ucfirst($house[0]->society_name).' house';?></strong></p>
                </div>
              </div>
          </div>
         <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">House Details</h3>
            </div>
              <div class="box-body">
                <table class="table table-responsive table-condensed table-hover table-bordered">
                  <!-- <caption>Personal Details</caption> -->
                  <thead></thead>
                  <tbody>
                    <tr>
                        <td>Socitey Name</td>
                        <td><?= $house[0]->society_name; ?></td>
                    </tr>
                    <tr>
                          <td>Building</td>
                          <td><?= $house[0]->building; ?></td>
                    </tr>
                     <tr>
                          <td>Wing</td>
                          <td><?= $house[0]->wing; ?></td>
                    </tr>
                     <tr>
                          <td>Block / Flat No.</td>
                          <td><?= $house[0]->block; ?></td>
                      </tr>
                      <tr>
                          <td>House Type</td>
                          <td><?= $house[0]->house_type; ?></td>
                      </tr>
                     <tr>
                        <td>Details</td>
                        <td><?= $house[0]->details; ?></td>
                    </tr>
                     <tr>
                        <td>Created Date</td>
                        <td><?= $house[0]->created_date; ?></td>
                    </tr>
                     <tr>
                       <!--  <td>Created_by</td>
                        <td><?= $house[0]->created_by; ?></td> -->
                    </tr>
                    <tr>
                        <td>Updated Date</td>
                        <td><?= $house[0]->updated_date; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
  </div>
</section>
<script type="text/javascript">

</script>