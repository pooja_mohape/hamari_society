<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Edit Netbanking
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">  
                          <form id="payment_request" name="payment_request" method="post" action="<?php echo base_url();?>back/netbanking/update/">
                                          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $editnetbanking[0]->id?>">
                             <div class="form-group">
                                    <label class="col-lg-2 control-label">Account Name</label>
                                        <div class="col-lg-4"> 
                                          <input type="text" class="form-control" name="acc_name" id="acc_name" value="<?php echo $editnetbanking[0]->acc_name?>">
                                        </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;">    
                            </div>
                            <div class="form-group">
                                    <label class="col-lg-2 control-label">Account Number</label>
                                        <div class="col-lg-4"> 
                                          <input type="text" class="form-control" name="acc_no" id="acc_no" value="<?php echo $editnetbanking[0]->acc_no?>">
                                        </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;">  
                            </div>
                            <div class="form-group">
                                    <label class="col-lg-2 control-label">Bank Name</label>
                                        <div class="col-lg-4"> 
                                          <input type="text" class="form-control" name="bank_name" id="bank_name" value="<?php echo $editnetbanking[0]->bank_name?>">
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                    <label class="col-lg-2 control-label">IFSC Code</label>
                                        <div class="col-lg-4"> 
                                          <input type="text" class="form-control" name="ifsc_code" id="ifsc_code" value="<?php echo $editnetbanking[0]->ifsc_code?>">
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                    <label class="col-lg-2 control-label">Branch Name</label>
                                        <div class="col-lg-4"> 
                                          <input type="text" class="form-control" name="branch" id="branch" value="<?php echo $editnetbanking[0]->branch?>">
                                        </div>
                                </div>
                              <div class="clearfix" style="height: 10px;clear: both;"></div>
                              <div class="col-lg-offset-3">
                                <button type="submit" id="submit" class="btn btn-primary pull-left">Save</button>&nbsp;&nbsp;
                                 <a class="btn btn-primary" href="<?php echo  base_url().'back/netbanking'?>" type="button">Back</a> 
                              </div>                          
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<script>
  $( function() {
       $( "#transaction_date" ).datepicker({ dateFormat: 'dd-mm-yy'});
  } );
</script>