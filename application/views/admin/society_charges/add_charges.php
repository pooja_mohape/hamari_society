<section class="content">
      <div class="row">
        <!-- left column -->
      <form method="post" action="<?=base_url()?>back/societycharges/add_charges" enctype="multipart/form-data" id="society_form">
         <div class="col-sm-12">
          <!-- general form elements -->
          <div class="box box-primary">

            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-header with-border">
            </div>

              <div class="box-body">
                 <div class="form-group row">
                      <label class="col-sm-2 col-form-label col-sm-offset-1" for="username"></label>
                      <div class="col-sm-5">
                      <h4><p><b>Add House Type , Parking &amp; Facility Details</b></p>
                      </h4>
                    </div>
                 </div>
                 <div class="form-group row">
                      <label class="col-sm-2 col-form-label col-sm-offset-1" for="username">Select Type</label>
                      <div class="col-sm-5">
                      <div class="form-check form-check-inline">
                          <input class="form-check-input" type="checkbox" id="parkcheckbox1" value="house_type" name="house_type_check">
                          <label class="form-check-label" for="inlineCheckbox1">House Type
                           <input class="form-check-input" type="checkbox" id="parkcheckbox2" value="parking" name="parking_check">
                          <label class="form-check-label" for="inlineCheckbox2">Parking</label>
                           <input class="form-check-input" type="checkbox" id="parkcheckbox3" value="facility" name="facility_check">
                          <label class="form-check-label" for="inlineCheckbox2">Facility</label>

                        </div>
                    </div>
                 </div>
                <div class="form-group row" id="house_div" style="display: none;">
                      <label class="col-sm-2 col-form-label col-sm-offset-1" for="total_house">House Type *</label>
                      <div class="col-sm-8">
                      <div class="table-responsive" id="cheque_detail">
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr class="bg-light-blue-active">
                                    <th>House Type</th>
                                    <th>Maintenance Amount</th>
                                    <th>Lease Amount</th>
                                    <th></th>
                                </tr>
                            </thead>
                       <tbody>
                                <tr data-row_no="1">
                                    <td>
                                         <input type="text" name="house_type[]" id="house_type" class="sd" placeholder="House Type" style="width: 8em" required>
                                    </td>
                                    <td>
                                        <input type="text" name="main_amt[]" class="main_amt sd" placeholder="Maintenance Amt" style="width:8em" min="0" required>
                                    </td>
                                    <td>
                                        <input type="text" name="lease_amt[]" class="lease_amt sd" placeholder="Lease Amt" style="width: 8em" min="0" required>
                                    </td>
                                    <td>
                                        <input type="button" name="add" value="Add " class="add_button" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                     </div>
                    </div>
                 </div>
                 <div class="form-group row" id="parking_div" style="display: none;">
                      <label class="col-sm-2 col-form-label col-sm-offset-1" for="total_house">Parking Details *</label>
                      <div class="col-sm-8">
                      <div class="table-responsive" id="cheque_detail">
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr class="bg-light-blue-active">
                                    <th>Vehicle Type</th>
                                    <th>Parking Charges Per Vehicle</th></th>
                                </tr>
                            </thead>
                       <tbody>
                                <tr data-row_no="1">
                                    <td>
                                         <input type="text" name="parking_type[]" id="parking_type" class="sd" placeholder="Vehicle Type" style="width: 8em" required>
                                    </td>
                                    <td>
                                        <input type="text" name="park_amt[]" class="park_amt sd" placeholder="Parking Amt" style="width:8em" min="0" required>
                                    </td>
                                    <td>
                                        <input type="button" name="add" value="Add " class="add_parking" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                     </div>
                    </div>
                 </div>

                <div class="form-group row" id="facility_div" style="display: none;">
                      <label class="col-sm-2 col-form-label col-sm-offset-1" for="total_house">Facility *</label>
                      <div class="col-sm-8">
                      <div class="table-responsive" id="cheque_detail">
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr class="bg-light-blue-active">
                                    <th>Facility Name</th>
                                    <th>Adult Charges</th>
                                    <th>Child Charges</th>
                                    <th></th>
                                </tr>
                            </thead>
                       <tbody>
                                <tr data-row_no="1">
                                    <td>
                                         <input type="text" name="facility_name[]" id="facility_name" class="sd" placeholder="facility name" style="width: 8em" required>
                                    </td>
                                    <td>
                                        <input type="text" name="adult_charges[]" class="adult_charges sd" placeholder="adult_charges" style="width:8em" min="0">
                                    </td>
                                    <td>
                                        <input type="text" name="child_charges[]" class="child_charges sd" placeholder="child Charges" style="width: 8em" min="0">
                                    </td>
                                    <td>
                                        <input type="button" name="add" value="Add " class="add_facility" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                     </div>
                    </div>
                 </div>
                 <div class="form-group row">
                      <label class="col-sm-3 col-form-label col-sm-offset-1" for="total_house"></label>
                      <div class="col-sm-3">
                         <button type="submit" class="btn btn-primary add_house">Add House /Parking /Facility</button>&nbsp;
&nbsp;
                          <!-- <button type="button" class="btn btn-danger">BAck</button> -->
                    </div>
                 </div>
                  <div class="row">
<?php if (isset($house_type)) {?>
	<div class="col-sm-1">

	                    </div>
	                            <div class="col-sm-10 table-responsive table-bordered">
	                      <h4><p><b>House Details</b></p></h4>
	                                <table class="table table-bordered table-hover table-responsive" id="wallet_table">
	                                    <thead>
	                                        <tr>
	                                            <th>Sr.no</th>
	                                            <th>House Type</th>
	                                            <th>Maintenance Amount</th>
	                                            <th>Lease Amount</th>
	                                            <th>Action</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	<?php
	$c = 1;
	if (!empty($house_type)) {
		foreach ($house_type as $key => $value) {
			$id = $value['id'];
			?>

			                                        <tr>
			                                            <td><?php echo $c;?>.</td>
			                                            <td><?php echo $value['house_type'];?></td>
			                                            <td><?php echo $value['maintenance_charges'];?></td>
			                                            <td><?php echo $value['lease_amt'];?></td>
			                                             <td><a href="<?=base_url().'back/societycharges/edit_house_charge/'.$id;?>" class="btn btn-sm btn-warning" id="edit">Edit / View</a> | <a href= "<?=base_url().'back/societycharges/delete_charge/'.$id;?>" class="btn btn-sm btn-danger" id="delete" onclick="return confirm('Are you sure want to delete this house permanently?')">Delete</a></td>
			                                        </tr>
			<?php $c++;}?>
		<?php }?>
	</table>
	                                </tbody>
	                                  <div class="col-md-12">
	                                     <div class="pull-right">

	                                      </ul>
	                                     </div>
	                                 </div>
	                                 <div>
	                            </div>
	                        </div>
	                        </div>
	<?php }?>
                        <?php if (isset($parking_charges)) {?>
	<div class="row">
	                             <div class="col-sm-1">

	                             </div>
	                            <div class="col-sm-10 table-responsive table-bordered">
	                                <h4><p><b>Parking Details</b></p></h4>
	                                <table class="table table-bordered table-hover table-responsive" id="wallet_table">
	                                    <thead>
	                                        <tr>
	                                            <th>Sr.no</th>
	                                            <th>Vehicle Type</th>
	                                            <th>Parking Amount</th>
	                                            <th>Action</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	<?php
	$c = 1;
	if (!empty($parking_charges)) {
		foreach ($parking_charges as $key => $value) {
			$id = $value['id'];
			?>

			                                        <tr>
			                                            <td><?php echo $c;?>.</td>
			                                            <td><?php echo $value['vehicle_type'];?></td>
			                                            <td><?php echo $value['parking_amt'];?></td>
			                                            <td><a href="<?=base_url().'back/societycharges/edit_park_charges/'.$id;?>" class="btn btn-sm btn-warning" id="edit">Edit/View</a> | <a href= "<?=base_url().'back/societycharges/delete_park/'.$id;?>" class="btn btn-sm btn-danger" id="delete" onclick="return confirm('Are you sure want to delete this parking type?')">Delete</a></td>

			                                        </tr>
			<?php $c++;}?>
		<?php }?>
	</table>
	                                </tbody>
	                                  <div class="col-md-12">
	                                     <div class="pull-right">

	                                      </ul>
	                                     </div>
	                                 </div>
	                                 <div>
	                            </div>
	                        </div>
	                        </div>
	<?php }?>
                        <?php if (isset($facilities)) {?>
	<div class="row">
	                             <div class="col-sm-1">

	                             </div>
	                            <div class="col-sm-10 table-responsive table-bordered">
	                                <h4><p><b>Facility Details</b></p></h4>
	                                <table class="table table-bordered table-hover table-responsive" id="wallet_table">
	                                    <thead>
	                                        <tr>
	                                            <th>Sr.no</th>
	                                            <th>Facility Name</th>
	                                            <th>Adult Charges</th>
	                                            <th>Child Charges</th>
	                                            <th>Action</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	<?php $count = '0';foreach ($facilities as $user) {
		$id         = $user->id;
		$count++;?>
		                                        <tr>
		                                          <td><?=$count;?></td>
		                                          <td><?=$user->facility_name;?></td>
		                                          <td><?=$user->adult_charges;?></td>
		                                          <td><?=$user->child_charges;?></td>
		                                          <!-- <td><?=$user->maintenance_cost;?></td> -->
		                                          <td><a href="<?=base_url().'back/Facility/update_facility/'.$id;?>" class="btn btn-warning" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> | <a href="<?=base_url().'back/Facility/view_facility/'.$id;?>" class="btn btn-info" id="update" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a> | <a href="<?=base_url().'back/Facility/delete_facility/'.$id;?>" onclick="return confirm('Are You Sure ?')" class="btn btn-danger" id="delete" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a></td>
		                                        </tr>
		<?php }?>
	</table>
	                                </tbody>
	                                  <div class="col-md-12">
	                                     <div class="pull-right">

	                                      </ul>
	                                     </div>
	                                 </div>
	                                 <div>
	                            </div>
	                        </div>
	                        </div>
	<?php }?>
</div>
            </div>
          </div>
      </form>
  </div>
</section>
<script>
 $(document).ready(function() {
    var dateToday = new Date();
      $('.date').datepicker({
                dateFormat: 'dd-mm-yy',
                'minDate': dateToday,
            });
  });

 var row_number_master = {};

 $(document).on('click','.add_button',function() {

    var parent_table_tbody = $(this).closest('table tbody');
    var row = $(this).closest('tr').data('row_no');
    var row_number = row + 1;

    var new_html = '<tr data-row_no="'+ row_number +'"></td><td><input type="text" name="house_type[]" id="house_type" class="sd" placeholder="House Type" style="width: 8em" required=""></td><td><input type="text" name="main_amt[]" class="main_amt sd" placeholder="Maintenance Amt" style="width: 8em" min="0" required=""></td><td><input type="text" name="lease_amt[]" class="lease_amt sd" placeholder="Lease Amount" style="width: 8em" min="0" required=""></td><td><input type="button" name="add" value="Add " class="add_button"> <input type="button" name="remove" value="Remove" class="remove_button" /></td></tr>';

      parent_table_tbody.append(new_html);

});

//To delete dynamic row
$(document).on('click','.remove_button',function() {
    if($(this).closest('table tbody').find('tr').length == 2) {
        $(this).closest('table').children('tbody').children('tr:first').find('td:last').html('<input type="button" name="add" value="Add" class="add_button">');
    }
    $(this).closest('tr').remove();
});

</script>

<script>
 var row_number_master = {};

 $(document).on('click','.add_parking',function() {

    var dateToday = new Date();

    var parent_table_tbody = $(this).closest('table tbody');
    var row = $(this).closest('tr').data('row_no');
    var row_number = row + 1;

    var new_html = '<tr data-row_no="'+ row_number +'"></td><td><input type="text" name="parking_type[]" id="parking_type" class="sd" placeholder="vehicle_type" style="width: 8em" required=""></td><td><input type="text" name="park_amt[]" class="park_amt sd" placeholder="park_amt" style="width: 8em" min="0" required=""></td><td><input type="button" name="add" value="Add " class="add_parking"> <input type="button" name="remove" value="Remove" class="remove_button" /></td></tr>';

   parent_table_tbody.append(new_html);

});

//To delete dynamic row
$(document).on('click','.remove_button',function() {
    if($(this).closest('table tbody').find('tr').length == 2) {
        $(this).closest('table').children('tbody').children('tr:first').find('td:last').html('<input type="button" name="add" value="Add" class="add_parking">');
    }
    $(this).closest('tr').remove();
});

</script>

<script>
 var row_number_master = {};

 $(document).on('click','.add_facility',function() {

    var dateToday = new Date();

    var parent_table_tbody = $(this).closest('table tbody');
    var row = $(this).closest('tr').data('row_no');
    var row_number = row + 1;

     var new_html = '<tr data-row_no="'+ row_number +'"></td><td><input type="text" name="facility_name[]" id="facility_name" class="sd" placeholder="facility_name" style="width: 8em" required=""></td><td><input type="text" name="adult_charges[]" class="adult_charges sd" placeholder="adult_charges" style="width: 8em" min="0"></td><td><input type="text" name="child_charges[]" class="child_charges sd" placeholder="child_charges" style="width: 8em" min="0"></td><td><input type="button" name="add" value="Add " class="add_facility"> <input type="button" name="remove" value="Remove" class="remove_button" /></td></tr>';

   parent_table_tbody.append(new_html);

});

//To delete dynamic row
$(document).on('click','.remove_button',function() {
    if($(this).closest('table tbody').find('tr').length == 2) {
        $(this).closest('table').children('tbody').children('tr:first').find('td:last').html('<input type="button" name="add" value="Add" class="add_parking">');
    }
    $(this).closest('tr').remove();
});

</script>

<script type="text/javascript">
$(function () {
        $("#parkcheckbox1").click(function () {
            if ($(this).is(":checked")) {
                 $("#house_div").show();
                 $(".add_house").attr('disabled',false);
            } else {
                 $("#house_div").hide();
                 $(".add_house").attr('disabled',true);
            }
        });

        $("#parkcheckbox2").click(function () {
            if ($(this).is(":checked")) {
                 $("#parking_div").show();
                 $(".add_house").attr('disabled',false);
            } else {
                $("#parking_div").hide();
                $(".add_house").attr('disabled',true);
            }
        });

         $("#parkcheckbox3").click(function () {
            if ($(this).is(":checked")) {
                 $("#facility_div").show();
                 $(".add_house").attr('disabled',false);
            } else {
                $("#facility_div").hide();
                $(".add_house").attr('disabled',true);
            }
        });

    });
</script>

<script type="text/javascript">
$(".add_house").attr('disabled',true);
</script>
