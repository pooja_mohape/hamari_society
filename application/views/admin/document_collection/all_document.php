<section class="content">
     <div class="row">
       
            <div class="col-md-12">
                <h1></h1>
              <div class="box box-primary" id="box-primary">
                <h4>All Documents</h4>
               <?php $role_id = $this->session->userdata('role_id'); if($role_id == SUPERADMIN || $role_id == SOCIETY_SUPERUSER){?>
                     <a type="submit" href="<?php echo base_url()?>back/document_collection/" class="btn btn-primary pull-right" style="background-color:#f0ad4e;">Add Document</a>
                          <?php }?>   <br/>

                <table id="allhouse" class="table table-condensed table-hover table-responsive">
                    <!--  <div id="allhouse_filter" class="dataTables_filter"><label><b>Search By User:</b>
                        <?php $usersdata=$this->db->get_where('users',array('society_id'=>$this->session->userdata('society_id')))->result_array();?>
                        <select type="text" class="form-control input-sm" placeholder="" aria-controls="allhouse"  style="width: 267px;" id="search_byuser">
                            <option value='true'>Select user</option>
                            <?php foreach($usersdata as $users){?>
                        <option value="<?php echo $users['id'];?>"><?php echo $users['first_name']." ".$users['last_name'];?></option>
                        <?php }?>
                    </select>
                        </label></div> -->
                   
                    <thead>
                      <tr>
                            <th>Sr. No</th>
                            <th>Received From</th>
                            <th>User</th>
                            <th>Handover to</th>
                            <th>Document Name</th>
                            <th>Remark</th>
                            <th>Date</th>
                            <?php $role_id = $this->session->userdata('role_id'); 
                            if($role_id == SUPERADMIN || $role_id == SOCIETY_SUPERUSER){?>
                             <th>User Action</th>
                             <?php }?>
                            <th>Action</th>
                        </tr>
                    </thead>
                           <tbody id="document_body">
                         <?php $count ='0'; foreach($documents as $document){
                            $id = $document->id;
                            $count++;?>
                        <tr>
                            <td id="increment"><?= $count; ?></td>
                            <td><?= $document->coming_from;?></td>
                            <?php $username=$this->db->get_where('users',array('id'=>$document->address_to))->result_array();?>
                            <td><?= $username[0]['first_name']." ".$username[0]['last_name'];?></td>
                            <td><?= $document->handover_to;?></td>
                             <td><?= $document->document_name;?></td>
                             <td><?= $document->remark;?></td>
                            <td><?= $document->date;?></td>

                            <?php $role_id = $this->session->userdata('role_id'); if($role_id == SUPERADMIN || $role_id == SOCIETY_SUPERUSER){?>
                            <td><?php if($document->document_view=='Y'){?>
                               <p class="custom" id="Update">Received</p></td>
                          <?php }else{?>
                            <p class="custom" id="Update">Not Received</p>
                          <?php }?>
                               <td>
                                <?php if($document->document_view=='Y'){ ?>
                                <!-- <a href="#" class="btn btn-sm btn-warning" id="edit1">Edit</a> | -->
                                <?php }else{?>
                                <a href="<?= base_url().'back/document_collection/editDocument/'.$id; ?>" class="btn btn-sm btn-warning" id="edit">Edit</a> |
                                <?php }?>
                                 <a href="<?= base_url().'back/document_collection/viewDocument/'.$id; ?>" class="btn btn-sm btn-info" id="Update">View</a> | <a href= "<?= base_url().'back/document_collection/deleteDocument/'.$id; ?>" class="btn btn-sm btn-danger" id="delete" onclick="return confirm('Are you sure?')">Delete</a></td>
                           <?php }else {?>
                            <td><a href="<?= base_url().'back/document_collection/viewDocument/'.$id; ?>" class="btn btn-sm btn-info" id="Update">View</a>| 
                                <?php $res=$this->db->get_where('document_collection',array('id'=>$id))->result_array();
                                if($res[0]['document_view']=='Y'){?>
                                <p  class="custom" id="Update">Received</p></td>
                                <?php }else{?>
                                 <!-- <a onclick="return confirm('Do you received your document??')" href="<?= base_url().'back/document_collection/readDocument/'.$id; ?>" class="btn btn-sm btn-danger" id="Update">Not Receive</a> -->
                                 <p class="custom" id="update">Not Received</p>
                               </td>
                                    <?php }?>
                           <?php }?>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
               </div>
            </div>
     </div>
</section>
<style type="text/css">
    #box-primary{
        padding:20px;
    }
    .custom {
    width: 90px !important;
}
.custom:hover{
  cursor: none;
}
#edit1:hover{
cursor: none;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $('#search_byuser').click(function(){
            var username=$("#search_byuser").val();
            if(username == 'true')
            {
                location.reload();
            }
            $.ajax({
                type:'post',
                data:{id:username},
                url:'<?php echo base_url()?>back/document_collection/search_byUsername',
                success:function(res)
                {
                     var obj=JSON.parse(res);
                      $("#allhouse").empty(); 
                       $('#allhouse').append('<tr><th> Sr. No </th><th> Coming From </th><th>Address to</th><th>Handover To</th><th>Date</th>');
                     for(var i=0;i<obj.length;i++){
                        var tr="<tr>";
                        var td1="<td>"+obj[i]["id"]+"</td>";
                        var td2="<td>"+obj[i]["coming_from"]+"</td>";
                        var td3="<td>"+obj[i]["first_name"]+" "+obj[i]["last_name"]+"</td>";
                        var td4="<td>"+obj[i]["handover_to"]+"</td>";
                        var td5="<td>"+obj[i]["date"]+"</td>";
                        $("#allhouse").append(tr+td1+td2+td3+td4+td5); 
                     }
                }
            });
        });  
      $('#allhouse').DataTable();
    });
</script>