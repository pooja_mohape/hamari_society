<section class="content">
     <div class="row">
        <?php $role_id = $this->session->userdata('role_id'); if($role_id == SUPERADMIN || $role_id == SOCIETY_SUPERUSER){?>
          <a type="submit" href="<?php echo base_url()?>back/document_collection/" class="btn btn-primary" style="background-color:#f0ad4e;margin-left:900px">Add Document</a>
          <?php }?>
            <div class="col-md-12">
                <h1></h1>
              <div class="box box-primary" id="box-primary">
                <table id="allhouse" class="table table-condensed table-hover table-responsive">
                    
                    <thead>
                      <tr>
                            <th>Sr. No</th>
                            <th>Coming from</th>
                            <th>Address to</th>
                            <th>Handover to</th>
                            <th>Document Name</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="document_body">
                         <?php $count ='0'; foreach($documents as $document){
                            $id = $document->id;
                            $count++;?>
                        <tr>
                            <td id="increment"><?= $count; ?></td>
                            <td><?= $document->coming_from;?></td>
                            <?php $username=$this->db->get_where('users',array('id'=>$document->address_to))->result_array();?>
                            <td><?= $username[0]['first_name']." ".$username[0]['last_name'];?></td>
                            <td><?= $document->handover_to;?></td>
                             <td><?= $document->document_name;?></td>
                            <td><?= $document->date;?></td>
                            <?php $role_id = $this->session->userdata('role_id'); if($role_id == SUPERADMIN || $role_id == SOCIETY_SUPERUSER){?>
                               <td><a href="<?= base_url().'back/document_collection/editDocument/'.$id; ?>" class="btn btn-sm btn-warning" id="edit">Edit</a> | <a href="<?= base_url().'back/document_collection/viewDocument/'.$id; ?>" class="btn btn-sm btn-info" id="Update">View</a> | <a href= "<?= base_url().'back/document_collection/deleteDocument/'.$id; ?>" class="btn btn-sm btn-danger" id="delete" onclick="return confirm('Are you sure?')">Delete</a></td>
                           <?php }else {?>
                            <td><a href="<?= base_url().'back/document_collection/viewDocument/'.$id; ?>" class="btn btn-sm btn-info" id="Update">View</a>| 
                                <?php $res=$this->db->get_where('document_collection',array('id'=>$id))->result_array();
                                if($res[0]['document_view']=='Y'){?>
                                <a onclick="confirm('You read your document details ??')" href="<?= base_url().'back/document_collection/readDocument/'.$id; ?>" class="btn btn-sm btn-info" id="Update">Read</a></td>
                                <?php }else{?>
                                 <a onclick="confirm('You read your document details ??')" href="<?= base_url().'back/document_collection/readDocument/'.$id; ?>" class="btn btn-sm btn-info" id="Update">Unread</a></td>
                                    <?php }?>
                           <?php }?>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
               </div>
            </div>
     </div>
</section>
<style type="text/css">
    #box-primary{
        padding:20px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $('#allhouse').DataTable();
         $('#delete').click(function() {
        var choice = confirm('Are You Sure, To Delete this House');
        if(choice!=true){
            return false;
        }
    });
    });
</script>
