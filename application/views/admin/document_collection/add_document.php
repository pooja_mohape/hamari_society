<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" id="document_collection">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Documents Collection </h3>
                <a type="submit" href="<?php echo base_url()?>back/document_collection/allDocument" class="btn btn-primary pull-right" style="background-color:#f0ad4e">All Documents</a>
            </div>
               
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="first_name">Received From *</label>
                    <div class="col-sm-5">
                      <input type="text" maxlength="25" class="form-control" name="coming_from" id="coming_from" placeholder="Received From" required>
                       <div class="error_comingfrom" style="color:red"> </div>
                       <div class="errors_ajax" style="color:red"> </div>
                   </div>
                </div>
               
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name"> Address To *</label>
                    <div class="col-sm-5">
                 <?php $usersdata=$this->db->get_where('users',array('society_id'=>$this->session->userdata('society_id'),'role_id !='=> SOCIETY_SUPERUSER))->result_array();
                 ?>
                    <select type="text" class="form-control chosen" placeholder="" aria-controls="allhouse" id="address_to" name="address_to">
                      <option value=' '>Select User</option>
                      <?php foreach($usersdata as $users){?>
                    <option value="<?php echo $users['id'];?>"><?php echo $users['first_name']." ".$users['last_name'];?></option>
                    <?php }?>
                  </select>
                   <div class="error_addressto" style="color:red"> </div>
                    </div>
               </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="email">Document Name</label>
                    <div class="col-sm-5">
                    <input class="form-control" id="document_name" maxlength="30" name="document_name" placeholder="Enter Document Name" type="text" value="" maxlength="30">
                     <div class="error_document_name" style="color:red"> </div>
                  </div>
                </div>
                 
              
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="email">Date *</label>
                    <div class="col-sm-5">
                    <input class="form-control" id="from_date" name="date" placeholder="dd-mm-yyyy" type="text" value="" readonly>
                     <div class="error_date" style="color:red"> </div>
                  </div>
                </div>
                 
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Handover To *</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="handover_to" id="handover_to" placeholder="Handover To" required maxlength="30">
                     <div class="error_handoverto" style="color:red"> </div>
                     <div class="errors_handover" style="color:red"> </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="remark">Remark *</label>
                  <div class="col-sm-5">
                    <textarea id="remark" name="remark" rows="8" placeholder="Enter Remark" style="width: 242px;height: 85px;"></textarea>
                     <div class="error_remark" style="color:red"> </div>
                  </div>
                </div>
                
                 <div class="box-footer" style="padding-left: 350px;">
                      <input type="button" class="btn btn-primary add" value="Add Document">
               </div>
             
            </div>
          </div>
        </div>
            </form>
          </div>
        </div>
      </div>
</section>
<style>
#document_collection input,#address_to
{
      max-width: 61% !important;
}
</style>
<script>
  $( function() {
       $( "#from_date" ).datepicker({ dateFormat: 'dd-mm-yy', maxDate:'0' });
  } );

     $(".add").click(function(){
      var alphanumers = /^[a-zA-Z0-9]+$/;
      var handover = /^[a-zA-Z]+$/;
   
    if($('#coming_from').val()==''){

      $(".error_comingfrom").html('Please enter the source name');

    }
    else if($('#address_to').val()==' '){
      $(".error_addressto").html('Please select user');

    }else if($('#from_date').val()==''){
       $(".error_date").html('Please select the date');
    }else if($('#handover_to').val()=='')
    {
       $(".error_handoverto").html('Handover to is required');
    }else if(!handover.test($("#handover_to").val())){
      $(".errors_handover").html('Please Enter valid name');
    }else if($('#remark').val()==''){
      $(".error_remark").html('Remark is required');
    }
    else{
      var details= $("#document_collection").serialize();
     $.ajax({
      type:'post',
      data:details,
      url:'<?php echo base_url()?>back/document_collection/addDocument',
      success:function(res){
        window.location.href='<?php echo base_url()?>back/document_collection/allDocument';
      }
    })
    }
  })
</script>