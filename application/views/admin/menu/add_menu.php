<script>
    $(document).ready(function () {
        /************** for confirm message***********/
        $('.confirm-div').hide();
<?php if ($this->session->flashdata('invalid')) { ?>
            $('.confirm-div').html('<?php echo $this->session->flashdata('invalid'); ?>').show();
<?php } else if ($this->session->flashdata('invalid')) { ?>
            $('.confirm-div').html('<?php echo $this->session->flashdata('valid'); ?>').show();
<?php } ?>
        /*********************************************/
    });
    
$("#add_menu_form_validate").validate({
            rules: {
                 menu_parent: {
                    required: true     
                },
                menu_name: {
                    required: true
                },
                menu_link: {
                    required: true,
                }
            }
        });
</script> 
<div class="confirm-div"></div>
<h1>Add Menu</h1>
<?php
$attributes = array("method" => "POST", "id" => "add_menu_form_validate", "class" => "cmxform form");
echo form_open('admin/menu/add_menu_action', $attributes);
?>
<p class="field">
    <label>Parent</label>
    <select name="parent" id="menu_parent">
        <option value = ''>Select Parent</option>
        <option value = '0'>Root</option>
        <?php
        foreach ($menu_array as $value) {
             echo "<option value = '".$value['id']."'>".ucwords($value['display_name'])."</option>";
        }
        ?>
    </select>
</p>
<p class="field">
    <label>Name</label>
    <input type="text" placeholder="Name" id="menu_name" name="menu_name" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter menu name" required/>
    <i class="fa fa-user"></i>
</p>
<p class="field">
    <label>Link</label>
    <input type="text" placeholder="Link" id="menu_link" name="menu_link" data-rule-required="true" data-msg-required="Please enter menu link" required/>
    <i class="fa fa-envelope"></i>
</p> 
<p class="field">
    <label>Class</label>
    <input type="text" placeholder="Class" id="menu_class" name="menu_class"/>
    <i class="fa fa-user"></i>
</p>

<p class="submit"><input type="submit" value="Add Menu"></p>
<?php echo form_close(); ?>