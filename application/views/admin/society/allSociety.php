<section class="content">
	 <div class="row">
    <div class="col-md-12">
        <div class="col-md-8">
           <h3 style="">View Society Details</h3>
        </div>
         <div class="col-md-4">
         	<?php $ses_id = $this->session->userdata('role_id');if( $ses_id== SUPERADMIN) {?>
         	<a href="<?php echo base_url()?>back/registration/societyView" class="btn btn-primary">Add Society</a>
            <?php }?>
              <a href="javascript:window.history.go(-1);" class="btn red btn-danger"><i class="fa fa-arrow-left">&nbsp;Back</i></a>
        </div>
    </div>
  </div>
      <div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12">
 			  <div class="box box-primary" id="box-primary">
 			  	<div class="box-body table-responsive">
 				<table class="table table-hover table-responsive" id="allsociety">
 					<thead>
 						<tr>
 							<th>Sr. No</th>
 							<th>Society Name</th>
 							<th>State</th>
 							<th>City</th>
 							<th>Total House</th>
 							<th>Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php $count ='0'; foreach($society as $society){
 							$id = $society->id;
 							$count++;?>
 						<tr>
 							<td><?= $count; ?></td>
 							<td><?= $society->name;?></td>
 							<td><?= $society->state;?></td>
 							<td><?= $society->city;?></td>
 							<td><?= $society->no_of_house;?></td>
 							<?php $role_id = $this->session->userdata('role_id'); if($role_id==SUPERADMIN || $role_id == SOCIETY_SUPERUSER){?>
 							<td><a href="<?= base_url().'back/registration/editsociety/'.$id; ?>" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit" id="edit"><i class="fa fa-pencil"></i></a> | <a href="<?= base_url().'back/registration/showsociety/'.$id; ?>" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="View" id="show"><i class="fa fa-eye"></i></a> | <a href="<?= base_url().'back/registration/deletesociety/'.$id; ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" id="delete"><i class="fa fa-trash"></i></a></td>
 							<?php } else {?>
 								<td><a href="<?= base_url().'back/registration/showsociety/'.$id; ?>" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="View" id="show"><i class="fa fa-eye"></i></a></td>
 							<?php } ?>
 						</tr>
 						<?php } ?>
 					</tbody>
 				</table>
 			</div>
 			   </div>
 			</div>
 			
      </div>
 </section>
 <script type="text/javascript">
 	$(document).ready(function() {
 		$('#delete').click(function() {
 		var choice = confirm('Are You Sure, To Delete this Society');
 		if(choice!=true){
 			return false;
 		}
 	});
 		$('#allsociety').DataTable();

 	});
 </script>