<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" method="post" action="<?= base_url();?>back/meeting/addmeeting" id="meeting_form">
         <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Meeting</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Select Society<span>*</span></label>
                  <div class="col-sm-5">
                    <select name="society_id" class="form-control" id="society_id">
                      <option value="">Select Society</option>
                      option
                      <?php  foreach($society as $society) {?>
                      <option value="<?= $society->id; ?>" selected=""><?= $society->name; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Select Authority<span>*</span></label>
                  <div class="col-sm-5">
                    <select name="authority[]" class="form-control" id="authority" multiple="" required="">
                      <?php  foreach($auth as $value) {?>
                      <option value="<?= $value->id; ?>"><?= $value->name; ?></option>
                      <?php } ?>
                      <option value="0">other members</option>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="ondate">Meeting Date<span>*</span></label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="ondate" id="ondate" placeholder="Choose Meeting Date">
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="stime">Meeting Satrt Time<span>*</span></label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="stime" id="stime" placeholder="Choose Meeting Start Time">
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="etime">Meeting End Time<span>*</span></label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="etime" id="etime" placeholder="Choose Meeting End Time">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="subject">Meeting Title<span>*</span></label>
                  <div class="col-sm-5">
                    <input type="text" name="subject" class="form-control" placeholder="Enter Subject Of Meeting"/>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="description">Location Of Meeting<span>*</span></label>
                  <div class="col-sm-5">
                    <input type="text" name="location" class="form-control" placeholder="Enter Location Of Meeting"/>
                  </div>
               </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="location">Description Of Meeting<span>*</span></label>
                  <div class="col-sm-5">
                  <textarea type="text" name="description" placeholder="Description of the meeting" id="description" rows="3" cols="56"></textarea>
               </div>
             </div>
              <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="location"></label>
                  <div class="col-sm-5">
                  <input id="email" type="checkbox"  name="email">&nbsp;<b>Email</b>
                  <input id="sms" type="checkbox"  name="sms">&nbsp;<b>Sms</b>
                  <input id="both" type="checkbox" name="both">&nbsp;<b>Both</b>
               </div>
             </div>
               <div class="box-footer" style="padding-left: 350px;">
                      <button type="submit" class="btn btn-primary">Create Meeting</button>
               </div>
              </div>
            </div>
          </div>
      </form>
  </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#ondate').datepicker({
    dateFormat:'dd-mm-yy',
    minDate: new Date()
  });
    $('#stime').ptTimeSelect();
    $('#etime').ptTimeSelect();
    $('#authority').multiselect({
            includeSelectAllOption: true,
            buttonWidth: 250,
            enableFiltering: true
        });
  $('#both').on('change', function() {
    $('#email').prop('checked', this.checked);
    $('#sms').prop('checked', this.checked);
  });

  $('#sms').on('change', function() {
      $('#sms').prop('checked', this.checked);
       if($("#email").prop("checked") == true){
            $('#both').prop('checked', this.checked);
        }
  });
      $('#email').on('change', function() {
           $('#email').prop('checked', this.checked);
          if($("#sms").prop("checked") == true){
                $('#both').prop('checked', this.checked);
            }
  });
  // $('.email').on('change', function() {
  //   $('#email').prop('checked', $('.email:checked').length===$('.email').length);
  // });
  // $('.sms').on('change', function() {
  //   $('#both').prop('checked', $('.sms:checked').length===$('.sms').length);
  // });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#meeting').click(function(){
      var stime = $('#stime').val();
      var etime = $('#etime').val();

      if(stime == etime && (stime!='' && etime!='')){
        $('#error').html('Start time should not be same as end time');
      }
    });
  });
</script>

