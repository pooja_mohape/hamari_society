<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" method="post" action="<?= base_url();?>back/meeting/updatemeeting" id="meeting_form">
         <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update Meeting</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Select Society</label>
                  <div class="col-sm-5">
                    <select name="society_id" class="form-control" id="society_id">
                      <option value="<?= $mt[0]->society_id; ?>"><?= $mt[0]->name; ?></option>
                      <?php  foreach($society as $society) {?>
                      <option value="<?= $society->id; ?>"><?= $society->name; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="ondate">Meeting Date</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="ondate" id="ondate" value="<?= $mt[0]->meeting_date ?>">
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="stime">Meeting Satrt Time</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="stime" id="stime" value="<?= $mt[0]->start_time ?>">
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="etime">Meeting End Time</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="etime" id="etime" value="<?= $mt[0]->end_time ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="subject">Meeting Title</label>
                  <div class="col-sm-5">
                    <input type="text" name="subject" class="form-control" value="<?= $mt[0]->subject ?>" />
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="location">Meeting Location</label>
                  <div class="col-sm-5">
                    <input type="text" name="location" class="form-control" value="<?= $mt[0]->meeting_location ?>" />
                  </div>
               </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="description">Description Of Meeting</label>
                  <div class="col-sm-5">
                  <textarea type="text" name="description" id="description" rows="3" cols="56" value="<?= $mt[0]->description ?>"><?= $mt[0]->description ?></textarea>
               </div>
             </div>
               <div class="box-footer" style="padding-left: 350px;">
                      <input type="hidden" name="Mid" value="<?= $mt[0]->id?>" />                      
                      <button type="submit" class="btn btn-primary">Update Meeting</button>
               </div>
              </div>
            </div>
          </div>
      </form>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('#ondate').datepicker({
    dateFormat:'dd-mm-yy',
    minDate: new Date()
  });
    $('#stime').ptTimeSelect();
    $('#etime').ptTimeSelect();
});
</script>