<section class="content">
      <div class="row">
         <div class="col-md-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= ucwords($meeting[0]->name); ?> Society Meeting Details</h3>
            </div>
              <div class="box-body">
                <table class="table table-responsive table-condensed table-hover table-bordered">
                  <!-- <caption>Personal Details</caption> -->
                  <thead></thead>
                  <tbody>
                    <tr>
                        <td>Socitey Name</td>
                        <td><?= ucwords($meeting[0]->name); ?></td>
                    </tr>
                     <tr>
                          <td>Meeting Date</td>
                          <td><?= date('d-m-Y',strtotime($meeting[0]->meeting_date)); ?></td>
                      </tr>
                      <tr>
                          <td>Start Time</td>
                          <td><?= $meeting[0]->start_time; ?></td>
                      </tr>
                     <tr>
                        <td>End Time</td>
                        <td><?= $meeting[0]->end_time; ?></td>
                    </tr>
                     <tr>
                        <td>Subject</td>
                        <td><?= ucwords($meeting[0]->subject); ?></td>
                    </tr>
                    <tr>
                        <td>Meeting Location</td>
                        <td><?= $meeting[0]->meeting_location; ?></td>
                    </tr>
                    <tr>
                       <td>Meeting Description</td>
                        <td><?= $meeting[0]->description; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>


  <div class="col-md-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Meeting Request To</h3>
            </div>
              <div class="box-body">
                <table class="table table-responsive table-condensed table-hover table-bordered" height="100px" overflow="auto">
                  <!-- <caption>Personal Details</caption> -->
                  <thead>
                    <tr>
                      <div class="col-md-6">
                       </td class="pull-left"><b>Name</b></td>
                  </div>
                 
                  <div class="col-md-6">
                      </td class="pull-left"><b> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Authority</b></td>
                  </div>
                     <!--  </td class="pull-left"><b>Name</b></td>
                    </td class="pull-left"><b>Authority</b></td>
                       -->
                    </tr>

                  </thead>
                  <tbody>
                    <?php foreach ($userdata as $key => $value) {?>
                    <tr>
                        <td><?php echo $value->first_name . "_" . $value->last_name;?></td>   
                        <td><?php echo ''.isset($value->name) ? $value->name:'Society Member'?></td>  
                        
                    </tr>
                   <?php  } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="clearfix" style="height: 10px;clear: both;"></div>
                       <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from"></label>
                                <div class="col-lg-6">
                                     <!--  <button type="submit" href="<?php echo  base_url().'back/parking'?> id="btn btn-danger" class="btn btn-primary pull-left">Back</button> -->
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                      <a class="btn btn-danger" href="<?php echo  base_url().'back/meeting/allmeeting'?>" type="button">Back</a> 
                                </div>
                            </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
      var time = $('#timepicker').timepicker('showWidget');
  });
</script>