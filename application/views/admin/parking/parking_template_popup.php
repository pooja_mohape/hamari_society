<div class="modal fade popup" id="add_edit_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php
            $attributes = array("method" => "POST", "id" => "mail_template_form", "name" => "mail_template_form");
            echo form_open(base_url().'back/parking/add_parking', $attributes);
            ?>
            <div class="modal-body">
                 <div class="form-group">
                    <label for="title">Users:</label>
                     <select class="form-control" name="members" required="members">
                      <option value="">Select User</option>
                       <?php if($userdata){?>
                        <?php foreach($userdata as $key => $value){?>
                        <option value="<?php echo $value->id ?>"><?php echo $value->first_name." ".$value->last_name." (".$value->building." ".$value->wing." ".$value->block.")"?></option>
                        <?php }?>
                       <?php }?>
                    </select>
                </div>
                 <div class="form-group">
                    <label for="title">Parking Slot:</label>
                     <select class="form-control" name="slot_no" required="slot_no">
                      <option value="">Parking Slot</option>
                       <?php if($park_slot){?>
                        <?php foreach($park_slot as $key => $value){?>
                        <option value="<?php echo $value->id;?>"><?php echo $value->slot_no.'-'.$value->vehicle_type;?></option>
                        <?php }?>
                       <?php }?>
                    </select>
                </div>
               
            </div>
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Cancel</button>

                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i>Save</button>
            </div>
            <?php echo form_close();?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(function() {
        
        CKEDITOR.replace('mail_body_class',{
            enterMode : CKEDITOR.ENTER_BR,
            entities : false,
            basicEntities : false,
            entities_greek : false,
            entities_latin : false, 
            htmlDecodeOutput : false
        });
        
    });
</script>
