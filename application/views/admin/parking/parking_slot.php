<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Add Parking Slot
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">
                         
                          <form id="payment_request" name="payment_request" method="post" action="<?php echo base_url();?>back/parking/insert_slot/">
                            <div class="form-group">
                           <label class="col-lg-2 control-label">Slot Number </label>
                           <div class="col-lg-4">
                               <input type="text" class="form-control" name="slot_name" id="slot_name" value="<?php ?>" placeholder="Enter Slot Number">
                          </div>
                     </div>
                  <div class="clearfix" style="height: 10px;clear: both;"></div>
                  <div class="form-group">
                           <label class="col-lg-2 control-label">Vehicle Type </label>
                            <div class="col-lg-4">
                            <select class="form-control" name="vehicle_type" required="vehicle_type">
                              <option value="">Vehicle Type</option>
                               <?php if($parking_details){?>
                                <?php foreach($parking_details as $key => $value){?>
                                <option value="<?php echo $value['id'];?>"><?php echo $value['vehicle_type'];?></option>
                                <?php }?>
                               <?php }?>
                          </select>
                        </div>
                        <div class="col-lg-1">
                           <a class="btn btn-info" href="<?php echo  base_url().'back/Societycharges'?>" type="button" style="border-radius: 50%"><i class="glyphicon glyphicon-plus" title="Add Vehicle Type"></i></a> 
                        </div>
                     </div>
                  <div class="clearfix" style="height: 10px;clear: both;"></div>
                      
 
                      <div class="clearfix" style="height: 10px;clear: both;"></div>
                  
                       <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from"></label>
                                <div class="col-lg-6">
                                      <button type="submit" id="submit_btn" class="btn btn-primary pull-left">Save</button>
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                      <a class="btn btn-primary" href="<?php echo  base_url().'back/parking'?>" type="button">Back</a> 
                                </div>
                            </div>

                                
                          </form>
                          <br><br>
                           <div class="box-body">
                 <div class="row">
                      <div class="col-sm-10 table-responsive table-bordered">
                          <h4><p><b><center>Slot Details</center></b></p></h4>
                          
                          <table class="table table-bordered table-hover table-responsive table-strip" id="wallet_table">
                              <thead>
                                  <tr>
                                      <th>Sr.No</th>
                                      <th>Slot No</th>
                                      <th>vehicle type</th>
                                      <th>parking amt</th>
                                      <th>Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php 

                                      $i=0;
                                      if(!empty($data)){
                                      foreach ($data as $key => $value) 
                                      {
                                          $i++;
                                          $vehicle_type = $value->vehicle_type;
                                           ?>
                                      
                                      
                                  <tr>
                                      <td><?php echo $i;?></td>
                                      <td><?php echo($value->slot_no ? $value->slot_no : '-'); ?>.</td>
                                      <td><?php echo($value->vehicle_type ? $value->vehicle_type : '-'); ?></td>
                                      <td><?php echo($value->parking_amt ? $value->parking_amt : '-');?></td>
                                      <td><a href="<?= base_url().'back/parking/delete_parkslot/'.$value->id; ?>" class="btn btn-sm btn-danger" id="delete" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-trash"></i></a></td>
                                     
                                  </tr>
                                  <?php } ?>  
                                  <?php }?>
                          </table>
                          </tbody>
                            
                           
                        </div> 
                        </div> 
                  </div>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<!-- <script>
  $( function() {
       $( "#transaction_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
</script> -->
<script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="scripts/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="scripts/jquery-validation/additional-methods.min.js"></script>
<script>
  $(document).ready(function(){
    $.validator.addMethod("loginRegex", function(value, element) {
       return this.optional(element) || /^[a-z0-9\\ _-]+$/i.test(value);
   }, "Slot no must contain only letters, numbers");

    $("#payment_request").validate({
      rules:{
          slot_name:{
            required:true,
            // alphanumeric:true
            loginRegex:true
          },
          vehicle_type:{
            required:true
          }
      },
      messages:{
          slot_name:{
            alphanumeric:"Please Enter only Digits and alphabet"
          },
          vehicle_type:{
            required:"Please select vehicle type"
          }
      }
    });
       $('#wallet_table').DataTable();
       $('#delete').click(function(){
       var choice = confirm('Are You Sure, To Delete this slot');
      if(choice!=true){
      return false;
      }
       })
  });
</script>
<!-- <section class="content">
      <div class="row">
         <div class="col-sm-12">
          <div class="box box-primary">
           
              <div class="box-body" >
                 <div class="row">
                      <div class="col-sm-10 table-responsive table-bordered">
                          <h4><p><b><center>Slot Details</center></b></p></h4>
                          
                          <table class="table table-bordered table-hover table-responsive table-strip" id="wallet_table">
                              <thead>
                                  <tr>
                                      <th>Sr.No</th>
                                      <th>Slot No</th>
                                      <th>vehicle type</th>
                                      <th>parking amt</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php 
                                      $i=0;
                                      if(!empty($data)){
                                      foreach ($data as $key => $value) 
                                      {
                                          $i++;
                                          $vehicle_type = $value->vehicle_type;
                                           ?>
                                      
                                      
                                  <tr>
                                      <td><?php echo $i;?></td>
                                      <td><?php echo($value->slot_no ? $value->slot_no : '-'); ?>.</td>
                                      <td><?php echo($value->vehicle_type ? $value->vehicle_type : '-'); ?></td>
                                      <td><?php echo($value->parking_amt ? $value->parking_amt : '-');?></td>
                                     
                                  </tr>
                                  <?php } ?>  
                                  <?php }?>
                          </table>
                          </tbody>
                            
                           
                        </div> 
                        </div> 
                  </div>
            </div>
          </div>
      </form>
  </div>
</section> -->
