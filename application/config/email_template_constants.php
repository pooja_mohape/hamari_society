<?php

define('SMS_BALANCE_ALERT','<p>Dear <b>{{username}}</b>,</p>
							  <p>Your Bill payment Request of Rs {{amount}} is approved.</p>
                              
                              <p>Regards,</p>
							  <p>Team SMA</p>');

define('SMS_BALANCE_REJECT','<p>Dear <b>{{username}}</b>,</p>
							  <p>Your Bill payment Request of Rs {{amount}} is Rejected.</p>
                              
                              <p>Regards,</p>
							  <p>Team SMA</p>');
							  	
define('MAIL_USER','<p>Dear <b>{{username}}</b>,</p>
							  <p>{{username}} You Are Added In Society {{society_id}}.</p>
                              
                              <p>Regards,</p>
							  <p>Team SMA</p>');	

define('BILL_PAYMENT_REQUEST','<!DOCTYPE html>
<html>
<head>
    <title></title>

<style type="text/css">
    @charset "utf-8";
/* CSS Document */

/* Client-specific Styles */
div, p, a, li, td {
    -webkit-text-size-adjust:none;
}
#outlook a {
    padding:0;
} /* Force Outlook to provide a "view in browser" menu link. */
html {
    width: 100%;
}
body {
    width:100% !important;
    -webkit-text-size-adjust:100%;
    -ms-text-size-adjust:100%;
    margin:0;
    padding:0;
}
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
.ExternalClass {
    width:100%;
} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
} /* Force Hotmail to display normal line spacing. */
#backgroundTable {
    margin:0;
    padding:0;
    width:100% !important;
    line-height: 100% !important;
}
img {
    outline:none;
    text-decoration:none;
    border:none;
    -ms-interpolation-mode: bicubic;
}
a img {
    border:none;
}
.image_fix {
    display:block;
}
p {
    margin: 0px 0px !important;
}
table td {
    border-collapse: collapse;
}
table {
    border-collapse:collapse;
    mso-table-lspace:0pt;
    mso-table-rspace:0pt;
}
a {
    color: #33b9ff;
    text-decoration: none;
    text-decoration:none!important;
}
/*STYLES*/
table[class=full] {
    width: 100%;
    clear: both;
}
/*IPAD STYLES*/
@media only screen and (max-width: 640px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 440px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 420px!important;
text-align:center!important;
}
 img[class=banner] {
width: 440px!important;
height:220px!important;
}
 img[class=col2img] {
width: 440px!important;
height:220px!important;
}
}
/*IPHONE STYLES*/
@media only screen and (max-width: 480px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 280px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 260px!important;
text-align:center!important;
}
 img[class=banner] {
width: 280px!important;
height:140px!important;
}
 img[class=col2img] {
width: 280px!important;
height:140px!important;
}
}
/*invoice_css */
.invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;

    }
    .title{
        color:#ef912e;
    }
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
</style>    
</head>
<body>
    <!-- Start of preheader -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of preheader -->
<!-- Start of header -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <tr>
                    <td>
                        <!-- logo -->
                        
                        <!-- end of logo -->
                        <!-- start of menu -->
                        <table bgcolor="#f49a2f" width="250" height="50" border="0" align="right" valign="middle" cellpadding="0" cellspacing="0" border="0" class="devicewidth">
                        <tbody>
                        <tr>
                                <!-- <td height="50" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828" st-content="menu">
                                    <a href="#" style="color: #ffffff;text-decoration: none;">Home</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Shop</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Contact</a>
                                    &nbsp;&nbsp;&nbsp;
                                </td> -->
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of menu -->
                    </td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Header -->
<!-- Start of seperator -->
<!-- End of seperator -->
<!-- Start of main-banner -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                <tbody>
                <tr>
                    <!-- start of image -->
                    <td align="center" st-image="banner-image" style="background-color: #060709;">
                        <div class="imgpop">
                            <a target="_blank" href="#"><img width="300" border="0" height="200" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png" class="banner"></a>
                        </div>
                    </td>
                </tr>
                 <!-- end of image -->
                </tbody>
                </table>
               
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of main-banner -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        </table>
    </td>
</tr>
</tbody>
</table>


<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>


<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
            <div>
        <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                   
                </td>
            </tr>

            <tr class="heading">
                <td style="text-align:center;">
                     Bill Payment Request 
                </td>
                <td>
                </td>
            </tr>
            
            <tr class="item">
                <td>
                   <span class=title>Payment By</span><br>
                    {{topup_by}}
                </td>
                
                <td>
                <span class=title>user Email</span><br>
                    {{useremail}}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                     <span class="title">Amount</span><br>
                     {{amount}}
                </td>
                
                <td>
                <span class="title">Name</span><br>
                   {{username}}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    <span class="title">Bank Name</span><br>
                    {{bank_name}} 
                </td>
                
                <td>
                    <span class="title">Transaction No.</span><br>
                    {{transaction_no}}
                </td>
            </tr>

             <tr class="item">
                <td>
                    <span class="title">Bank Account No.</span><br>
                    {{bank_acc_no}} 
                </td>
                
                <td>
                    <span class="title">Transaction Remark</span><br>
                        {{transaction_remark}}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                <span class="title">Transaction Date</span><br>
                {{transaction_date}}
                
                <td>
                  <span class="title">Approval</span><br>
                  Approved
                </td>
            </tr>
            <tr class="">
            <td></td><tr>

        </table>
    </div>          
            </div>
        </div>
  <!-- Spacing -->
        <tr>
            <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                &nbsp;
            </td>
        </tr>


<!-- Spacing -->
</tbody>
</table>
                 

           
<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>
<tr>
    <td height="9" align="center" style="font-size:1px; line-height:1px;">
        &nbsp;
    </td>
</tr>

<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
                <p>Regards,<br>{{society_name}}.</p>
            </div>
        </div>   
       </div>      
    </td>
</tr>

                      
</tbody>
</table>

<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        <tbody>
        <tr>
            <td align="center" height="30" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of seperator -->
<!-- Start of footer -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
<tbody>
<tr>
    <td>
        <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table bgcolor="#f49a2f" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td>
                        <!-- Social icons -->
                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                        <tbody>
                        <tr>
                            <td width="49" height="43" align="center" style="color: #ffffff;">
                             Copyright © 2018 Hamari Society.<br> All rights reserved.
                            </td>
                        
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of Social icons -->
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of footer -->
<!-- End of footer -->


</body>
</html>');

define('BILL_PAYMENT_REJECTED','<!DOCTYPE html>
<html>
<head>
    <title></title>

<style type="text/css">
    @charset "utf-8";
/* CSS Document */

/* Client-specific Styles */
div, p, a, li, td {
    -webkit-text-size-adjust:none;
}
#outlook a {
    padding:0;
} /* Force Outlook to provide a "view in browser" menu link. */
html {
    width: 100%;
}
body {
    width:100% !important;
    -webkit-text-size-adjust:100%;
    -ms-text-size-adjust:100%;
    margin:0;
    padding:0;
}
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
.ExternalClass {
    width:100%;
} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
} /* Force Hotmail to display normal line spacing. */
#backgroundTable {
    margin:0;
    padding:0;
    width:100% !important;
    line-height: 100% !important;
}
img {
    outline:none;
    text-decoration:none;
    border:none;
    -ms-interpolation-mode: bicubic;
}
a img {
    border:none;
}
.image_fix {
    display:block;
}
p {
    margin: 0px 0px !important;
}
table td {
    border-collapse: collapse;
}
table {
    border-collapse:collapse;
    mso-table-lspace:0pt;
    mso-table-rspace:0pt;
}
a {
    color: #33b9ff;
    text-decoration: none;
    text-decoration:none!important;
}
/*STYLES*/
table[class=full] {
    width: 100%;
    clear: both;
}
/*IPAD STYLES*/
@media only screen and (max-width: 640px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 440px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 420px!important;
text-align:center!important;
}
 img[class=banner] {
width: 440px!important;
height:220px!important;
}
 img[class=col2img] {
width: 440px!important;
height:220px!important;
}
}
/*IPHONE STYLES*/
@media only screen and (max-width: 480px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 280px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 260px!important;
text-align:center!important;
}
 img[class=banner] {
width: 280px!important;
height:140px!important;
}
 img[class=col2img] {
width: 280px!important;
height:140px!important;
}
}
/*invoice_css */
.invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;

    }
    .title{
        color:#ef912e;
    }
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
</style>    
</head>
<body>
    <!-- Start of preheader -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of preheader -->
<!-- Start of header -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <tr>
                    <td>
                        <!-- logo -->
                        
                        <!-- end of logo -->
                        <!-- start of menu -->
                        <table bgcolor="#f49a2f" width="250" height="50" border="0" align="right" valign="middle" cellpadding="0" cellspacing="0" border="0" class="devicewidth">
                        <tbody>
                        <tr>
                                <!-- <td height="50" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828" st-content="menu">
                                    <a href="#" style="color: #ffffff;text-decoration: none;">Home</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Shop</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Contact</a>
                                    &nbsp;&nbsp;&nbsp;
                                </td> -->
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of menu -->
                    </td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Header -->
<!-- Start of seperator -->
<!-- End of seperator -->
<!-- Start of main-banner -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                <tbody>
                <tr>
                    <!-- start of image -->
                    <td align="center" st-image="banner-image" style="background-color: #060709;">
                        <div class="imgpop">
                            <a target="_blank" href="#"><img width="300" border="0" height="200" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png" class="banner"></a>
                        </div>
                    </td>
                </tr>
                 <!-- end of image -->
                </tbody>
                </table>
               
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of main-banner -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        </table>
    </td>
</tr>
</tbody>
</table>


<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>


<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
            <div>
        <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                   
                </td>
            </tr>

            <tr class="heading">
                <td style="text-align:center;">
                     Bill Payment Request 
                </td>
                <td>
                </td>
            </tr>
            
            <tr class="item">
                <td>
                   <span class=title>Payment By</span><br>
                    {{topup_by}}
                </td>
                
                <td>
                <span class=title>user Email</span><br>
                    {{useremail}}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                     <span class="title">Amount</span><br>
                     {{amount}}
                </td>
                
                <td>
                <span class="title">Name</span><br>
                   {{username}}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    <span class="title">Bank Name</span><br>
                    {{bank_name}}
                </td>
                
                <td>
                    <span class="title">Transaction No.</span><br>
                    {{transaction_no}}
                </td>
            </tr>

             <tr class="item">
                <td>
                    <span class="title">Bank Account No.</span><br>
                    {{bank_acc_no}}
                </td>
                
                <td>
                    <span class="title">Transaction Remark</span><br>
                       {{transaction_remark}}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                <span class="title">Transaction Date</span><br>
                {{transaction_date}}
                
                <td>
                  <span class="title">Approval</span><br>
                  Rejected
                </td>
            </tr>
            <tr class="">
            <td></td><tr>
            
            <tr class="">
            <td></td><tr>

            <tr>
                <td>
                <span class="title">Rejected Reason</span><br>
                Mera Buzz Mujhe jine na de;
                </td>
            </tr>
        </table>
    </div>          
            </div>
        </div>
  <!-- Spacing -->
        <tr>
            <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                &nbsp;
            </td>
        </tr>


<!-- Spacing -->
</tbody>
</table>
                 

           
<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>
<tr>
    <td height="9" align="center" style="font-size:1px; line-height:1px;">
        &nbsp;
    </td>
</tr>

<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
                <p>Regards,<br>{{society_name}}.</p>
            </div>
        </div>   
       </div>      
    </td>
</tr>

                      
</tbody>
</table>

<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        <tbody>
        <tr>
            <td align="center" height="30" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of seperator -->
<!-- Start of footer -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
<tbody>
<tr>
    <td>
        <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table bgcolor="#f49a2f" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td>
                        <!-- Social icons -->
                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                        <tbody>
                        <tr>
                            <td width="49" height="43" align="center" style="color: #ffffff;">
                             Copyright © 2018 Hamari Society.<br> All rights reserved.
                            </td>
                        
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of Social icons -->
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of footer -->
<!-- End of footer -->


</body>
</html>');  	

define('MONTHLY_PAYMENT','<p>Dear <b>Admin</b>,</p>
                    <p>Please Find below payment details for the month of {{month}}</p>
                              
                                <table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                   <th>Name</th>
                                   <th>maintenance_amt</th>
                                   <th>parking_amt</th>
                                   <th>other_charges</th>
                                   <th>facility_charges</th>
                                   <th>total_amt</th>
                                   <th>net_payable_amt</th>
                                   <th>Current Outstanding Amount</th>
                                </tr>
                                <tr>
                                   <td><center>{{username}}</center></td>
                                   <td><center>{{maintenance_amt}}</center></td>
                                   <td><center>{{parking_amt}}</center></td>
                                   <td><center>{{other_charges}}</center></td>
                                   <td><center>{{facility_charges}}</center></td>
                                   <td><center>{{total_amt}}</center></td>
                                   <td><center>{{net_payable_amt}}</center></td>
                                   <td><center>{{current_outstanding_amount}}</center></td>
                                 </tr>
                                </table><br><br><br><br>
                              <p>Regards,</p>
							  <p>Team Hamarisociety</p>');		

define('LOGIN_CREDENTIAL_FOR_USER','<p>Dear <b>{{fullname}}</b>,</p>
                <p> Thank you for registering with {{society_name}}. Here are your login credentials,</p>
                <p>Username: {{username}}.</p>
                <p>Password: {{password}}.</p>
                 <br>
                 <p>click here to login : https://hamarisociety.co.in/dashboard
                <p>This is an auto generated email. Please do not reply.</p>


                <p>Regards,</p>
                <p>Hamarisociety Team.</p>');			

define('SOCIETY_CREDENTIALS','<p><b>{{society_name}}</b>,</p>
                  
                <p> Thank you for registering with us. Here are your login credentials,</p>
                <p>Username: {{username}}.</p>
                <p>Password: {{password}}.</p>
                <p>click here to login : https://hamarisociety.co.in/dashboard
                <br>
                <p>Please do not share credentials with anyone else.<br>

                <p>This is an auto generated email. Please do not reply.</p>


                <p>Regards,</p>
                <p>Hamarisociety Team.</p>');  			

define('OTP_TEMPLATE', '<!DOCTYPE html>
<html>
<head>
    <title></title>

<style type="text/css">
    @charset "utf-8";
/* CSS Document */

/* Client-specific Styles */
div, p, a, li, td {
    -webkit-text-size-adjust:none;
}
#outlook a {
    padding:0;
} /* Force Outlook to provide a "view in browser" menu link. */
html {
    width: 100%;
}
body {
    width:100% !important;
    -webkit-text-size-adjust:100%;
    -ms-text-size-adjust:100%;
    margin:0;
    padding:0;
}
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
.ExternalClass {
    width:100%;
} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
} /* Force Hotmail to display normal line spacing. */
#backgroundTable {
    margin:0;
    padding:0;
    width:100% !important;
    line-height: 100% !important;
}
img {
    outline:none;
    text-decoration:none;
    border:none;
    -ms-interpolation-mode: bicubic;
}
a img {
    border:none;
}
.image_fix {
    display:block;
}
p {
    margin: 0px 0px !important;
}
table td {
    border-collapse: collapse;
}
table {
    border-collapse:collapse;
    mso-table-lspace:0pt;
    mso-table-rspace:0pt;
}
a {
    color: #33b9ff;
    text-decoration: none;
    text-decoration:none!important;
}
/*STYLES*/
table[class=full] {
    width: 100%;
    clear: both;
}
/*IPAD STYLES*/
@media only screen and (max-width: 640px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 440px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 420px!important;
text-align:center!important;
}
 img[class=banner] {
width: 440px!important;
height:220px!important;
}
 img[class=col2img] {
width: 440px!important;
height:220px!important;
}
}
/*IPHONE STYLES*/
@media only screen and (max-width: 480px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 280px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 260px!important;
text-align:center!important;
}
 img[class=banner] {
width: 280px!important;
height:140px!important;
}
 img[class=col2img] {
width: 280px!important;
height:140px!important;
}
}

</style>    
</head>
<body>
    <!-- Start of preheader -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of preheader -->
<!-- Start of header -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <tr>
                    <td>
                        <!-- logo -->
                        
                        <!-- end of logo -->
                        <!-- start of menu -->
                        <table bgcolor="#f49a2f" width="250" height="50" border="0" align="right" valign="middle" cellpadding="0" cellspacing="0" border="0" class="devicewidth">
                        <tbody>
                        <tr>
                                <!-- <td height="50" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828" st-content="menu">
                                    <a href="#" style="color: #ffffff;text-decoration: none;">Home</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Shop</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Contact</a>
                                    &nbsp;&nbsp;&nbsp;
                                </td> -->
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of menu -->
                    </td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Header -->
<!-- Start of seperator -->
<!-- End of seperator -->
<!-- Start of main-banner -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                <tbody>
                <tr>
                    <!-- start of image -->
                    <td align="center" st-image="banner-image" style="background-color: #060709;">
                        <div class="imgpop">
                            <a target="_blank" href="#"><img width="300" border="0" height="200" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png" class="banner"></a>
                        </div>
                    </td>
                </tr>
                 <!-- end of image -->
                </tbody>
                </table>
               
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of main-banner -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        </table>
    </td>
</tr>
</tbody>
</table>


<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>


<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
        <div>
            <p>Hi {{useremail}},&nbsp;&nbsp;<br>You recently requested to reset your password for {{society_name}} !&nbsp;&nbsp;<br>use below OTP to Reset password:</p><br>
  <!-- Spacing -->
        <tr>
            <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                &nbsp;
            </td>
        </tr>

<tr>
    <td style="text-align: center;">
        <table width="120" height="32" bgcolor="#f49a2f" align="left" valign="middle" border="0" cellpadding="0" cellspacing="0" style="border-radius:3px;" st-button="Login Here">
        <tbody>
        <tr>
            <td height="9" align="center" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td height="14" align="center" valign="middle" style="font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-weight:bold;color: #ffffff; text-align:center; -webkit-text-size-adjust:none;" st-title="fulltext-btn">
                <a style="text-decoration: none;color: #ffffff; text-align:center;">{{otp}}</a>
            </td>
        </tr>
        <tr>
            <td height="9" align="center" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
<!-- Spacing -->
</tbody>
</table>
                 

           
<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>
<tr>
    <td height="9" align="center" style="font-size:1px; line-height:1px;">
        &nbsp;
    </td>
</tr>

<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
        <div>

            <p>If you did not make this request then you can safely Ignore this E-mail.&nbsp;&nbsp;</p>

            <div><br>
                <p>Regards,<br>{{society_name}}</p>
            </div>
        </div>   
       </div>      
    </td>
</tr>

                      
</tbody>
</table>

<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        <tbody>
        <tr>
            <td align="center" height="30" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of seperator -->
<!-- Start of footer -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
<tbody>
<tr>
    <td>
        <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table bgcolor="#f49a2f" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td>
                        <!-- Social icons -->
                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                        <tbody>
                        <tr>
                            <td width="49" height="43" align="center" style="color: #ffffff;">
                                   Copyright © 2018 <br> Hamari Society.<br> All rights reserved.
                            </td>
                        
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of Social icons -->
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of footer -->
<!-- End of footer -->


</body>
</html>');


define('SOCIETY_REGISTRATION','<!DOCTYPE html>
<html>
<head>
    <title></title>

<style type="text/css">
    @charset "utf-8";
/* CSS Document */

/* Client-specific Styles */
div, p, a, li, td {
    -webkit-text-size-adjust:none;
}
#outlook a {
    padding:0;
} /* Force Outlook to provide a "view in browser" menu link. */
html {
    width: 100%;
}
body {
    width:100% !important;
    -webkit-text-size-adjust:100%;
    -ms-text-size-adjust:100%;
    margin:0;
    padding:0;
}
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
.ExternalClass {
    width:100%;
} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
} /* Force Hotmail to display normal line spacing. */
#backgroundTable {
    margin:0;
    padding:0;
    width:100% !important;
    line-height: 100% !important;
}
img {
    outline:none;
    text-decoration:none;
    border:none;
    -ms-interpolation-mode: bicubic;
}
a img {
    border:none;
}
.image_fix {
    display:block;
}
p {
    margin: 0px 0px !important;
}
table td {
    border-collapse: collapse;
}
table {
    border-collapse:collapse;
    mso-table-lspace:0pt;
    mso-table-rspace:0pt;
}
a {
    color: #33b9ff;
    text-decoration: none;
    text-decoration:none!important;
}
/*STYLES*/
table[class=full] {
    width: 100%;
    clear: both;
}
/*IPAD STYLES*/
@media only screen and (max-width: 640px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 440px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 420px!important;
text-align:center!important;
}
 img[class=banner] {
width: 440px!important;
height:220px!important;
}
 img[class=col2img] {
width: 440px!important;
height:220px!important;
}
}
/*IPHONE STYLES*/
@media only screen and (max-width: 480px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 280px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 260px!important;
text-align:center!important;
}
 img[class=banner] {
width: 280px!important;
height:140px!important;
}
 img[class=col2img] {
width: 280px!important;
height:140px!important;
}
}

</style>    
</head>
<body>
    <!-- Start of preheader -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of preheader -->
<!-- Start of header -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <tr>
                    <td>
                        <!-- logo -->
                      
                        <!-- end of logo -->
                        <!-- start of menu -->
                        <table bgcolor="#f49a2f" width="250" height="50" border="0" align="right" valign="middle" cellpadding="0" cellspacing="0" border="0" class="devicewidth">
                        <tbody>
                        <tr>
                                <!-- <td height="50" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828" st-content="menu">
                                    <a href="#" style="color: #ffffff;text-decoration: none;">Home</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Shop</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Contact</a>
                                    &nbsp;&nbsp;&nbsp;
                                </td> -->
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of menu -->
                    </td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Header -->
<!-- Start of seperator -->
<!-- End of seperator -->
<!-- Start of main-banner -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                <tbody>
                <tr>
                    <!-- start of image -->
                    <td align="center" st-image="banner-image" style="background-color: #060709;">
                        <div class="imgpop">
                            <a target="_blank" href="#"><img width="300" border="0" height="200" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png" class="banner"></a>
                        </div>
                    </td>
                </tr>
                 <!-- end of image -->
                </tbody>
                </table>
               
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of main-banner -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        </table>
    </td>
</tr>
</tbody>
</table>


<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>


<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
        <div>
            <p>Dear {{society_name}},&nbsp;&nbsp;<br>Welcome to <b>Hamarisociety</b> !&nbsp;&nbsp;<br>We have created an account for you. Here are your details:</p><br>
            <table>
                <tr>
                    <th style="text-align:left;">Name</th>
                    <td>: {{username}}</td>
                </tr>

                <tr>
                    <th style="text-align:left;">Password</th>
                    <td>: {{password}}</td>
                </tr>
            </table><br>

            <p>Please do not share credentials with anyone else.<br>This is an auto generated email. Please do not reply.&nbsp;&nbsp;</p>

            <div><br>
                <p>Regards,<br>Hamarisociety !!.</p>
            </div>
        </div>   
       </div>      
    </td>
</tr>

                        <!-- Spacing -->
                        <tr>
                            <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                                &nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: center;">
                                <table width="120" height="32" bgcolor="#f49a2f" align="left" valign="middle" border="0" cellpadding="0" cellspacing="0" style="border-radius:3px;" st-button="Login Here">
                                <tbody>
                                <tr>
                                    <td height="9" align="center" style="font-size:1px; line-height:1px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td height="14" align="center" valign="middle" style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight:bold;color: #ffffff; text-align:center; line-height: 14px; ; -webkit-text-size-adjust:none;" st-title="fulltext-btn">
                                        <a style="text-decoration: none;color: #ffffff; text-align:center;" href="https://hamarisociety.co.in/dashboard/">Login Here</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="9" align="center" style="font-size:1px; line-height:1px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>


                        <!-- Spacing -->
                        </tbody>
                        </table>
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Full Text -->
<!-- Start of seperator -->

<!-- End of seperator -->
<!-- Start of Right Image -->

<!-- End of Right Image -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        <tbody>
        <tr>
            <td align="center" height="30" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of seperator -->
<!-- Start of footer -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
<tbody>
<tr>
    <td>
        <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table bgcolor="#f49a2f" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td>
                        <!-- Social icons -->
                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                        <tbody>
                        <tr>
                            <td width="49" height="43" align="center" style="color: #ffffff;">
                                   Copyright © 2018 <br> Hamari Society.<br> All rights reserved.
                            </td>
                        
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of Social icons -->
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of footer -->
<!-- End of footer -->


</body>
</html>');

define('SOCIETY_MEMBER_REGISTRATION','<!DOCTYPE html>
<html>
<head>
    <title></title>

<style type="text/css">
    @charset "utf-8";
/* CSS Document */

/* Client-specific Styles */
div, p, a, li, td {
    -webkit-text-size-adjust:none;
}
#outlook a {
    padding:0;
} /* Force Outlook to provide a "view in browser" menu link. */
html {
    width: 100%;
}
body {
    width:100% !important;
    -webkit-text-size-adjust:100%;
    -ms-text-size-adjust:100%;
    margin:0;
    padding:0;
}
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
.ExternalClass {
    width:100%;
} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
} /* Force Hotmail to display normal line spacing. */
#backgroundTable {
    margin:0;
    padding:0;
    width:100% !important;
    line-height: 100% !important;
}
img {
    outline:none;
    text-decoration:none;
    border:none;
    -ms-interpolation-mode: bicubic;
}
a img {
    border:none;
}
.image_fix {
    display:block;
}
p {
    margin: 0px 0px !important;
}
table td {
    border-collapse: collapse;
}
table {
    border-collapse:collapse;
    mso-table-lspace:0pt;
    mso-table-rspace:0pt;
}
a {
    color: #33b9ff;
    text-decoration: none;
    text-decoration:none!important;
}
/*STYLES*/
table[class=full] {
    width: 100%;
    clear: both;
}
/*IPAD STYLES*/
@media only screen and (max-width: 640px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 440px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 420px!important;
text-align:center!important;
}
 img[class=banner] {
width: 440px!important;
height:220px!important;
}
 img[class=col2img] {
width: 440px!important;
height:220px!important;
}
}
/*IPHONE STYLES*/
@media only screen and (max-width: 480px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 280px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 260px!important;
text-align:center!important;
}
 img[class=banner] {
width: 280px!important;
height:140px!important;
}
 img[class=col2img] {
width: 280px!important;
height:140px!important;
}
}

</style>    
</head>
<body>
    <!-- Start of preheader -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of preheader -->
<!-- Start of header -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <tr>
                    <td>
                        <!-- logo -->
                      
                        <!-- end of logo -->
                        <!-- start of menu -->
                        <table bgcolor="#f49a2f" width="250" height="50" border="0" align="right" valign="middle" cellpadding="0" cellspacing="0" border="0" class="devicewidth">
                        <tbody>
                        <tr>
                                <!-- <td height="50" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828" st-content="menu">
                                    <a href="#" style="color: #ffffff;text-decoration: none;">Home</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Shop</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Contact</a>
                                    &nbsp;&nbsp;&nbsp;
                                </td> -->
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of menu -->
                    </td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Header -->
<!-- Start of seperator -->
<!-- End of seperator -->
<!-- Start of main-banner -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                <tbody>
                <tr>
                    <!-- start of image -->
                    <td align="center" st-image="banner-image" style="background-color: #060709;">
                        <div class="imgpop">
                            <a target="_blank" href="#"><img width="300" border="0" height="200" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png" class="banner"></a>
                        </div>
                    </td>
                </tr>
                 <!-- end of image -->
                </tbody>
                </table>
               
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of main-banner -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        </table>
    </td>
</tr>
</tbody>
</table>


<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>


<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
        <div>
            <p>Dear {{fullname}},&nbsp;&nbsp;<br>Welcome to {{society_name}} !&nbsp;&nbsp;<br>We have created an account for you. Here are your details:</p><br>
            <table>
                <tr>
                    <th style="text-align:left;">Name</th>
                    <td>: {{username}}</td>
                </tr>

                <tr>
                    <th style="text-align:left;">Password</th>
                    <td>: {{password}} </td>
                </tr>
            </table><br>

            <p>Please do not share credentials with anyone else.<br>This is an auto generated email. Please do not reply.&nbsp;&nbsp;</p>

            <div><br>
                <p>Regards,<br>{{society_name}}.</p>
            </div>
        </div>   
       </div>      
    </td>
</tr>

                        <!-- Spacing -->
                        <tr>
                            <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                                &nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: center;">
                                <table width="120" height="32" bgcolor="#f49a2f" align="left" valign="middle" border="0" cellpadding="0" cellspacing="0" style="border-radius:3px;" st-button="Login Here">
                                <tbody>
                                <tr>
                                    <td height="9" align="center" style="font-size:1px; line-height:1px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td height="14" align="center" valign="middle" style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight:bold;color: #ffffff; text-align:center; line-height: 14px; ; -webkit-text-size-adjust:none;" st-title="fulltext-btn">
                                        <a style="text-decoration: none;color: #ffffff; text-align:center;" href="https://hamarisociety.co.in/dashboard/">Login Here</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="9" align="center" style="font-size:1px; line-height:1px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>


                        <!-- Spacing -->
                        </tbody>
                        </table>
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Full Text -->
<!-- Start of seperator -->

<!-- End of seperator -->
<!-- Start of Right Image -->

<!-- End of Right Image -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        <tbody>
        <tr>
            <td align="center" height="30" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of seperator -->
<!-- Start of footer -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
<tbody>
<tr>
    <td>
        <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table bgcolor="#f49a2f" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td>
                        <!-- Social icons -->
                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                        <tbody>
                        <tr>
                            <td width="49" height="43" align="center" style="color: #ffffff;">
                                    Copyright © 2018 <br> Hamari Society.<br> All rights reserved.
                            </td>
                        
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of Social icons -->
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of footer -->
<!-- End of footer -->


</body>
</html>');


define('MEETING_SEND', '<!DOCTYPE html>
<html>
<head>
    <title></title>

<style type="text/css">
    @charset "utf-8";
/* CSS Document */

/* Client-specific Styles */
div, p, a, li, td {
    -webkit-text-size-adjust:none;
}
#outlook a {
    padding:0;
} /* Force Outlook to provide a "view in browser" menu link. */
html {
    width: 100%;
}
body {
    width:100% !important;
    -webkit-text-size-adjust:100%;
    -ms-text-size-adjust:100%;
    margin:0;
    padding:0;
}
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
.ExternalClass {
    width:100%;
} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
} /* Force Hotmail to display normal line spacing. */
#backgroundTable {
    margin:0;
    padding:0;
    width:100% !important;
    line-height: 100% !important;
}
img {
    outline:none;
    text-decoration:none;
    border:none;
    -ms-interpolation-mode: bicubic;
}
a img {
    border:none;
}
.image_fix {
    display:block;
}
p {
    margin: 0px 0px !important;
}
table td {
    border-collapse: collapse;
}
table {
    border-collapse:collapse;
    mso-table-lspace:0pt;
    mso-table-rspace:0pt;
}
a {
    color: #33b9ff;
    text-decoration: none;
    text-decoration:none!important;
}
/*STYLES*/
table[class=full] {
    width: 100%;
    clear: both;
}
/*IPAD STYLES*/
@media only screen and (max-width: 640px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 440px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 420px!important;
text-align:center!important;
}
 img[class=banner] {
width: 440px!important;
height:220px!important;
}
 img[class=col2img] {
width: 440px!important;
height:220px!important;
}
}
/*IPHONE STYLES*/
@media only screen and (max-width: 480px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 280px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 260px!important;
text-align:center!important;
}
 img[class=banner] {
width: 280px!important;
height:140px!important;
}
 img[class=col2img] {
width: 280px!important;
height:140px!important;
}
}

</style>
</head>
<body>
    <!-- Start of preheader -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->

                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of preheader -->
<!-- Start of header -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <tr>
                    <td>
                        <!-- logo -->

                        <!-- end of logo -->
                        <!-- start of menu -->
                        <table bgcolor="#f49a2f" width="250" height="50" border="0" align="right" valign="middle" cellpadding="0" cellspacing="0" border="0" class="devicewidth">
                        <tbody>
                        <tr>
                                <!-- <td height="50" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828" st-content="menu">
                                    <a href="#" style="color: #ffffff;text-decoration: none;">Home</a>
                                    &nbsp;
&nbsp;
&nbsp;
 <a href="#" style="color: #ffffff;text-decoration: none;">Shop</a>
                                    &nbsp;
&nbsp;
&nbsp;
 <a href="#" style="color: #ffffff;text-decoration: none;">Contact</a>
                                    &nbsp;
&nbsp;
&nbsp;
                                </td> -->
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of menu -->
                    </td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Header -->
<!-- Start of seperator -->
<!-- End of seperator -->
<!-- Start of main-banner -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                <tbody>
                <tr>
                    <!-- start of image -->
                    <td align="center" st-image="banner-image" style="background-color: #060709;">
                        <div class="imgpop">
                            <a target="_blank" href="#"><img width="300" border="0" height="200" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png" class="banner"></a>
                        </div>
                    </td>
                </tr>
                 <!-- end of image -->
                </tbody>
                </table>

            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of main-banner -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        </table>
    </td>
</tr>
</tbody>
</table>


<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>


<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
        <div>
            <p>Dear {{member}},&nbsp;
&nbsp;
<br>Welcome to {{society_name}} !&nbsp;
&nbsp;
<br>Society meeting details:</p><br>
            <table>
                <tr>
                    <th style="text-align:left;">Date</th>
                    <td>: {{meeting_date}}</td>
                </tr>

                <tr>
                    <th style="text-align:left;">Start Time</th>
                    <td>: {{start_time}}</td>
                </tr>
                <tr>
                    <th style="text-align:left;">End Time</th>
                    <td>: {{end_time}} </td>
                </tr>
                <tr>
                    <th style="text-align:left;">Description</th>
                    <td>: {{description}} </td>
                </tr>
            </table><br/>
                <!-- Spacing -->
                <table>
                        <tr>
                            <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                                &nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: center;">
                                <table width="120" height="32" bgcolor="#f49a2f" align="left" valign="middle" border="0" cellpadding="0" cellspacing="0" style="border-radius:3px;" st-button="Login Here">
                                <tbody>
                                <tr>
                                    <td height="9" align="center" style="font-size:1px; line-height:1px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td height="14" align="center" valign="middle" style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight:bold;color: #ffffff; text-align:center; line-height: 14px; ; -webkit-text-size-adjust:none;" st-title="fulltext-btn">
                                        <a style="text-decoration: none;color: #ffffff; text-align:center;" href="https://hamarisociety.co.in/dashboard/">Login Here</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="9" align="center" style="font-size:1px; line-height:1px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>


                        <!-- Spacing -->
            </table><br>



            <div><br>
                <p>Regards,<br>{{society_name}}.</p>
            </div>
        </div>
       </div>
    </td>
</tr>
</tbody>
                        </table>
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Full Text -->
<!-- Start of seperator -->

<!-- End of seperator -->
<!-- Start of Right Image -->

<!-- End of Right Image -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        <tbody>
        <tr>
            <td align="center" height="30" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of seperator -->
<!-- Start of footer -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
<tbody>
<tr>
    <td>
        <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table bgcolor="#f49a2f" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td>
                        <!-- Social icons -->
                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                        <tbody>
                        <tr>
                            <td width="49" height="43" align="center" style="color: #ffffff;">
                               Copyright © 2018 <br> Hamari Society.<br> All rights reserved.
                            </td>

                        </tr>
                        </tbody>
                        </table>
                        <!-- end of Social icons -->
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of footer -->
<!-- End of footer -->


</body>
</html>');

define('REMIANDER', '<!DOCTYPE html>

<html>

<head>

    <title></title>



<style type="text/css">

    @charset "utf-8";

/* CSS Document */



/* Client-specific Styles */

div, p, a, li, td {

    -webkit-text-size-adjust:none;

}

#outlook a {

    padding:0;

} /* Force Outlook to provide a "view in browser" menu link. */

html {

    width: 100%;

}

body {

    width:100% !important;

    -webkit-text-size-adjust:100%;

    -ms-text-size-adjust:100%;

    margin:0;

    padding:0;

}

/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */

.ExternalClass {

    width:100%;

} /* Force Hotmail to display emails at full width */

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {

    line-height: 100%;

} /* Force Hotmail to display normal line spacing. */

#backgroundTable {

    margin:0;

    padding:0;

    width:100% !important;

    line-height: 100% !important;

}

img {

    outline:none;

    text-decoration:none;

    border:none;

    -ms-interpolation-mode: bicubic;

}

a img {

    border:none;

}

.image_fix {

    display:block;

}

p {

    margin: 0px 0px !important;

}

table td {

    border-collapse: collapse;

}

table {

    border-collapse:collapse;

    mso-table-lspace:0pt;

    mso-table-rspace:0pt;

}

a {

    color: #33b9ff;

    text-decoration: none;

    text-decoration:none!important;

}

/*STYLES*/

table[class=full] {

    width: 100%;

    clear: both;

}

/*IPAD STYLES*/

@media only screen and (max-width: 640px) {

 a[href^="tel"], a[href^="sms"] {

 text-decoration: none;

 color: #33b9ff; /* or whatever your want */

 pointer-events: none;

 cursor: default;

}

 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {

 text-decoration: default;

 color: #33b9ff !important;

 pointer-events: auto;

 cursor: default;

}

 table[class=devicewidth] {

width: 440px!important;

text-align:center!important;

}

 table[class=devicewidthinner] {

width: 420px!important;

text-align:center!important;

}

 img[class=banner] {

width: 440px!important;

height:220px!important;

}

 img[class=col2img] {

width: 440px!important;

height:220px!important;

}

}

/*IPHONE STYLES*/

@media only screen and (max-width: 480px) {

 a[href^="tel"], a[href^="sms"] {

 text-decoration: none;

 color: #33b9ff; /* or whatever your want */

 pointer-events: none;

 cursor: default;

}

 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {

 text-decoration: default;

 color: #33b9ff !important;

 pointer-events: auto;

 cursor: default;

}

 table[class=devicewidth] {

width: 280px!important;

text-align:center!important;

}

 table[class=devicewidthinner] {

width: 260px!important;

text-align:center!important;

}

 img[class=banner] {

width: 280px!important;

height:140px!important;

}

 img[class=col2img] {

width: 280px!important;

height:140px!important;

}

}



</style>    

</head>

<body>

    <!-- Start of preheader -->

<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">

<tbody>

<tr>

    <td>

        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">

        <tbody>

        <tr>

            <td width="100%">

                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">

                <tbody>

                <!-- Spacing -->

                <tr>

                    <td width="100%" height="20">

                    </td>

                </tr>

                <!-- Spacing -->

                

                <!-- Spacing -->

                <tr>

                    <td width="100%" height="20">

                    </td>

                </tr>

                <!-- Spacing -->

                </tbody>

                </table>

            </td>

        </tr>

        </tbody>

        </table>

    </td>

</tr>

</tbody>

</table>

<!-- End of preheader -->

<!-- Start of header -->

<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">

<tbody>

<tr>

    <td>

        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">

        <tbody>

        <tr>

            <td width="100%">

                <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">

                <tbody>

                <tr>

                    <td>

                        <!-- logo -->

                        

                        <!-- end of logo -->

                        <!-- start of menu -->

                        <table bgcolor="#f49a2f" width="250" height="50" border="0" align="right" valign="middle" cellpadding="0" cellspacing="0" border="0" class="devicewidth">

                        <tbody>

                        <tr>

                                <!-- <td height="50" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828" st-content="menu">

                                    <a href="#" style="color: #ffffff;text-decoration: none;">Home</a>

                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Shop</a>

                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Contact</a>

                                    &nbsp;&nbsp;&nbsp;

                                </td> -->

                        </tr>

                        </tbody>

                        </table>

                        <!-- end of menu -->

                    </td>

                </tr>

                </tbody>

                </table>

            </td>

        </tr>

        </tbody>

        </table>

    </td>

</tr>

</tbody>

</table>

<!-- End of Header -->

<!-- Start of seperator -->

<!-- End of seperator -->

<!-- Start of main-banner -->

<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">

<tbody>

<tr>

    <td>

        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">

        <tbody>

        <tr>

            <td width="100%">

                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">

                <tbody>

                <tr>

                    <!-- start of image -->

                    <td align="center" st-image="banner-image" style="background-color: #060709;">

                        <div class="imgpop">

                            <a target="_blank" href="#"><img width="300" border="0" height="200" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png" class="banner"></a>

                        </div>

                    </td>

                </tr>

                 <!-- end of image -->

                </tbody>

                </table>

               

            </td>

        </tr>

        </tbody>

        </table>

    </td>

</tr>

</tbody>

</table>

<!-- End of main-banner -->

<!-- Start of seperator -->

<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">

<tbody>

<tr>

    <td>

        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">

        </table>

    </td>

</tr>

</tbody>

</table>





<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">

<tbody>

<tr>

    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
        <div>
            <p>Hi&nbsp;{{username}}.,&nbsp;&nbsp;<br>
            <p>{{subject}}</p><br>
            <p>{{body}}</p>
        </div>
        <div><br>
                <p>Regards,<br>{{society_name}}.</p>
            </div>

        </div>

    </td>

</tr>

</tbody>

</table>



<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">

<tbody>

<tr>

    <td>

        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">

        <tbody>

        <tr>

            <td align="center" height="30" style="font-size:1px; line-height:1px;">

                &nbsp;

            </td>

        </tr>

        </tbody>

        </table>

    </td>

</tr>

</tbody>

</table>

<!-- End of seperator -->

<!-- Start of footer -->

<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">

<tbody>

<tr>

    <td>

        <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">

        <tbody>

        <tr>

            <td width="100%">

                <table bgcolor="#f49a2f" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">

                <tbody>

                <!-- Spacing -->

                <tr>

                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">

                        &nbsp;

                    </td>

                </tr>

                <!-- Spacing -->

                <tr>

                    <td>

                        <!-- Social icons -->

                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">

                        <tbody>

                        <tr>

                            <td width="49" height="43" align="center" style="color: #ffffff;">

                             Copyright © 2018 Hamari Society.<br> All rights reserved. 

                            </td>

                        

                        </tr>

                        </tbody>

                        </table>

                        <!-- end of Social icons -->

                    </td>

                </tr>

                <!-- Spacing -->

                <tr>

                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">

                        &nbsp;

                    </td>

                </tr>

                <!-- Spacing -->

                </tbody>

                </table>

            </td>

        </tr>

        </tbody>

        </table>

    </td>

</tr>

</tbody>

</table>

<!-- End of footer -->

<!-- End of footer -->
</body>
</html>');

define('MONTHLY_BILL_REMAINDER', '<!DOCTYPE html>
<html>
<head>
    <title></title>

<style type="text/css">
    @charset "utf-8";
/* CSS Document */

/* Client-specific Styles */
div, p, a, li, td {
    -webkit-text-size-adjust:none;
}
#outlook a {
    padding:0;
} /* Force Outlook to provide a "view in browser" menu link. */
html {
    width: 100%;
}
body {
    width:100% !important;
    -webkit-text-size-adjust:100%;
    -ms-text-size-adjust:100%;
    margin:0;
    padding:0;
}
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
.ExternalClass {
    width:100%;
} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
} /* Force Hotmail to display normal line spacing. */
#backgroundTable {
    margin:0;
    padding:0;
    width:100% !important;
    line-height: 100% !important;
}
img {
    outline:none;
    text-decoration:none;
    border:none;
    -ms-interpolation-mode: bicubic;
}
a img {
    border:none;
}
.image_fix {
    display:block;
}
p {
    margin: 0px 0px !important;
}
table td {
    border-collapse: collapse;
}
table {
    border-collapse:collapse;
    mso-table-lspace:0pt;
    mso-table-rspace:0pt;
}
a {
    color: #33b9ff;
    text-decoration: none;
    text-decoration:none!important;
}
/*STYLES*/
table[class=full] {
    width: 100%;
    clear: both;
}
/*IPAD STYLES*/
@media only screen and (max-width: 640px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 440px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 420px!important;
text-align:center!important;
}
 img[class=banner] {
width: 440px!important;
height:220px!important;
}
 img[class=col2img] {
width: 440px!important;
height:220px!important;
}
}
/*IPHONE STYLES*/
@media only screen and (max-width: 480px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 280px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 260px!important;
text-align:center!important;
}
 img[class=banner] {
width: 280px!important;
height:140px!important;
}
 img[class=col2img] {
width: 280px!important;
height:140px!important;
}
}
/*invoice_css */
.invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
</style>    
</head>
<body>
    <!-- Start of preheader -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of preheader -->
<!-- Start of header -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <tr>
                    <td>
                        <!-- logo -->
                        
                        <!-- end of logo -->
                        <!-- start of menu -->
                        <table bgcolor="#f49a2f" width="250" height="50" border="0" align="right" valign="middle" cellpadding="0" cellspacing="0" border="0" class="devicewidth">
                        <tbody>
                        <tr>
                                <!-- <td height="50" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828" st-content="menu">
                                    <a href="#" style="color: #ffffff;text-decoration: none;">Home</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Shop</a>
                                    &nbsp;&nbsp;&nbsp; <a href="#" style="color: #ffffff;text-decoration: none;">Contact</a>
                                    &nbsp;&nbsp;&nbsp;
                                </td> -->
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of menu -->
                    </td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Header -->
<!-- Start of seperator -->
<!-- End of seperator -->
<!-- Start of main-banner -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                <tbody>
                <tr>
                    <!-- start of image -->
                    <td align="center" st-image="banner-image" style="background-color: #060709;">
                        <div class="imgpop">
                            <a target="_blank" href="#"><img width="300" border="0" height="200" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png" class="banner"></a>
                        </div>
                    </td>
                </tr>
                 <!-- end of image -->
                </tbody>
                </table>
               
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of main-banner -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        </table>
    </td>
</tr>
</tbody>
</table>


<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>


<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
            <div>
        <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                   
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                {{username}}.,<br>
                                {{address}}
                            </td>
                            
                            <td>
                                Generated at: {{today}}<br>
                                Month: {{month}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Description
                </td>
                
                <td>
                    Price
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Maintenance Amount
                </td>
                
                <td>
                    {{maintenance_amt}}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Parking Amount 
                </td>
                
                <td>
                    {{parking_amt}}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Facility Charges 
                </td>
                
                <td>
                    {{facility_charges}}
                </td>
            </tr>

             <tr class="item">
                <td>
                    Other Charges 
                </td>
                
                <td>
                    {{other_charges}}
                </td>
            </tr>
            
            <tr class="heading">
                <td> Total Amount </td>
                
                <td>
                   {{total_amt}}
                </td>
            </tr>
             <tr>
                <td> Remaining Outstanding </td>
                <td>
                   {{outstanding_remaining}}    
                </td>
            </tr>
            <tr class="heading">
                <td> Net_payable_amt </td>
                <td>
                   {{net_payable_amt}}
                </td>
            </tr>
        </table>
    </div>          
            </div>
        </div>
  <!-- Spacing -->
        <tr>
            <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                &nbsp;
            </td>
        </tr>


<!-- Spacing -->
</tbody>
</table>
                 

           
<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>
<tr>
    <td height="9" align="center" style="font-size:1px; line-height:1px;">
        &nbsp;
    </td>
</tr>

<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
                <p>Regards,<br>{{society_name}}.</p>
            </div>
        </div>   
       </div>      
    </td>
</tr>

                      
</tbody>
</table>

<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        <tbody>
        <tr>
            <td align="center" height="30" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of seperator -->
<!-- Start of footer -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
<tbody>
<tr>
    <td>
        <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table bgcolor="#f49a2f" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td>
                        <!-- Social icons -->
                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                        <tbody>
                        <tr>
                            <td width="49" height="43" align="center" style="color: #ffffff;">
                             Copyright © 2018 Hamari Society.<br> All rights reserved.
                            </td>
                        
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of Social icons -->
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of footer -->
<!-- End of footer -->


</body>
</html>');
define('NOTICES','<!DOCTYPE html>
<html>
<head>
    <title>{{notices_title}}</title>

<style type="text/css">
    @charset "utf-8";
/* CSS Document */

/* Client-specific Styles */
div, p, a, li, td {
    -webkit-text-size-adjust:none;
}
#outlook a {
    padding:0;
} /* Force Outlook to provide a "view in browser" menu link. */
html {
    width: 100%;
}
body {
    width:100% !important;
    -webkit-text-size-adjust:100%;
    -ms-text-size-adjust:100%;
    margin:0;
    padding:0;
}
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
.ExternalClass {
    width:100%;
} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
} /* Force Hotmail to display normal line spacing. */
#backgroundTable {
    margin:0;
    padding:0;
    width:100% !important;
    line-height: 100% !important;
}
img {
    outline:none;
    text-decoration:none;
    border:none;
    -ms-interpolation-mode: bicubic;
}
a img {
    border:none;
}
.image_fix {
    display:block;
}
p {
    margin: 0px 0px !important;
}
table td {
    border-collapse: collapse;
}
table {
    border-collapse:collapse;
    mso-table-lspace:0pt;
    mso-table-rspace:0pt;
}
a {
    color: #33b9ff;
    text-decoration: none;
    text-decoration:none!important;
}
/*STYLES*/
table[class=full] {
    width: 100%;
    clear: both;
}
/*IPAD STYLES*/
@media only screen and (max-width: 640px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 440px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 420px!important;
text-align:center!important;
}
 img[class=banner] {
width: 440px!important;
height:220px!important;
}
 img[class=col2img] {
width: 440px!important;
height:220px!important;
}
}
/*IPHONE STYLES*/
@media only screen and (max-width: 480px) {
 a[href^="tel"], a[href^="sms"] {
 text-decoration: none;
 color: #33b9ff; /* or whatever your want */
 pointer-events: none;
 cursor: default;
}
 .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration: default;
 color: #33b9ff !important;
 pointer-events: auto;
 cursor: default;
}
 table[class=devicewidth] {
width: 280px!important;
text-align:center!important;
}
 table[class=devicewidthinner] {
width: 260px!important;
text-align:center!important;
}
 img[class=banner] {
width: 280px!important;
height:140px!important;
}
 img[class=col2img] {
width: 280px!important;
height:140px!important;
}
}

</style>
</head>
<body>
    <!-- Start of preheader -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->

                <!-- Spacing -->
                <tr>
                    <td width="100%" height="20">
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of preheader -->
<!-- Start of header -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <tr>
                    <td>
                        <!-- logo -->

                        <!-- end of logo -->
                        <!-- start of menu -->
                        <table bgcolor="#f49a2f" width="250" height="50" border="0" align="right" valign="middle" cellpadding="0" cellspacing="0" border="0" class="devicewidth">
                        <tbody>
                        <tr>
                                <!-- <td height="50" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828" st-content="menu">
                                    <a href="#" style="color: #ffffff;text-decoration: none;">Home</a>
                                    &nbsp;
&nbsp;
&nbsp;
 <a href="#" style="color: #ffffff;text-decoration: none;">Shop</a>
                                    &nbsp;
&nbsp;
&nbsp;
 <a href="#" style="color: #ffffff;text-decoration: none;">Contact</a>
                                    &nbsp;
&nbsp;
&nbsp;
                                </td> -->
                        </tr>
                        </tbody>
                        </table>
                        <!-- end of menu -->
                    </td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Header -->
<!-- Start of seperator -->
<!-- End of seperator -->
<!-- Start of main-banner -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
<tbody>
<tr>
    <td>
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                <tbody>
                <tr>
                    <!-- start of image -->
                    <td align="center" st-image="banner-image" style="background-color: #060709;">
                        <div class="imgpop">
                            <a target="_blank" href="#"><img width="300" border="0" height="200" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png" class="banner"></a>
                        </div>
                    </td>
                </tr>
                 <!-- end of image -->
                </tbody>
                </table>

            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of main-banner -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        </table>
    </td>
</tr>
</tbody>
</table>


<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
<tbody>


<tr>
    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; text-align:center; line-height: 24px;">
        <div class="email_body" style="text-align:left;">
        <div>
            <p>Dear {{fullname}},&nbsp;
&nbsp;
<br>{{society_name}} !&nbsp;
&nbsp;
<br><span style="font-weight:bold;">Society Notice Details:</span></p><br>
            <table>
                <tr>
                    <th style="text-align:left;">Subject</th>
                    <td>: {{subject}}</td>
                </tr>

                <tr>
                    <th style="text-align:left;">Description</th>
                    <td>: {{notice_description}} </td>
                </tr>
            </table><br/>
                <!-- Spacing -->
                <table>
                        <tr>
                            <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                                &nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: center;">
                                <table width="120" height="32" bgcolor="#f49a2f" align="left" valign="middle" border="0" cellpadding="0" cellspacing="0" style="border-radius:3px;" st-button="Login Here">
                                <tbody>
                                <tr>
                                    <td height="9" align="center" style="font-size:1px; line-height:1px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td height="14" align="center" valign="middle" style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight:bold;color: #ffffff; text-align:center; line-height: 14px; ; -webkit-text-size-adjust:none;" st-title="fulltext-btn">
                                        <a style="text-decoration: none;color: #ffffff; text-align:center;" href="https://hamarisociety.co.in/dashboard/">Login Here</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="9" align="center" style="font-size:1px; line-height:1px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>


                        <!-- Spacing -->
            </table><br>



            <div><br>
                <p>Regards,<br>{{society_name}}.</p>
            </div>
        </div>
       </div>
    </td>
</tr>


                        </tbody>
                        </table>
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of Full Text -->
<!-- Start of seperator -->

<!-- End of seperator -->
<!-- Start of Right Image -->

<!-- End of Right Image -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
<tbody>
<tr>
    <td>
        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
        <tbody>
        <tr>
            <td align="center" height="30" style="font-size:1px; line-height:1px;">
                &nbsp;
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of seperator -->
<!-- Start of footer -->
<table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
<tbody>
<tr>
    <td>
        <table width="600" bgcolor="#f49a2f" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
        <tbody>
        <tr>
            <td width="100%">
                <table bgcolor="#f49a2f" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td>
                        <!-- Social icons -->
                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                        <tbody>
                        <tr>
                            <td width="49" height="43" align="center" style="color: #ffffff;">
                               Copyright © 2018 <br> Hamari Society.<br> All rights reserved.
                            </td>

                        </tr>
                        </tbody>
                        </table>
                        <!-- end of Social icons -->
                    </td>
                </tr>
                <!-- Spacing -->
                <tr>
                    <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Spacing -->
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<!-- End of footer -->
<!-- End of footer -->


</body>
</html>');                              
