<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');
require_once ('email_template_constants.php');
require_once ('sms_template_constants.php');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
define('FRONT_PAGES','front/front_pages/');
define('FRONT_LAYOUT','front/layout/');
define('ADMIN_HEADER','admin/_layout/admin_header');
define('ADMIN_FOOTER','admin/_layout/admin_footer');
/* Notice Template start */
define('NOTICE_MODULE','admin/notice/');
define('NOTICE_TEMPLATE_AJAX',NOTICE_MODULE.'notice_template_ajax');
define('NOTICE_TEMPLATE_POPUP',NOTICE_MODULE.'notice_template_popup');
/*Notice Template end*/
/* Bill payment start */
define('BILL_PAYMENT_MODULE','admin/bill_payment/');
define('BILL_TEMPLATE_AJAX',BILL_PAYMENT_MODULE.'bill_template_ajax');
define('BILL_TEMPLATE_POPUP',BILL_PAYMENT_MODULE.'bill_template_popup');
/*bill payment end*/
// net banking start
define('NET_BANKING_MODULE','admin/netbanking/');
define('NET_BANKING_AJAX',NET_BANKING_MODULE.'net_banking_ajax');
define('NET_BANKING_POPUP',NET_BANKING_MODULE.'net_banking_popup');
// net banking end
/* User role id start */
define('SUPERADMIN_USER_ID','1');

define('SUPERADMIN','1');
define('SOCIETY_SUPERUSER','2');
define('SOCIETY_ADMIN','3');
define('SOCIETY_MEMBER','4');
/* User role id end */

define('MENU_FOLDER', 'admin/menu/');

define('MENU_VIEW', MENU_FOLDER . 'menu');
define('MENU_AJAX', MENU_FOLDER . 'menu_ajax');
define('MENU_POPUP', MENU_FOLDER . 'menu_popup');
define('MENU_SEQUENCE_VIEW', MENU_FOLDER . 'menu_sequence');

define('USER_ROLES_FOLDER', 'admin/user_roles/');

define('USER_ROLES_VIEW', USER_ROLES_FOLDER . 'role_group_view');
define('CREATE_USER_ROLE__VIEW', USER_ROLES_FOLDER . 'create_role');
define('SHOW_USER_ROLE_VIEW', USER_ROLES_FOLDER . 'show_role_view');
define('EDIT_USER_ROLE_VIEW', USER_ROLES_FOLDER . 'edit_role_view');

define('PERMISSION_MENU_FOLDER','admin/permission_menu/');


define('INDEX_VIEW', PERMISSION_MENU_FOLDER . 'index');
define('EDIT_MENU_VIEW', PERMISSION_MENU_FOLDER . 'edit');
//define('SHOW_ROLE_VIW', ROLE_GROUP_FOLDER . 'show_role_view');
define('COMPLAINT_MODULE','admin/complaint/');
define('COMPLAINT_TEMPLATE_AJAX',COMPLAINT_MODULE.'complaint_template_ajax');
define('COMPLAINT_TEMPLATE_POPUP',COMPLAINT_MODULE.'complaint_template_popup');

define('DOCUMENT_MODULE','admin/document_collection/');
define('DOCUMENT_TEMPLATE_AJAX',DOCUMENT_MODULE.'document_template_ajax');
define('DOCUMENT_TEMPLATE_POPUP',DOCUMENT_MODULE.'document_template_popup');
define('PARKING_MODULE','admin/parking/');
define('PARKING_TEMPLATE_AJAX',PARKING_MODULE.'parking_template_ajax');
define('PARKING_TEMPLATE_POPUP',PARKING_MODULE.'parking_template_popup');
//Email
 define('MAIL_FUNCTION','$mail_function = array("protocol" => "smtp","smtp_host"=>"ssl://smtp.googlemail.com","smtp_port"=>"465","smtp_user"=>"raza10495@gmail.com","smtp_pass"=>"Raza1010","mailtype"=>"mailtype","charset"=>"iso-8859-1","wordwrap"=>"true","newline"=>"\r\n");');
define('EMAIL_FROM', 'raza10495@gmail.com');
//sms
define('SMSAPI','/02+X+jqnec-ERkHS4NboT3T3WTKO8mMBxgBBXc6Nw');
define('SMSURL','https://api.textlocal.in/bulk_json');
define('ADMIN_EMAIL','info.hamarisociety@gmail.com');