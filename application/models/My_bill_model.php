<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class My_bill_model extends MY_Model {

    protected $table = 'my_bill';
    var $fields = array("id", "user_id","society_id","month","maintenance_amt","parking_amt","facility_charges","other_charges","net_payable_amt","total_amt","outstanding_amt_before","created_by","outstanding_amt_after","wallet_bal_before","wallet_bal_after","created_at");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

}
