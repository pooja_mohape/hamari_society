<?php
if(!defined('BASEPATH'))
exit('No Direct Access Script Allowed');

class House_master_model extends MY_Model{

	protected $table = 'house_master';
	var $fields = array("id","block","society_id","house_type","details","is_deleted","house_picture","created_by","created_date","updated_by","updated_date");

	public function addHouse($house_data) {

	   
		$this->db->insert('house_master',$house_data);	
		return true;
	}
	public function houseExist($block,$wing,$building) {
		$sess_sid = $this->session->userdata('society_id');
		$dt = array('block' => $block,'wing'=>$wing,'building'=>$building,'society_id'=>$sess_sid);
		$res = $this->db->where($dt)->get('house_master');
		$result='';
		if($res->num_rows()>0) {
			$result ="<span>Block $block of Wing $wing Building $building is Already Added</span>";
		echo $result;
		}
	}
	public function showHouse($id) {
		
		$this->db->select('sm.name as society_name,sm.id as society_id,h.id,h.building,h.wing,h.block,shc.house_type,h.details,h.is_deleted,h.house_picture,DATE_FORMAT(h.created_date,"%d/%m/%Y") as created_date,h.created_by,DATE_FORMAT(h.updated_date,"%d/%m/%Y") as updated_date');
		$this->db->from('society_master sm');
		$this->db->join('house_master h','sm.id=h.society_id','left');
		$this->db->join('society_house_charges shc','h.house_type=shc.id','left');
		$this->db->where('h.id',$id);
		$res = $this->db->get()->result();
		return $res; 
	}
	public function allHouse() {
		$sess_id = $this->session->userdata('role_id');
		$society_id = $this->session->userdata('society_id');
		$myhid = $this->session->userdata('house_id');
		$this->db->select('sm.name as society_name,sm.id as society_id,h.id,h.building,h.wing,h.block,h.house_type,h.details,h.is_deleted,h.house_picture,shc.house_type,DATE_FORMAT(h.created_date,"%d/%m/%Y") as created_date,h.created_by');
		$this->db->from('society_house_charges shc');
			$this->db->join('house_master h','shc.id=h.house_type');
			$this->db->join('society_master sm','shc.society_id=sm.id');
			$this->db->order_by('h.id','DESC');
			$this->db->where('h.is_deleted','N');
			
		if($sess_id ==SUPERADMIN) {

		} else if($sess_id==SOCIETY_MEMBER){
			$this->db->where('sm.id',$society_id);
			$this->db->where('h.id',$myhid);
		} else{
			$this->db->where('sm.id',$society_id);
		}
			$this->db->where('sm.is_deleted','N');
			$res  =	$this->db->get()->result();
		return $res;

	}
	public function editHouse($id,$sess_scid) {
			
		$this->db->select('sm.name as society_name,sm.id as society_id,h.id,h.wing,h.block,h.house_type,h.details,h.is_deleted,h.house_picture,h.on_lease,shc.house_type,h.building,shc.id as shc_id,DATE_FORMAT(h.created_date,"%d/%m/%Y") as created_date,h.created_by');
		$this->db->from('house_master h');
	    $this->db->from('society_master sm','sm.id=h.society_id','left');
	    $this->db->join('society_house_charges shc','shc.id=h.house_type','left');
		// $this->db->from('society_house_charges shc');
		// $this->db->join('house_master h','shc.id=h.house_type','right');
		// $this->db->from('society_master sm','shc.society_id=sm.id','left');
		$this->db->where('h.id',$id);
		$this->db->where('sm.id',$sess_scid);
		$res  =	$this->db->get()->result();

		return $res;
	}
	public function deAllocat_house($id){

		$res = $this->db->where('id',$id)->get('users')->result();
		$house_id = $res[0]->house_id;

		$this->db->set('allocated','N');
		$this->db->where('id',$house_id);
		$this->db->update('house_master');
	}
	public function updateHouse($hid,$house_data) {

		$this->db->where('id',$hid);
	    $this->db->update('house_master',$house_data);	
		return true;
	}
	public function deleteHouse($id) {

		$this->db->set('is_deleted','Y');
		$this->db->where('id',$id);
		$res = $this->db->update('house_master');
		return true;
	}
	public function houseCSV($data) {
    
    $this->db->insert('house_master', $data);
    return TRUE;
    }
    public function allocateHouseCsv($house_id){

    $this->db->where('id',$house_id);
    $this->db->set('allocated','Y');
    $this->db->update('house_master');	
    } 
    public function validateHouseCsv($validate_csv){

    	$this->db->select('building,wing,block');
    	$this->db->from('house_master');
    	$this->db->where($validate_csv);
    	$res = $this->db->get()->result();
    	return $res;
    }
}