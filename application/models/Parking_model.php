<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parking_model extends MY_Model {

    protected $table = 'parking';
    var $fields = array("id", "user_id","pm_id","society_id","slot_type","slot_no","slot_name","created_by","created_date","updated_by","updated_date");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }
    public function editParking($id){
    	$this->db->select('concat(u.first_name," ",u.last_name) as fullname,u.id as uid,p.*');
    	$this->db->from('parking p');
    	$this->db->join('users u','p.user_id=u.id','left');
    	$this->db->where('p.id',$id);
    	$res = $this->db->get()->result();
    	return $res;
    }
    public function allNames(){
     $this->db->select('concat(u.first_name," ",u.last_name) as fullname,u.id as uid');
    	$this->from('users u');
    	$res = $this->db->get()->result();
    	// show($res,1);
    	return $res;
    }
    public function updatePark($pid,$pdata) {
    	
    	$this->db->where('id',$pid);
    	$this->db->update('parking',$pdata);
    	return true;
    }
    public function delete_park($id)
    {
    	$this->db->where('id',$id);
    	$this->db->delete('parking');
    	return true;
    }
     public function park_slot(){
      
        $this->db->select('pm.id,pm.slot_no,pc.vehicle_type,pc.parking_amt');
        $this->db->from('parking_master pm');
        $this->db->where('pm.society_id',$this->session->userdata('society_id'));
        $this->db->join('parking_charges pc','pc.id=parking_charge_id','left');
        $res = $this->db->get()->result();
        return $res;
    }
}
