<?php
defined('BASEPATH')OR die('No direct script access allowed');

class Profile_model extends CI_Model
{
	
	public function image_insert($id,$fname)
	{
		if($this->input->post() && !empty($this->input->post()))
		{
			$name = $_FILES['profile_pic']['name'];
			$tpname=explode('.', $name);
	        $name=$tpname[0].'_'.$fname.'.'.$tpname[1];
	        $tmp_name = $_FILES['profile_pic']['tmp_name'];
			$path = $_SERVER['DOCUMENT_ROOT'].'/dashboard/upload/profileImages';
	        move_uploaded_file($tmp_name, $path.'/'.$name);          	
	        $data = array('profile_pic' => $name);
	        if($id){

           		 $unlink_img = $this->db->where('id',$id)->get('users')->result();
		          $unlink_img = $unlink_img[0]->profile_pic;
		          if($_FILES['profile_pic']['name']!=""){
		             unlink($_SERVER['DOCUMENT_ROOT']."/dashboard/upload/profileImages/".$unlink_img);
		         }
		         $dd = $this->db->update('users',$data,array('id'=>$id));
	        }
        }
	}

	public function img_select($id){
		$this->db->where('id',$id);
		$this->db->select('profile_pic');
		return $this->db->get('users')->result();
	}

	public function change_pass()
	{
		$data=array(
			'password'=>$this->input->post('password')
		);
		$this->db->where('id',$this->session->userdata('id'));
		$this->db->update('users',$data);
	}

	public function super_profile($user_id){
    	$this->db->where('id',$user_id);
    	return $this->db->get('users')->result_array();
    }

    public function data_insert($data){
    	return $this->db->update('users',$data,array('id'=>$this->session->userdata('id')));
    }

    public function old_pass(){
    	$this->db->where('id',$this->session->userdata('id'));
    	return $this->db->get('users')->result_array();
    }

    public function set_newPassword($cpass)
    {
    	return $this->db->update('users',array('password'=>$cpass),array('id'=>$this->session->userdata('id')));
    }
}
?>