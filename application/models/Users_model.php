<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends MY_Model {

    protected $table = 'users';
    var $fields = array("id", "role_id","first_name","last_name","username","email","phone","birth_date","total_family_member","password","salt","status","is_deleted","society_id","house_id","created_by","created_date","updated_by","updated_date");
    
     public function getRoleName($role_id)
    {
      $this->db->select('role_name');
      $this->db->from('user_roles');
      $this->db->where('id',$role_id);

      $query = $this->db->get()->result();
      return $query[0]->role_name;
    }

    public function houseSociety($society_id) {
      $house_in_society = array(
          'society_id' => $society_id,
          'allocated'  =>'N',
          'is_deleted' =>'N'
      );
    	$this->db->where($house_in_society);
    	$res = $this->db->get('house_master')->result();
    	return $res;
    }
    public function addUser($data,$house_id){

      $this->db->insert('users',$data);
      $inserted_id = $this->db->insert_id();
      
      $this->db->set('allocated','Y');
      $this->db->where('id',$house_id);
      $this->db->update('house_master');
      return $inserted_id;
  }
      public function userExist($username) {
        $res = $this->db->where('username',$username)->get('users');
        if($res->num_rows()>'0') {
          $dd = "<span>Username $username is Already Taken, Try Other Username<span/>";
          echo ($dd);
        }
      }
    public function showUser($id) {
     $this->db->select('u.id as uid,u.*,DATE_FORMAT(u.created_date,"%d/%m/%Y") as created_date,ur.role_name,h.building,h.wing,h.block,sm.name as society_name');
      $this->db->from('users u');
      $this->db->join('user_roles ur','u.role_id=ur.id','left');
      $this->db->join('house_master h','u.house_id=h.id','left');
      $this->db->join('society_master sm','u.society_id=sm.id','left');
      $this->db->where('u.id',$id);
          $res = $this->db->get()->result();
          return $res; 
    }
  	public function allUser() {

        $sess_id = $this->session->userdata('role_id');
       $society_id = $this->session->userdata('society_id');
       $myid = $this->session->userdata('id');
  		$this->db->select('u.id as uid,u.username,concat(first_name," ",last_name) as name,u.email,u.authority_id,u.phone,u.status,DATE_FORMAT(u.created_date,"%d/%m/%Y") as created_date,ur.role_name,h.wing,h.building,h.block,sm.name as society_name');
  		$this->db->from('users u');
  		$this->db->join('user_roles ur','u.role_id=ur.id','left');
  		$this->db->join('house_master h','u.house_id=h.id','left');
  		$this->db->join('society_master sm','u.society_id=sm.id','left');
      $this->db->order_by('u.id','DESC');
  		  if($sess_id ==SUPERADMIN) {

        } else if($sess_id ==SOCIETY_MEMBER){
          $this->db->where('sm.id',$society_id);
          $this->db->where('u.id',$myid);
        } else {
          $this->db->where('sm.id',$society_id);
        }
         $this->db->where('u.role_id !=',SOCIETY_SUPERUSER);
          $this->db->where('u.is_deleted','N');
      $res  = $this->db->get()->result();
     return $res;
  	}
    public function editUser($id) {
    	$this->db->select('u.*,sm.name as society_name,building,wing,block,role_name');
      $this->db->from('users u');
      $this->db->join('society_master sm','u.society_id=sm.id','left');
      $this->db->join('house_master h','u.house_id=h.id','left');
      $this->db->join('user_roles ur','u.role_id=ur.id','left');
      $this->db->where('u.id',$id);
      $res = $this->db->get()->result();
      return $res;
		}
    public function updateUser($uid,$user_data) {

        $this->db->where('id',$uid);
        $this->db->update('users',$user_data);
        return true;
  }
    public function deleteUser($id) {

      $this->db->where('id',$id);
      $this->db->update('users',array('is_deleted'=>'Y','status'=>'N'));
      return true;
    }
            /* User Report*/

    public function roleData() {
      $res = $this->db->get('user_roles')->result();
      return $res;
    }

    public function selfVerify() {

      $id   = $this->session->userdata('id');
      $dt   = $this->db->where('id',$id)->get('users')->result();
      $flag = false;
      if($dt) {
          $first_name           = $dt[0]->first_name;
          $last_name            = $dt[0]->last_name;
          $username             = $dt[0]->username;
          $email                = $dt[0]->email;
          $phone                = $dt[0]->phone;
          $birth_date           = $dt[0]->birth_date;
          $total_family_member  = $dt[0]->total_family_member;
          $society_id           = $dt[0]->society_id;
          $house_id             = $dt[0]->house_id;

          if(!empty($first_name) && !empty($last_name) && !empty($username) && !empty($email) && !empty($phone) && !empty($birth_date) && !empty($total_family_member) && !empty($society_id) && !empty($house_id))
          {
            $flag =true;
          }
      }
      return $flag;
    }

   public function userCSV($data) {
    
    $this->db->insert('users', $data);
    return TRUE;
    }
    
    public function validateUserCsv($validate_csv) {

      $this->db->select('username,email,phone');
      $this->db->from('users');
      $this->db->or_where($validate_csv);
      $res = $this->db->get()->result();
      return $res;
    }
    public function userReport($filter_by) {

      $role_id = $this->session->userdata('role_id');
      $ses_scid = $this->session->userdata('society_id');
      $this->db->select('u.*,ur.role_name,ur.id,sm.name as society_name,h.building,h.wing,h.block');
      $this->db->from('users u');
      $this->db->join('user_roles ur','u.role_id=ur.id','left');
      $this->db->join('society_master sm','u.society_id=sm.id','left');
      $this->db->join('house_master h','u.house_id=h.id','left'); 

       if($filter_by == 'All') {
         
      } else {

        $this->db->where('ur.id',$filter_by);
        
      }
      if($role_id==SUPERADMIN) {

      } else {
        $this->db->where('society_id',$ses_scid);
      }
        $this->db->where('u.is_deleted','N');
           $res = $this->db->get()->result();
          return $res;
      }
    public function userReportByBoth($filter_by,$filter_list)
    {
        // return $this->db->get_where('users',array('role_id'=>$id1,'id'=>$id2))->result();
        $role_id = $this->session->userdata('role_id');
      $ses_scid = $this->session->userdata('society_id');
      $this->db->select('u.*,ur.role_name,ur.id,sm.name as society_name,h.building,h.wing,h.block');
      $this->db->from('users u');
      $this->db->join('user_roles ur','u.role_id=ur.id','left');
      $this->db->join('society_master sm','u.society_id=sm.id','left');
      $this->db->join('house_master h','u.house_id=h.id','left'); 

       if($filter_by == 'All') {
         
      } else {

        $this->db->where('ur.id',$filter_by);
        $this->db->where('sm.id',$filter_list);
        
      }
      if($role_id==SUPERADMIN) {

      } else {
        $this->db->where('society_id',$ses_scid);
      }
        $this->db->where('u.is_deleted','N');
           $res = $this->db->get()->result();
          return $res;
    }
    public function fetchUsers($society_id,$notify_type,$group) {

      $ses_scid = $this->session->userdata('society_id');
      $ses_role = $this->session->userdata('role_id');
      $this->db->select('u.username, concat(first_name," ",last_name) as name,u.id,s.name as sname, ur.role_name rname,phone','authority_id');
      $this->db->from('users u');
      $this->db->join('user_roles ur','u.role_id = ur.id','left');
      $this->db->join('society_master s','u.society_id = s.id','left');
      $this->db->join('member_authority ma','u.authority_id = ma.id','left');
      if($society_id=='all') {

      } else {
        $this->db->where('society_id',$society_id);
      }
       if($group =='all') 
      {

      }elseif ($group == '3') 
      {
          $this->db->where('role_id',$group);
          $this->db->where('authority_id',0);
      }elseif ($group == '4') 
      {
          $this->db->where('role_id',$group);
          $this->db->where('authority_id',0);
      }elseif ($group == 'auth') {
           $this->db->where('authority_id >',0);
      }
      else
      {
         $this->db->where('role_id',$group);
      }
      $res = $this->db->get()->result();
     
      return $res;
    }
    public function user_list($society_id){
        $this->db->select('u.id, concat(first_name," ",last_name)as name');
        $this->db->from('users u');
        $this->db->where('society_id',$society_id);
        $this->db->where('role_id !=',SOCIETY_SUPERUSER);

        $res=$this->db->get()->result();
        return $res;
      }
}
?>