<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Complaint_model extends MY_Model {

    protected $table = 'complaints';
    var $fields = array("id", "user_id","society_id","complaint_type","complaint_details","admin_remark","is_solved", "updated_date","created_date");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

}
