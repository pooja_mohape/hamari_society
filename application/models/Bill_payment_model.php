<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Bill_payment_model extends MY_Model {

	protected $table = 'bill_payment';
	var $fields      = array("id", "user_id", "society_id", "amount", "bank_name", "bank_acc_no", "ifsc_code", "transaction_type", "transaction_ref_no", "branch_name", "payment_remark", "transaction_date", "payment_remark", "admin_remark", "transaction_status", "is_approved", "updated_by", "updated_at", "created_by", "created_at");
	var $key         = 'id';

	public function __construct() {
		parent::__construct();
	}
	public function get_payment_count() {
		$society_id = $this->session->userdata('society_id');
		$this->db->select('count(id) as id');
		$this->db->where('society_id', $society_id);
		$this->db->where('is_approved', 'Pending');
		$this->db->from('bill_payment');
		return $this->db->get()->result();
	}

	public function bill_users($society_id) {
		$this->db->select("id,role_id,CONCAT(first_name, '' , last_name) AS user_name");
		$this->db->from('users u');
		$this->db->where('status','Y');
		$this->db->where('role_id !=',SOCIETY_SUPERUSER);
		$this->db->where('society_id', $society_id);
		// $this->db->where('society_id',;

		return $this->db->get()->result();
	}
}
