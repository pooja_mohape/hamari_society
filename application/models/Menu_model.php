<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menu
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu_model extends MY_Model {

    var $table = 'menu';
    var $fields = array("id", "menu_id", "display_name", "link", "parent", "class", "seq_no", "created_by", "created_date", "updated_by", "updated_date", "record_status");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
   /* IMP FUNCTION DO NOT TOUCH
    * @function    : get_menu
    * @param       : 
    * @detail      : To fetch all menu from menu table.
    */
    function get_menu() {
        $this->db->select("*");
        $this->db->from("menu");
        $this->db->where("record_status = 'Y'");
        $this->db->order_by("parent ASC, seq_no ASC");
        $menu_result = $this->db->get();

        $menu_array = $menu_result->result_array();
        return $menu_array;
    }

    // getMenu End   
    
    /*
     * @function    : add_menu
     * @param       : $menudata -> menu to add.
     * @detail      : to add menu in menu table.
     *                 
     */

    public function add_menu($menudata) {
        if (!empty($menudata)) {
            if ($this->db->insert('menu', $menudata)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

// end of add menu


    /*
     * @function    : get_all_menu with its child name
     * @param       : 
     * @detail      : to get all menu with its child name.
     *                 
     */

    public function get_all_menu() {
        $this->db->select("mn.display_name as child_name,m.*");
        $this->db->from("menu m");
        $this->db->join('menu mn', 'mn.parent = m.id', 'left');
        $this->db->group_by("m.id");
        $result = $this->db->get();
        $menu_array = $result->result_array();
        return $menu_array;
    }

// end of get_all_menu


    /*
     * @function    : find_seq_no
     * @param       : 
     * @detail      : return seq number 
     *                 
     */
    public function find_seq_no($parent_id) {
        $this->db->select("max(seq_no) as seq_no");
        $this->db->from("menu");
        $this->db->where("parent = " . $parent_id);
        $result = $this->db->get();
        $seq = $result->row_array();
        return $seq;
    }

    // end of add menu action
    
    
   /* IMP FUNCTION DO NOT TOUCH
    * @function    : get_parent_menu
    * @param       : 
    * @detail      : To fetch all parent menu from menu table.
    */
    function get_parent_menu() {
        $this->db->select("*");
        $this->db->from("menu");
        $this->db->where("record_status = 'Y'");
        $this->db->where("parent = 0");
        $this->db->order_by("parent ASC, seq_no ASC");
        $menu_result = $this->db->get();

        $menu_array = $menu_result->result_array();
        return $menu_array;
    }

     function get_all_menu_role_id() {
        $this->db->select("id,display_name");
        $this->db->from("menu");
        $this->db->where("record_status = 'Y'");
        $this->db->order_by("parent ASC, seq_no ASC");
        $menu_result = $this->db->get();
        $menu_array = $menu_result->result_array();
        return $menu_array;
    }
    
}

// End of Menu_model class