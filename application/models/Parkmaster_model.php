<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parkmaster_model extends MY_Model {

    protected $table = 'parking_master';
    var $fields = array("society_id","slot_no","parking_charge_id","created_date");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }
   
}
