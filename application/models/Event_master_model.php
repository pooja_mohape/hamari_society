<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Event_master_model extends MY_Model {

    protected $table = 'event_master';
    var $fields = array("eid", "title","description","date");

    public function __construct() {
        parent::__construct();
    }

    public function addEvent($data) {
    	return $this->db->insert('event_master',$data);
    }
    public function allEvent()
    { 
    	return $this->db->get_where('event_master',array('society_id'=>$this->session->userdata('society_id')))->result();
    }
    public function deletEvent($id)
    {
    	return $this->db->delete('event_master',array('eid'=>$id));
    }
    public function updateEvent($data,$updateon)
    {
    	return $this->db->update('event_master',$data,$updateon);
    }

}
?>