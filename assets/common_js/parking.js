﻿$(document).ready(function() {
     
     var tconfig = {
       "processing": true,
       "serverSide": true,
       "ajax": {
           "url": base_url + "back/parking/get_parking_data",
           "type": "POST",
           "data": "json"
       },
       "iDisplayLength": 50,
       "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, 100, "All"]],
       //        "paginate": true,
       "paging": true,
       "searching": true,
       "order": [[1, "desc"]],
       "aoColumnDefs": [
                   {
                       "targets": [1],
                       "visible": false,
                       "searchable": false             

                   },
                   {
                    //"bSortable":false,
                    //"aTargets":[4]
                   }
               ],  

                      "fnRowCallback": function(nRow, aData, iDisplayIndex) {
          var info = $(this).DataTable().page.info();  
          $("td:first", nRow).html(info.start + iDisplayIndex + 1);      
          return nRow;
      },
   };    
   var oTable = $('#mail_display_table').dataTable(tconfig);
       
   $(document).off('click', '.add_btn').on('click', '.add_btn',function () {
       $('#add_edit_popup').modal('show');
   });
  $(document).off('click', '.add_slot').on('click', '.add_slot',function () {
       window.open(base_url+'back/parking/add_slot',"_self");
   });
 
});
