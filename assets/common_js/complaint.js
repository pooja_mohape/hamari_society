$(document).ready(function() {
     
     var tconfig = {
       "processing": true,
       "serverSide": true,
       "ajax": {
           "url": base_url + "back/complaint/get_complaint_data",
           "type": "POST",
           "data": "json"
       },
       "iDisplayLength": 10,
       "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, 100, "All"]],
       //        "paginate": true,
       "paging": true,
       "searching": true,
       "order": [[1, "desc"]],
       "aoColumnDefs": [
                   {
                       "targets": [1],
                       "visible": false,
                       "searchable": false                    
                     },
                   {
                      "bSortable": false,
                      "aTargets": [5,6]
                  }
               ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
          var info = $(this).DataTable().page.info();  
          $("td:first", nRow).html(info.start + iDisplayIndex + 1);  
          console.log(aData[5]);
          if (aData[5] == 'Y')
          {
              $("td:eq(5)", nRow).html('<a href='+base_url+'back/complaint/get_member_data/'+aData[1]+' class="btn btn-info btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>');
          }
          if (aData[5] == 'Y')
          {
             $("td:eq(4)", nRow).html("Yes");
          }else
          {
            $("td:eq(4)", nRow).html("No");
          }           
          return nRow;
      },
   };    
   var oTable = $('#mail_display_table').dataTable(tconfig);
       
   $(document).off('click', '.add_btn').on('click', '.add_btn',function () {
       $('#add_edit_popup').modal('show');
   });
 
});
