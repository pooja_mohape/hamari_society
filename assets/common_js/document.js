$(document).ready(function() {
     
     var tconfig = {
       "processing": true,
       "serverSide": true,
       "ajax": {
           "url": base_url + "back/document_collection/allDocument",
           "type": "POST",
           "data": "json"
       },
       "iDisplayLength": 10,
       "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
       //        "paginate": true,
       "paging": true,
       "searching": true,
       "order": [[1, "desc"]],
       "aoColumnDefs": [
                   {
                       "targets": [1],
                       "visible": false,
                       "searchable": false                    }
               ],         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
          var info = $(this).DataTable().page.info();  
          $("td:first", nRow).html(info.start + iDisplayIndex + 1);  
          if (aData[4] == 'Y')
          {
              $("td:eq(4)", nRow).html('<a href='+base_url+'back/complaint/get_member_data/'+aData[1]+' class="views_wallet_trans margin"><i class="glyphicon glyphicon-eye-open"></i></a>');
          }
          if (aData[4] == 'Y')
          {
             $("td:eq(3)", nRow).html("Yes");
          }else
          {
            $("td:eq(3)", nRow).html("No");
          }           
          return nRow;
      },
   };    
   var oTable = $('#mail_display_table').dataTable(tconfig);
       
   $(document).off('click', '.add_btn').on('click', '.add_btn',function () {
       $('#add_edit_popup').modal('show');
   });
 
});