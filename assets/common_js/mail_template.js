$(document).ready(function() {
    
    var myckeditor = CKEDITOR.replace('mail_body',{
        enterMode : CKEDITOR.ENTER_BR,
        entities : false,
        basicEntities : false,
        entities_greek : false,
        entities_latin : false, 
        htmlDecodeOutput : false
    });

        
/*    $("#mail_template_form").validate({
        ignore: ":hidden:not(textarea)",
        rules: {
            title: {
                required: true     
            },
            subject: {
                required: true
            },
            mail_body: {
                required: function(textarea) {
                    CKEDITOR.instances["mail_body"].updateElement(); // update textarea
                    var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                    return (editorcontent.length === 0);
                }
            }
        }
    });*/
    
    //var myckeditor = CKEDITOR.instances.mail_body;
    myckeditor.on('key', function(evt){
        $('#mail_template_form').valid();
    });


    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "back/notices/get_notices_data",
            "type": "POST",
            "data": "json"
        },
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, 100, "All"]],
        //        "paginate": true,
        "paging": true, 
        "searching": true,
        "order": [[1, "desc"]],
        "aoColumnDefs": [
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false

                    }
                ],

         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();  
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);  
                   
            return nRow;
        },
    };

    var oTable = $('#mail_display_table').dataTable(tconfig);
        
    $(document).off('click', '.add_btn').on('click', '.add_btn', function () {
        //$('#mail_template_form').reset_form();
        CKEDITOR.instances.mail_body.setData("");
        $("#mail_template_form #title").attr("readonly", false); 
        $('#add_edit_popup').modal('show');
    });
    
     $(document).off('click', '#ind_member').on('click', '#ind_member', function () {
        $('#notices_to').show();
    });
     $(document).off('click', '#all_member').on('click', '#all_member', function () {
        $('#notices_to').hide();
    });

    $(document).off('click', '.edit_btn').on('click', '.edit_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var detail = {};
        var div = "";
        var ajax_url = base_url+'back/notices/view_template';
        var form = '';

        detail['id'] = id;

        get_data(ajax_url, form, div, detail, function (response)
        {
            if (response.flag == '@#success#@')
            {
                template_data = response.template_data;
                $("#mail_template_form #id").val(template_data.id);
                $("#mail_template_form #title").val(template_data.title);
                $("#mail_template_form #title").attr("readonly", true); 
                $("#mail_template_form #subject").val(template_data.subject);
                //$("#mail_template_form #mail_body").val(template_data.body);
                CKEDITOR.instances.mail_body.setData( template_data.notice_description );
                $('#add_edit_popup').modal('show');

            // $('#submit_btn').removeClass('disable');
            }
            else
            {
                alert(response.msg);  
            }
        }, '', false);
    });    
   
});